;;; Various defs for ruby

;; (setenv
;;  "PATH"
;;  (concat "~/.rbenv/shims:/usr/local/git/bin:/usr/local/bin:"
;;          (getenv "PATH")))

;; (use-package enh-ruby-mode
;;   :init
;;   (require 'enh-ruby-mode)
;;   :config
;;   (setq enh-ruby-bounce-deep-indent t)
;;   (setq enh-ruby-program (executable-find "ruby"))
;;   (add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))
;;   :interpreter "ruby"
;;   :mode
;;   ("\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'" . enh-ruby-mode)

;;   :hook
;;   (enh-ruby-mode . cavd-ruby-mode-stuff)
;;   (enh-ruby-mode . yard-mode))

;; From https://github.com/aki2o/emacs-docker-robe
(require 'docker-robe)
(docker-robe:activate)

(use-package ruby-ts-mode
  :mode
  ("\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|irbrc\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'" . ruby-ts-mode)
  :hook (ruby-ts-mode . cavd-ruby-mode-stuff)
  :bind (:map ruby-ts-mode-map
              ("C-c r b" . 'treesit-beginning-of-defun)
              ("C-c r e" . 'treesit-end-of-defun))
  :custom
  (ruby-indent-level 2)
  (ruby-indent-tabs-mode nil))

;; (use-package ruby-mode
;;   :config
;;   (defun seeing-is-believing ()
;;     "Replace the current region (or the whole buffer, if none) with the output
;; of seeing_is_believing."
;;     (interactive)
;;     (let ((beg (if (region-active-p) (region-beginning) (point-min)))
;;           (end (if (region-active-p) (region-end) (point-max)))
;;           (origin (point)))
;;       (shell-command-on-region beg end "seeing_is_believing" nil 'replace)
;;       (goto-char origin)))

;;   (setq ruby-insert-encoding-magic-comment nil)
;;   :interpreter "ruby"
;;   :mode
;;   ("\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|irbrc\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'" . ruby-mode)
;;   :hook
;;   (ruby-mode . cavd-ruby-mode-stuff))

(use-package robe
  :after (ruby-mode company-mode)
  :init
  (defun cavd-ruby-robe-stuff ()
    "Set up ruby-robe."
    (push 'company-robe company-backends))
  :config
  (global-robe-mode)
  (require 'company)
  :hook
  (ruby-mode . cavd-ruby-robe-stuff))

(use-package projectile-rails
  :bind  (:map projectile-rails-mode
               ("s-r" . hydra-projectile-rails/body))
  :hook
  (after-init . (lambda () (projectile-rails-global-mode 1))))

(use-package rubocop
  :ensure t
  :commands rubocop-mode)

(use-package inf-ruby
  :after ruby-mode
  :config
  (setq inf-ruby-default-implementation "pry")
  (inf-ruby-enable-auto-breakpoint)
  :hook
  (ruby-mode . inf-ruby-switch-setup)
  (compilation-filter . inf-ruby-auto-enter-and-focus))

(use-package ruby-refactor
  :ensure t
  :config
  (transient-define-prefix cavd-ruby-refactor-transient ()
    "My custom Transient menu for Ruby refactoring."
    [["Refactor"
      ("e" "Extract Region to Method" ruby-refactor-extract-to-method)
      ("v" "Extract Local Variable" ruby-refactor-extract-local-variable)
      ("l" "Extract to let" ruby-refactor-extract-to-let)
      ("c" "Extract Constant" ruby-refactor-extract-constant)
      ("r" "Rename Local Variable or Method (LSP)" eglot-rename)
      ("{" "Toggle block style" ruby-toggle-block)
      ("'" "Toggle string quotes" ruby-toggle-string-quotes)
      ]
     ["Actions"
      ("d" "Documentation Buffer" eldoc-doc-buffer)
      ("q" "Quit" transient-quit-one)
      ("C" "Run a REPL" inf-ruby-console-auto)
      ("TAB" "Switch to REPL" ruby-switch-to-inf)]]))

(use-package rbenv
  :ensure t
  :config
  (setq rbenv-modeline-function 'rbenv--modeline-plain)
  (global-rbenv-mode))

(defun cavd-goto-rails-console ()
  "Pop to the *rails* buffer."
  (interactive)
  (if (get-buffer "*rails*")
      (pop-to-buffer "*rails*")
    (inf-ruby-console-rails)))

(defun cavd-goto-inf-ruby ()
  "Pop to the *ruby* buffer."
  (interactive)
  (if (equal "*ruby*" (buffer-name))
      (call-interactively 'rename-buffer)
      (if (get-buffer "*ruby*")
          (switch-to-buffer "*ruby*")
          (inf-ruby))))

(defun cavd-ruby-mode-stuff ()
  ;; (defadvice inf-ruby-console-auto (before activate-rbenv-for-robe activate)
  ;;   (rbenv-use-corresponding))
  (inf-ruby-minor-mode 1)
  (smartparens-mode)
  (subword-mode)
  ;; (setq ruby-deep-arglist t)     ;Only has effect when `ruby-use-smie intern-soft nil
  (setq ruby-deep-indent-paren nil)
  (setq c-tab-always-indent nil)
  (rubocop-mode)
  (ruby-extra-highlight-mode)
  (eglot-ensure))

;; Copied from https://emacs.stackexchange.com/questions/3537/how-do-you-run-pry-from-emacs/3539
(defun cavd-run-remote-pry (&rest args)
  (interactive)
  (let ((buffer (apply 'make-comint "pry-remote" "pry-remote" nil args)))
    (switch-to-buffer buffer)
    (setq-local comint-process-echoes t)))

;; Creat keybindings for ruby/enh-ruby mode.

(cl-dolist (m '("ruby-mode" "enh-ruby-mode"))
  (eval-after-load m
    `(progn
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "M-{")
         'paredit-wrap-curly)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "M-(")
         'paredit-wrap-round)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "M-[")
         'paredit-wrap-square)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "C-c c")
         'inf-ruby-console-rails)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "C-c C-c")
         'xmp)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "<f12>")
         'cavd-goto-rails-console)
       (define-key
           (symbol-value (intern (concat ,m "-map")))
           (kbd "C-c r d")
         'cavd-run-remote-pry))))

(add-to-list 'auto-mode-alist '("\\.html\\.erb$" . web-mode))

(provide 'cavd-ruby-funcs)
