;; See the following for lsp
;; https://stackoverflow.com/questions/50633092/where-does-go-get-install-packages
;; https://emacs-lsp.github.io/lsp-mode/page/lsp-sqls/
(require 'sql)

;; https://mbork.pl/2024-05-04_Pretty_printing_SQL
;; https://github.com/sql-formatter-org/sql-formatter
(when (executable-find "sql-formatter")
  (defvar sql-formatter-config "~/.emacs.d/cavd-emacs/sql-formatter.json")

  (defun sql-format-region ()
    "Format SQL in region using `sql-formatter'."
    (interactive)
    (shell-command-on-region
     (region-beginning)
     (region-end)
     (concat "sql-formatter -c " sql-formatter-config)
     nil
     t)))

(use-package sql
  :after auth-source
  :commands (cavd-sql-open-database
             set-sql-options
             set-sql-local
             set-sql-remote
             set-sql-ssl-off)
  :init
  (defun lookup-password (&rest keys)
    (let ((result (apply #'auth-source-search keys)))
      (if result
          (funcall (plist-get (car result)
                              :secret))
        nil)))

  (defvar cavd-sql-connections-file
    (concat user-emacs-directory "sql-connections.el")
    "File that contains sql-connections-alist.")

  (defun cavd-sql-make-conn-env-name (conn)
    "Make a string that includes the DB server and name."
    (let ((user (car (alist-get 'sql-user (assoc conn sql-connection-alist))))
          (server (car (alist-get 'sql-server (assoc conn sql-connection-alist)))))
      (concat user "-" (format "%9s" server))))

  (defun cavd-sql-make-file-name (database-env path prefix)
    "Make a file name using DB environment PATH and a PREFIX."
    (concat path prefix database-env ".sql"))

  (defun cavd-sql-open-database (conn)
    "Open a SQLI process and name the SQL statement window with the name provided."
    (interactive (list
                  (progn
                    (if (file-exists-p cavd-sql-connections-file)
                        (load-file cavd-sql-connections-file))
                    (completing-read "Connection: " sql-connection-alist))))
    (if (file-exists-p cavd-sql-connections-file)
        (load-file cavd-sql-connections-file))
    (sql-set-product-feature 'mysql :password-in-comint t)
    (sql-connect conn)
    (let ((database-env (cavd-sql-make-conn-env-name conn)))
      (sql-rename-buffer database-env)
      (setq sql-buffer (current-buffer))
      (cavd-sql-switch-to-db-buffer database-env)
      (sql-mode)
      (sql-set-product "mysql")
      (setq sql-buffer (concat "*SQL: " database-env "*"))))

  (defun cavd-sql-switch-to-db-buffer (database-env)
    (interactive)
    (other-window 1)
    (find-file (cavd-sql-make-file-name database-env "~/sql/" "DB-")))

  (defun set-sql-options (options)
    "Turn on/off ssh tunneling.  ssh must reference same port."
    (interactive "Specifiy port (if any): ")
    (setq sql-mysql-options `(",options")))

  (defun set-sql-remote (port)
    "Turn on port for ssh tunneling.  ssh must reference same port."
    (interactive "sPort: ")
    (push (concat "-P " port) sql-mysql-options))

  (defun set-sql-ssl-off ()
    "Turn off ssl."
    (interactive)
    (push "--ssl-mode=DISABLED " sql-mysql-options))

  (defun unset-sql-mysql-options ()
    "Unset last thing pushed to sql-mysql-options."
    (interactive)
    (setq sql-mysql-options (cdr sql-mysql-options)))

  (defun set-sql-local ()
    "Turn off port for ssh tunneling.  This is for running locally."
    (interactive)
    (push "-v" sql-mysql-options)
    (setq sql-port nil))

  (defun cavd-switch-sql-buffer ()
    "Switch to a sql buffer."
    (interactive)
    (let ((bufs
           (cl-remove-if-not
            (lambda (buf)
              (with-current-buffer buf
                (equal 'sql-interactive-mode major-mode)))
            (internal-complete-buffer "" nil t))))
      (cond ((eq (car bufs) 0)
             (message "No SQL buffers."))
            ((eq (length bufs) 1)
             (pop-to-buffer (car bufs)))
            ((> (length bufs) 1)
             (ivy-read
              "Choose buffer: "
              bufs
              :matcher #'ivy--switch-buffer-matcher
              :action (lambda (buf) (pop-to-buffer buf))
              :keymap ivy-switch-buffer-map
              :caller 'cavd-switch-sql-buffer))
            )))

  :bind
  ("<C-f4>" . cavd-switch-sql-buffer)
  ("s-4" . cavd-switch-sql-buffer)
  :hook
  (sql-mode . cavd-sql-mode-stuff)
  (sql-interactive-mode . cavd-sqli-hook-stuff)
  :config
  (require 'sql-completion)
  (defvar db-fields nil)

  (defun cavd-sql-mode-stuff ()
    (smartparens-mode 1)
    (show-smartparens-mode -1)
    (abbrev-mode)
    ;; (setq company-backends '(cavd-company-sql))
    (define-key sql-mode-map "\t" 'completion-at-point))

  (defun cavd-sqli-hook-stuff ()
    (smartparens-mode 1)
    (toggle-truncate-lines)
    (define-key sql-interactive-mode-map "\t" 'completion-at-point)
    ;; These should go in completions code
    (set (make-local-variable 'sql-mysql-database) sql-database)
    (set (make-local-variable 'mysql-user) sql-user)
    (set (make-local-variable 'mysql-options) sql-mysql-options)
    (set (make-local-variable 'mysql-password) sql-password)
    (set (make-local-variable 'mysql-server) sql-server)
    (set (make-local-variable 'mysql-port) sql-port)
    ;; (set (make-local-variable 'sql-mysql-options) sql-mysql-options)

    ;; (sql-mysql-build-databases t)
    (sql-mysql-completion-init)
    ;; (setq company-backends '(cavd-company-sql))
    (setq comint-input-ignoredups t)
    (setq sql-alternate-buffer-name (sql-make-smart-buffer-name))
    (setq sql-input-ring-file-name
          (cavd-sql-make-file-name
           (concat sql-database "-" sql-server)
           "~/sql/"
           "history-")
          )
    (hl-line-mode))

  (setq sql-mysql-program (executable-find "mysql"))
  (push "sys" sql-mysql-exclude-databases)
  (push "Database" sql-mysql-exclude-databases)
  (push "performance_schema" sql-mysql-exclude-databases)
  (push "--default-character-set=utf8mb4" sql-mysql-options)
)

(use-package sql-indent
  :ensure t
  :commands sqlind-minor-mode
  :hook
  (sql-mode . sqlind-minor-mode))

(defun cavd-set-sql-mysql-databases ()
  (setq sql-mysql-databases
        (cl-remove-if
         (lambda (db)
           (or
            (and (> (length db) 3) (string= (substring db -4) "test"))
            (string= db "sys")))
         sql-mysql-databases)))

(defun sql-make-smart-buffer-name ()
  "Return a string that can be used to rename a SQLi buffer.
  This is used to set `sql-alternate-buffer-name' within
  `sql-interactive-mode'."
  (or (and (boundp 'sql-name) sql-name)
      (concat (if (not(string= "" sql-server))
                  (concat
                   (or (and (string-match "[0-9.]+" sql-server) sql-server)
                       (car (split-string sql-server "\\.")))
                   "/"))
              sql-database)))

;; (defvar cavd-sql-connections-file
;;   (concat user-emacs-directory "sql-connections.el")
;;   "File that contains sql-connections-alist.")

;; (if (file-exists-p cavd-sql-connections-file)
;;     (load-library cavd-sql-connections-file))

(defmacro mysql-connect-preset (name)
  `(let ,(cdr (assoc name sql-connection-alist))
     (sql-product-interactive sql-product)))

(defun sql-connect-preset (name)
  "Connect to a predefined SQL connection listed in `sql-connection-alist'"
  (eval `(let ,(cdr (assoc name sql-connection-alist))
    (flet ((sql-get-login (&rest what)))
      (sql-product-interactive sql-product)))))

(defun local-mysql ()
  (interactive)
  (ivy-read
   "Choose connection: "
   (mapcar
    (lambda (conn)
      (car conn))
    sql-connection-alist)
   :action (lambda (x) (mysql-connect-preset x))
   :caller 'local-mysql))

(defadvice sql-send-region (after sql-store-in-history)
  "The region sent to the SQLi process is also stored in the history."
  (let ((history (buffer-substring-no-properties start end)))
    (with-current-buffer
        sql-buffer
      (message history)
      (if (and (funcall comint-input-filter history)
               (or (null comint-input-ignoredups)
                   (not (ring-p comint-input-ring))
                   (ring-empty-p comint-input-ring)
                   (not (string-equal (ring-ref comint-input-ring 0)
                                      history))))
          (ring-insert comint-input-ring history))
      (setq comint-save-input-ring-index comint-input-ring-index)
      (setq comint-input-ring-index nil))))

(ad-activate 'sql-send-region)

(defadvice sql-connect-mysql (around sql-mysql-port activate)
  "Add support for connecting to MySQL on other ports"
  (let ((sql-mysql-options
         (or (and (boundp 'sql-port)
                  sql-port
                  (cons (concat "-P "
                                (or (and
                                     (numberp sql-port)
                                     (number-to-string sql-port))
                                    sql-port))
                        sql-mysql-options))
             sql-mysql-options)))
    ad-do-it))

;; The following is a modified version of:
;; https://emacs.stackexchange.com/questions/26365/is-there-a-company-backend-for-completion-in-sql-interactive-mode
;; (defvar db-fields-file "~/.emacs.d/cavd-emacs/ke-tables.el"
;;   "File containing fields.")

;; (if (file-exists-p db-fields-file)
;;     (load-library db-fields-file))

(defun cavd-company-sql-prefix ()
  "Grab prefix for `cavd-company-sql' backend."
  (let* ((sym (company-grab-symbol))
         (start 0)
         (end (length sym))
         (str (set-text-properties start end nil sym)))
    (and
     (or (eq major-mode 'sql-mode) (eq major-mode 'sql-interactive-mode))
     (mysql-connection)
     (not (company-in-string-or-comment))
     (or sym
         'stop))))

(defun cavd-company-sql-candidates (prefix)
  "Candidates list for `mysql'.
PREFIX is a candidates prefix supplied by `company'."
  (cl-remove-if-not
   (lambda (x)
     (s-prefix? prefix x t))
   (mapcar
    (lambda (x)
      (if (consp x)
          (car x)
        x))
    (append
     (mapcar
      (lambda (com) (list com))
      (append sql-mysql-command-alist sql-mysql-other-alist))
     (mysql-show-tables sql-mysql-database)
     (mysql-show-all-columns)))))

(defun cavd-company-sql (command &optional arg &rest ignored)
  "`company-mode' back-end for SQL mode based on database name."
  (interactive (list 'interactive))
  (cl-case command
    (interactive (company-begin-backend 'cavd-company-sql))
    (prefix (cavd-company-sql-prefix))
    (candidates (cavd-company-sql-candidates arg))
    (sorted t)))

;(add-to-list 'company-backends 'cavd-company-sql)

;; (setq sql-mysql-options (list "-P 3307"))
;; DBI:mysql:(database name):127.0.0.1

;; (unless (string-match-p "dal2039" cavd-machine)
;;   (add-hook 'edbi:sql-mode-hook
;;             (lambda ()
;;               (company-mode -1)
;;               (auto-complete-mode 1)))

;;   (add-hook 'edbi-minor-mode-hook
;;             (lambda ()
;;               (company-mode -1)
;;               (auto-complete-mode 1)))
;;   (add-hook 'sql-mode-hook 'edbi-minor-mode))

(provide 'cavd-sql-funcs)
