;;
;; javascript code
;;

(defun cavd-js-hook ()
  (smartparens-mode 1)
  (local-set-key (kbd "C-x C-e") 'js-send-last-sexp)
  (local-set-key (kbd "C-c b") 'js-send-buffer))

(use-package js
  :commands (lsp lsp-deferred)
  :hook
  (js-mode . cavd-js-hook)
  (js-mode . lsp-deferred)
  (js-mode . add-node-modules-path)
  :mode
  (("composer.lock" . js-mode)
   ("\\.js.twig$" . js-mode))
  :config
  (setq js-indent-level 2
        js-jsx-syntax t))

(use-package rjsx-mode
  :mode
  ("components\\/.*\\.js\\'" . rjsx-mode))

(use-package js-comint
  :commands (run-js js-send-last-sexp js-send-buffer)
  :init
  (defun inferior-js-mode-hook-setup ()
    (add-hook 'comint-output-filter-functions 'js-comint-process-output))
  :config
  (setq js-comint-program-arguments '("--interactive"))
  :hook
  (inferior-js-mode . inferior-js-mode-hook-setup))

(use-package typescript-mode
  :ensure t
  :config
  (setq typescript-indent-level 2)
  :hook
  (typescript-mode . smartparens-mode))

(add-hook 'typescript-ts-base-mode-hook 'smartparens-mode)

;; Tide
;; (use-package tide
;;   :hook
;;   (before-save . tide-format-before-save)
;;   :config
;;   (defun setup-tide-mode ()
;;     (interactive)
;;     (tide-setup)
;;     (flycheck-mode +1)
;;     (smartparens-mode +1)
;;     (setq flycheck-check-syntax-automatically '(save mode-enabled))
;;     (eldoc-mode +1)
;;     ;; company is an optional dependency. You have to
;;     ;; install it separately via package-install
;;     (company-mode +1)
;;     ;; (remove-hook 'before-save-hook 'tide-format-before-save)
;;     )
;;   (setq typescript-indent-level 2
;; 	tide-format-options
;; 	'(:tabSize 2
;; 		   :indentSize 2
;; 		   :insertSpaceBeforeAndAfterBinaryOperators false
;; 		   :insertSpaceAfterFunctionKeywordForAnonymousFunctions false
;; 		   :insertSpaceAfterOpeningAndBeforeClosingNonemptyParenthesis false
;; 		   :insertSpaceAfterOpeningAndBeforeClosingNonemptyBrackets false
;; 		   :placeOpenBraceOnNewLineForControlBlocks false
;; 		   :insertSpaceAfterCommaDelimiter false
;; 		   :insertSpaceAfterSemicolonInForStatements false
;; 		   :insertSpaceAfterKeywordsInControlFlowStatements false
;; 		   :placeOpenBraceOnNewLineForFunctions false)))


;; see https://github.com/Microsoft/TypeScript/blob/cc58e2d7eb144f0b2ff89e6a6685fb4deaa24fde/src/server/protocol.d.ts#L421-473 for the full list available options

;; (add-hook 'typescript-mode-hook 'setup-tide-mode)
;; begin ng2
;; (add-hook 'ng2-mode-hook 'setup-tide-mode)
;; (skewer-setup)

(provide 'cavd-js-funcs)
