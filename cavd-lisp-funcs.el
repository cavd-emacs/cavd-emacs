;;
;; Lisp
;;

(autoload 'clhs-doc "clhs" "Get doc on ANSI CL" t)
(define-key help-map "\C-l" 'clhs-doc)
(custom-set-variables
 '(tags-apropos-additional-actions
   '(("Common Lisp" clhs-doc clhs-symbols))))

(use-package slime
  :load-path "~/elisp/slime"
  :commands (cltl2-lookup slime-setup slime-mode)
  :preface
  (defvar slime-dir (concat elisp-dir "/slime"))

  ;; CL doc (CLtL2, HyperSpec) locations
  (defconst cltl2-root-url (concat "file://" (concat home-dir "lisp/doc/cltl2/")))
  (defconst cltl2-prog "~/elisp/cltl2")
  (defconst common-lisp-hyperspec-root (concat home-dir "lisp/doc/Hyperspec/"))
  (defconst common-lisp-hyperspec-symbol-table
    (concat common-lisp-hyperspec-root "Data/Map_Sym.txt"))
  (defconst hyperspec-prog (concat slime-dir "/lib/hyperspec"))

  :mode
  ("\\.lisp$" . lisp-mode)
  ("\\.lsp$" . lisp-mode)
  ("\\.cl$" . lisp-mode)
  ("\\.asd$" . lisp-mode)
  ("\\.system$" . lisp-mode)
  ("\\.sexp$" . lisp-mode)
  :bind
  ("<f5>" . slime)
  ("<C-f5>" . slime-connect)
  ("<C-S-f5>" . slime-disconnect)
  ("<f12>" . slime-selector)
  :hook
  (lisp-mode . (lambda ()
                 (slime-mode t)
                 (paredit-mode +1)))
;  (slime-load . slime-fuzzy-init)
;  (inferior-lisp-mode . (lambda () (inferior-slime-mode t)))
  :config
  ;; Putting the env var in slime-lisp-implementations doesn't seem to work
  (setq ccl-dir "~/src/ccl")
  (setenv "PATH" (concat ccl-dir ":" (getenv "PATH")))
  (setenv "CCL_DEFAULT_DIRECTORY" ccl-dir)

  (defun clhs-info ()
    (interactive)
    (ignore-errors
      (info (concatenate 'string "(gcl) " (thing-at-point 'symbol)))))

  (defun cavd-cltl2-lookup ()
    (interactive)
    (cltl2-lookup (thing-at-point 'symbol)))

  (defun replace-slime-cmd ()
    "Replaces regular Slime command."
    (interactive)
    (defun slime ()
      "Start Slime without an inferior lisp."
      (interactive)
      (while (not (ignore-errors (slime-connect "localhost" 4005) t))
        (sit-for 1))))

  (defun cavd-load-slime/contribs ()
    "Load slime and some contribs."
    (interactive)
    (require 'slime)
    (add-to-list 'load-path (concat slime-dir "/contrib"))
    (slime-setup '(slime-fancy slime-asdf slime-tramp)))

  (defun cavd-reload-slime ()
    (interactive)
    (mapc 'load-library
          (reverse (cl-remove-if-not
                    (lambda (featrue)
                      (string-prefix-p "slime" featrue))
                    (mapcar 'symbol-name features))))
    ;(setq slime-protocol-version (slime-changelog-date))
    (cavd-load-slime/contribs))

  ;; Load/Requires
  ;; (autoload 'redshank-mode "redshank"
  ;;   "Minor mode for editing and refactoring Common Lisp code." t)
  ;; (autoload 'turn-on-redshank-mode "redshank"
  ;;   "Turn on Redshank mode.  See function 'redshank-mode'." t)
  ;; (require 'redshank-loader)

  ;; (eval-after-load "redshank-loader"
  ;;   `(redshank-setup '(lisp-mode-hook
  ;;                      slime-repl-mode-hook) t))

  (require 'slime-autoloads)

  (eval-after-load "paredit"
    '(progn
       (setq paredit-space-for-delimiter-predicates
             '((lambda (e d)
                 (member
                  (buffer-local-value 'major-mode (current-buffer))
                  '(lisp-mode)))))
       (define-key paredit-mode-map (kbd "RET") nil)
       (define-key paredit-mode-map (kbd "M-s") nil)
       (define-key paredit-mode-map (kbd "M-|") 'paredit-split-sexp)
       (define-key lisp-mode-map (kbd "M-;") 'paredit-comment-dwim)
       (define-key paredit-mode-map (kbd "M-;") nil)
       (define-key paredit-mode-map (kbd "C-c M-w") 'paredit-copy-as-kill)
       (define-key paredit-mode-map (kbd "{") 'paredit-open-curly)
       (define-key paredit-mode-map (kbd "}") 'paredit-close-curly)
       (define-key paredit-mode-map (kbd ")") 'paredit-close-round-and-newline)
       (define-key paredit-mode-map (kbd "M-)") 'paredit-close-round)
       (define-key paredit-mode-map (kbd "M-[") 'paredit-wrap-square)
       (define-key paredit-mode-map (kbd "M-{") 'paredit-wrap-curly)))

  ;; (when window-system
  ;;   (require 'mic-paren)
  ;;   (paren-activate)
  ;;   (setf paren-priority 'close))

  ;; Load slime and the contribs
  (cavd-load-slime/contribs)

  (setq slime-lisp-implementations
        '((ccl ("~/bin/ccl/scripts/ccl64"))
          (sbcl ((executable-find "sbcl") "--noinform"))
          (ecl ((executable-find "ecl")))
          (abcl ((executable-find "abcl")))))

  (defun cavd-translate-filenames ()
    (push (list "[a-zA-Z0-9-]*.local"
                (lambda (emacs-filename) emacs-filename)
                (lambda (lisp-filename) lisp-filename))
          slime-filename-translations)

    (push (list "[a-zA-Z0-9-]*"
                (lambda (emacs-filename) emacs-filename)
                (lambda (lisp-filename) lisp-filename))
          slime-filename-translations)

    (push (list "[a-zA-Z0-9-]*.local"
                (lambda (emacs-filename) emacs-filename)
                (lambda (lisp-filename) lisp-filename))
          slime-filename-translations))

  (defun paredit-check-region-for-yank ()
    "Run after yank and yank-pop to verify balanced parens."
    (when paredit-mode
      (save-excursion
        (save-restriction
          (narrow-to-region (point) (mark))
          (condition-case nil
              (check-parens)
            (error
             (if (not (y-or-n-p "The text inserted has unbalanced parentheses, continue? "))
                 (delete-region (point-min) (point-max)))))))))

  (defadvice yank (after yank-check-parens activate)
    (paredit-check-region-for-yank))
  (defadvice yank-pop (after yank-pop-check-parens activate)
    (paredit-check-region-for-yank))


  ;; (setq lisp-indent-function 'common-lisp-indent-function
  ;;       slime-use-autodoc-mode nil)

  (load-library cltl2-prog)
  (load-library hyperspec-prog)

  (setq slime-complete-symbol*-fancy t)
  (setq slime-autodoc-use-multiline-p t)
  (setq slime-repl-history-remove-duplicates t)
  (setq slime-repl-history-trim-whitespaces t)
  (push 'slime-fuzzy-complete-symbol slime-completion-at-point-functions)
  (define-key slime-mode-map (kbd "<f1>") 'slime-hyperspec-lookup)
  (define-key slime-mode-map (kbd "<M-f1>") 'cavd-cltl2-lookup)
  (define-key slime-mode-map (kbd "C-c c") 'slime-call-defun)
  (define-key slime-mode-map (kbd "C-c s") 'slime-sync-package-and-default-directory)
  (cavd-translate-filenames)
  )

(provide 'cavd-lisp-funcs)
