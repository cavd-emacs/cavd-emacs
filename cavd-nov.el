;;; Various defs for epub

(push '("\\.epub\\'" . nov-mode) auto-mode-alist)

(defun cavd-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"))

(setq nov-text-width 80)
(setq nov-text-width most-positive-fixnum)
(setq visual-fill-column-center-text t)

;; Hooks
(add-hook 'nov-mode-hook 'cavd-nov-font-setup)
(add-hook 'nov-mode-hook 'visual-line-mode)
(add-hook 'nov-mode-hook 'visual-fill-column-mode)

(provide 'cavd-nov)
