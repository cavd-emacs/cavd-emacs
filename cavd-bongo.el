(use-package bongo
  :ensure t
  :config
  (setq bongo-default-directory "~/Music")
  (setq bongo-prefer-library-buffers t) ; Might want to change this
  (setq bongo-insert-whole-directory-trees t)
  (setq bongo-logo nil)
  (setq bongo-action-track-icon nil)
  (setq bongo-display-track-icons nil)  ; Not sure what this is
  (setq bongo-display-header-icons nil)
  (setq bongo-display-playback-mode-indicator nil)
  (setq bongo-display-inline-playback-progress nil)
  (setq bongo-header-line-mode t)
  (setq bongo-mode-line-indicator-mode nil)

  (defun contrib/bongo-add-dired-files ()
    "Add marked files inside of a Dired buffer to the Bongo library"
    (interactive)
    (let (file-point file (files nil))
      (dired-map-over-marks
       (setq file-point (dired-move-to-filename)
             file (dired-get-filename)
             files (append files (list file)))
       nil t)
      (save-excursion
        ;; Is this always safe or can there be more than
        ;; one Bongo buffer?
        (set-buffer bongo-default-playlist-buffer-name)
        (mapc 'bongo-insert-file files))))

  (defun prot/bongo-dired-library ()
    "Activate `bongo-dired-library-mode' when accessing the
~/Music directory.  This is meant to be hooked into `dired-mode'.

Upon activation, the directory becomes a valid library buffer for
Bongo, from where we can, among others, add tracks to playlists.
The added benefit is that Dired will continue to behave as
normal, making this a superior alternative to a purpose-specific
library buffer."
    (when (string-match-p "\\`~/Music/" default-directory)
      (set (make-local-variable 'bongo-dired-library-mode) 't)))
  :hook
  (dired-mode . prot/bongo-dired-library)
  :bind (("s-m m" . bongo-playlist)
         ("s-m s" . bongo-pause/resume) ;; start/stop
         ("s-m n" . bongo-next)
         ("s-m p" . bongo-previous)
         :map bongo-dired-library-mode-map
         ("SPC" . (lambda ()
                    (interactive)
                    (contrib/bongo-add-dired-files)
                    (bongo-play-random)
                    (bongo-random-playback-mode 1)))))

(provide 'cavd-bongo)
