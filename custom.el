;;;  Customizations

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(adaptive-fill-mode nil)
 '(adaptive-fill-regexp (purecopy "[ \11]*\\([-!|#%;>*·•‣⁃◦]+[ \11]*\\)*"))
 '(ansi-color-names-vector
   ["#0d0d0d" "#cc6666" "#b5bd68" "#f0c674" "#81a2be" "#c9b4cf" "#8abeb7" "#ffffff"])
 '(ansi-term-color-vector
   [unspecified "#081724" "#ff694d" "#68f6cb" "#fffe4e" "#bad6e2" "#afc0fd" "#d2f1ff" "#d3f9ee"] t)
 '(anzu-deactivate-region nil)
 '(anzu-replace-threshold nil)
 '(anzu-replace-to-string-separator "")
 '(anzu-search-threshold 100)
 '(c-default-style
   '((c-mode . "bsd")
     (objc-mode . "bsd")
     (java-mode . "java")
     (awk-mode . "awk")
     (other . "gnu")))
 '(canlock-password "1897fef2886ada63baf4c29e058b76ac5d13c49b")
 '(custom-safe-themes
   '("1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "756597b162f1be60a12dbd52bab71d40d6a2845a3e3c2584c6573ee9c332a66e" "6a37be365d1d95fad2f4d185e51928c789ef7a4ccf17e7ca13ad63a8bf5b922f" "b8f561a188a77e450ab8a060128244c81dea206f15c1152a6899423dd607b327" "90b5269aefee2c5f4029a6a039fb53803725af6f5c96036dee5dc029ff4dff60" "33c5a452a4095f7e4f6746b66f322ef6da0e770b76c0ed98a438e76c497040bb" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default))
 '(dbgp-default-port 9003)
 '(dgi-commit-message-format "%cr\11%s" t)
 '(eshell-hist-ignoredups t)
 '(eshell-scroll-to-bottom-on-output 'all)
 '(eyebrowse-new-workspace t)
 '(fci-rule-color "#5c5e5e")
 '(geben-dbgp-default-port 9003)
 '(git-commit-setup-hook
   '(git-commit-save-message git-commit-setup-changelog-support git-commit-turn-on-auto-fill git-commit-turn-on-flyspell git-commit-propertize-diff with-editor-usage-message))
 '(hydra-default-hint nil)
 '(ibuffer-default-sorting-mode 'major-mode)
 '(ibuffer-use-other-window nil)
 '(ibuffer-view-ibuffer t)
 '(inhibit-startup-screen t)
 '(ivy-posframe-display-functions-alist
   '((complete-symbol . ivy-posframe-display-at-point)
     (swiper)
     (swiper-isearch)
     (counsel-describe-function)
     (counsel-describe-variable)
     (counsel-rg)
     (counsel-switch-buffer)
     (complete-symbol . ivy-posframe-display-at-point)
     (counsel-M-x . ivy-posframe-display-at-window-bottom-left)
     (t . ivy-posframe-display-at-frame-center)))
 '(ivy-posframe-height-alist '((swiper . 15) (swiper-isearch . 15) (t . 10)))
 '(ivy-posframe-parameters
   '((left-fringe . 2)
     (right-fringe . 2)
     (internal-border-width . 2)))
 '(jdee-db-active-breakpoint-face-colors (cons "#0d0d0d" "#41728e"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0d0d0d" "#b5bd68"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0d0d0d" "#5a5b5a"))
 '(max-lisp-eval-depth 2000)
 '(max-specpdl-size 2552 t)
 '(ns-command-modifier 'super)
 '(org-agenda-files '("~/org/todo.org" "~/org/sifter.org"))
 '(package-selected-packages
   '(docker consult-git-log-grep vertico-prescient dap-mode multi-vterm vterm-toggle ready-player cape direnv docker-compose-mode terraform-mode hcl-mode seeing-is-believing pinentry company-nginx nginx-mode elisp-lint popper ruby-refactor envrc expreg counsel-jq nodejs-repl quick-buffer-switch add-node-modules-path consult-projectile dockerfile-mode fontaine tree-sitter tree-sitter-langs corfu embark embark-consult marginalia vertico yard-mode accent consult smartscan smart-region expand-region reveal-in-osx-finder clhs prescient ivy-prescient git-timemachine rg transient gcmh dired-posframe lsp-ui lsp-ivy lsp-mode counsel-projectile dired-rsync goto-last-change beginend password-store vterm flimenu apache-mode js-comint dictionary fd-dired revbufs ido-vertical-mode php-auto-yasnippets ivy-avy ivy-fuz erc-hl-nicks flyspell-correct-ivy orderless counsel-tramp mark-thing-at windresize ts ws-butler transpose-frame objed eshell-autojump eshell-git-prompt fish-completion verb which-key-posframe git-backup git-backup-ivy anzu company-posframe vagrant-tramp dired-subtree diredfl ivy-posframe company-dict compdef ox-jira volume bongo ibuffer-projectile vlf fuz pass edit-server yasnippet-snippets jiralib2 define-word eshell-z shackle deadgrep clomacs hierarchy connection phpactor phpunit rainbow-mode github-review immaterial-theme forge dired-git-info disk-usage amx nswbuff xr synosaurus magit highlight-defined yaml-imenu git-gutter other-frame-window ido-completing-read+ doom-modeline night-owl-theme company-edbi csv-mode elixir-mode flycheck-elixir ivy-yasnippet cliphist speeddating auto-yasnippet ialign paced free-keys smart-jump ivy-xref company-eshell-autosuggest ruby-extra-highlight sicp multi-term visual-fill-column frecency rjsx-mode nov neotree ebdb noaa go-mode dad-joke eyebrowse rubocop helpful shx company-flx sql-indent dumb-jump link-hint smart-mode-line-powerline-theme ivy-hydra nord-theme easy-escape historian ivy-historian project-shells mu4e-alert mu4e-maildirs-extension kaolin-theme green-screen-theme hacker-typer promise exec-path-from-shell cobol-mode ripgrep ac-inf-ruby afternoon-theme ag ampc anything avy-flycheck bbdb browse-at-remote browse-kill-ring buffer-move char-menu chruby cl-generic color-theme-sanityinc-solarized color-theme-sanityinc-tomorrow company-web counsel-osx-app cycle-quotes debbugs diff-hl dtrt-indent easy-kill edbi-minor-mode elisp-slime-nav enh-ruby-mode flx-ido flx-isearch fullframe ggtags historyf http-twiddle ibuffer-vc indent-guide inf-php js2-mode json-reformat jump-char key-chord kill-or-bury-alive lcs less-css-mode lui markdown-mode+ material-theme mic-paren paredit pc-bufsw projectile-rails psvn psysh quickrun rbenv redshank resize-window restclient rinari robe rspec-mode scratch selectric-mode smart-mode-line smartparens smex solarized-theme tramp-theme undo-tree use-package web web-mode which-key whitespace-cleanup-mode whole-line-or-region xml-rpc yaml-mode powerline ng2-mode ts-comint git-blamed wgrep-ag web-mode-edit-element projectile-ripgrep readline-complete copy-as-format emacs-sounds ivy-rich hydra ace-window ivy-youtube avy auto-complete company counsel diminish edbi flycheck inf-ruby ivy markdown-mode oauth2 popup projectile ruby-compilation swiper websocket))
 '(package-vc-selected-packages
   '((ready-player :vc-backend Git :url "https://github.com/xenodium/ready-player")))
 '(redshank-accessor-name-function 'ignore)
 '(redshank-canonical-package-designator-function 'redshank-package-designator/keyword)
 '(safe-local-variable-values
   '((nginx-indent-level . 2)
     (eglot-server-programs
      (ruby-base-mode "bundle" "exec" "solargraph" "stdio"))
     (web-mode-indent-style . 2)
     (web-mode-markup-indentation . 2)
     (projectile-project-run-cmd . "mkdir -p build; cd build; cmake ..; make run")
     (projectile-project-compilation-cmd . "mkdir -p build; cd build; cmake ..; make")
     (firestarter . ert-run-tests-interactively)
     (checkdoc-minor-mode . t)
     (mangle-whitespace . t)
     (eval when
           (fboundp 'rainbow-mode)
           (rainbow-mode 1))
     (encoding . utf-8)
     (Package . DTX)
     (Patch-file . T)
     (Package . ZWEI)
     (package . asdf)
     (Package . SYNTAX-STYLING)
     (Package . CL-WHO)
     (Package . HUNCHENTOOT)
     (Package RT :use "COMMON-LISP" :colon-mode :external)
     (syntax . COMMON-LISP)
     (Package . FLEXI-STREAMS)
     (Dictionary . /afs/cs/project/clisp/scribe/hem/hem)
     (Editor . t)
     (package . ccl)
     (Package . CL-DOCUMENTATION)
     (Log . Hemlock.Log)
     (Package . Hemlock-Internals)
     (Log . hemlock.log)
     (Package . Hemlock)
     (Package . GUI)
     (Package . TAPE)
     (Package . MAIL)
     (Vsp . 0)
     (Fonts CPTFONT HL12 TR12I COURIER CPTFONT HL12B)
     (Syntax . COMMON-LISP)
     (Package . CL-USER)
     (Syntax . Common-Lisp)
     (Package . CONX)
     (Package . System)
     (Package . CCL)
     (Syntax . ANSI-Common-Lisp)
     (Base . 10)))
 '(spacemacs-theme-comment-bg nil)
 '(split-height-threshold 0)
 '(tab-width 4)
 '(tags-apropos-additional-actions '(("Common Lisp" clhs-doc clhs-symbols)))
 '(term-scroll-show-maximum-output t)
 '(term-scroll-to-bottom-on-output t)
 '(text-mode-hook '(text-mode-hook-identify))
 '(tramp-verbose 1)
 '(transient-default-level 7)
 '(underline-minimum-offset 1)
 '(use-dialog-box nil)
 '(use-file-dialog nil)
 '(vc-annotate-background "#1d1f21")
 '(vc-annotate-color-map
   (list
    (cons 20 "#b5bd68")
    (cons 40 "#c8c06c")
    (cons 60 "#dcc370")
    (cons 80 "#f0c674")
    (cons 100 "#eab56d")
    (cons 120 "#e3a366")
    (cons 140 "#de935f")
    (cons 160 "#d79e84")
    (cons 180 "#d0a9a9")
    (cons 200 "#c9b4cf")
    (cons 220 "#ca9aac")
    (cons 240 "#cb8089")
    (cons 260 "#cc6666")
    (cons 280 "#af6363")
    (cons 300 "#936060")
    (cons 320 "#765d5d")
    (cons 340 "#5c5e5e")
    (cons 360 "#5c5e5e")))
 '(vc-annotate-very-old-color nil)
 '(which-key-idle-delay 3)
 '(which-key-idle-secondary-delay 0.05)
 '(which-key-popup-type 'side-window)
 '(which-key-show-early-on-C-h t)
 '(x-underline-at-descent-line t))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(company-tooltip ((t (:background "black" :foreground "green"))))
;;  '(company-tooltip-common ((t (:inherit company-tooltip :foreground "cyan"))))
;;  '(company-tooltip-selection ((t (:inherit company-tooltip :background "dim gray"))))
;;  '(cursor ((t (:background "#FFFFFF"))))
;;  '(erc-input-face ((t (:foreground "cornflower blue"))))
;;  '(holiday ((((class color) (background dark)) (:foreground "dark gray"))))
;;  '(magit-diff-file-heading-highlight ((t (:background "#373d3f" :foreground "gray"))))
;;  '(which-func ((t (:foreground "forest green")))))

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(erc-input-face ((t (:foreground "cornflower blue"))))
;;  '(slack-message-output-text ((t (:weight normal :height 1.0))))
;;  '(which-func ((t (:foreground "forest green")))))

;  '(mode-line-inactive ((t (:background "#3B4252" :foreground "#999999"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(erc-input-face ((t (:foreground "cornflower blue"))))
 '(slack-message-output-text ((t (:weight normal :height 1.0))))
 '(which-func ((t (:foreground "white")))))
