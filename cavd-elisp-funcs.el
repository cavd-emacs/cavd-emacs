;;
;; Elisp
;;

(use-package emacs
  :preface
  (defun cavd-emacs-lisp-hook ()
    (enable-paredit-mode)
    (highlight-defined-mode 1)
    (elisp-slime-nav-mode 1))
  :hook
  (emacs-lisp-mode . cavd-emacs-lisp-hook))

(use-package elisp-slime-nav
  :ensure t
  :diminish
  :commands (
             elisp-slime-nav-find-elisp-thing-at-point
             pop-tag-mark
             xref-pop-marker-stack)
  :hook
  (emacs-lisp-mode . elisp-slime-nav-mode))

(use-package highlight-defined
  :ensure t
  :commands highlight-defined-mode)

(use-package ielm
  :bind
  ("M-<f5>" . ielm)
  ("s-5" . ielm)
  :config
  (defvar ielm-comint-input-ring nil "Global copy of the buffer-local variable.")

  (defun set-ielm-comint-input-ring ()
    ;; create a buffer-local binding of kill-buffer-hook
    (make-local-variable 'kill-buffer-hook)
    ;; save the value of comint-input-ring when this buffer is killed
    (add-hook 'kill-buffer-hook 'save-ielm-comint-input-ring)
    ;; restore saved value (if available)
    (when ielm-comint-input-ring
      (message "Restoring comint-input-ring...")
      (setq comint-input-ring ielm-comint-input-ring)))

  (defun save-ielm-comint-input-ring ()
    (message "Saving comint-input-ring...")
    (setq ielm-comint-input-ring comint-input-ring))
  (setq ielm-prompt "* ")
  (add-to-list 'desktop-locals-to-save 'ielm-comint-input-ring)
  :hook
  (ielm-mode . cavd-emacs-lisp-hook)
  (ielm-mode . set-ielm-comint-input-ring))

(font-lock-add-keywords 'emacs-lisp-mode
  '(("\\<\\(FIXME\\|TODO\\):" 1 'font-lock-warning-face prepend)))

(defun cavd-interactive-minibuf-hook ()
  (eldoc-mode)
  (enable-paredit-mode))

(add-hook 'eval-expression-minibuffer-setup-hook 'cavd-interactive-minibuf-hook)
;(add-hook 'minibuffer-inactive-mode-hook 'cavd-interactive-minibuf-hook)

(provide 'cavd-elisp-funcs)
