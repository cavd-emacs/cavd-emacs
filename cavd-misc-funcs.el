;;
;; Miscellaneous functions
;;

(require 'rect)

(use-package emacs
  :commands (cavd-column-to-list my:yank-rectangle-to-new-lines)
  :config
  (defun cavd-column-to-list (beg end sep)
    "Convert a column of values to a list."
    (interactive "r\nsWhich separator? ")
    (while (re-search-forward "\n[ ]*" end t)
      (replace-match sep)))

  ;; Courtesy of https://github.com/dsedivec/dot-emacs-d/blob/master/recipes/emacs-yank-rectangle-to-new-lines.el
  (defvar killed-rectangle)

  (defun my:yank-rectangle-to-new-lines ()
    (interactive)
    (unless killed-rectangle
      (error (concat "`killed-rectangle' is not set"
                     " (did you mean to `copy-rectangle-as-kill'?)")))
    (save-excursion (newline (length killed-rectangle)))
    (yank-rectangle))

  :bind
  ("C-x r z" . cavd-column-to-list)
  ("C-x r C-y" . my:yank-rectangle-to-new-lines))

(use-package emacs
  :commands (cavd-base64-encode cavd-base64-decode)
  :config
  (defun cavd-base64-encode ()
    "Encode the string using base64."
    (interactive)
    (let* ((str
            (read-from-minibuffer "Value: "))
           (new-str (base64-encode-string str)))
      (kill-new new-str)))

  (defun cavd-base64-decode ()
    "Decode the string using base64."
    (interactive)
    (let* ((str
            (read-from-minibuffer "Value: "))
           (new-str (base64-decode-string str)))
      (kill-new new-str))))


(defun cavd-url-unhex-string (s)
  "Convert stuff like %5B %5D to [ ]"
  (url-unhex-string s))

(defun cavd-convert-crlf-to-lf ()
  "Convert CRLF line-endings to LF."
  (interactive)
  (set-buffer-file-coding-system 'utf-8-unix))

(defun cavd-gtags-create-or-update ()
  "Create or update the gnu global tag file"
  (interactive)
  (if (not (= 0 (call-process "global" nil nil nil " -p"))) ; tagfile doesn't exist?
      (let ((olddir default-directory)
            (topdir (read-directory-name
                     "gtags: top of source tree:" default-directory)))
        (cd topdir)
        (shell-command "gtags && echo 'created tagfile'")
        (cd olddir))             ; restore
    ;;  tagfile already exists; update it
    (shell-command "global -u && echo 'updated tagfile'")))

(use-package emacs
  :commands
  (
   cavd-dismiss-notification
   set-system-dark-mode
   )
  :config
  (defun cavd-dismiss-ical ()
    "Get rid of the iCal Alarm."
    (interactive)
    (do-applescript "activate application \"NotificationCenter\"
tell application \"System Events\"
    tell process \"Notification Center\"
        set theWindow to group 1 of UI element 1 of scroll area 1 of window \"Notification Center\"
        click theWindow
        set theActions to actions of theWindow
        repeat with theAction in theActions
            if description of theAction is \"Close\" then
                tell theWindow
                    perform theAction
                end tell
            end if
        end repeat
    end tell
end tell"))

  (defun cavd-dismiss-notification ()
    (interactive)
    (let ((applescript "my closeNotif()
on closeNotif()
    tell application \"System Events\"
        tell process \"Notification Center\"
            set theWindows to every window
            repeat with i from 1 to number of items in theWindows
                set this_item to item i of theWindows
                try
                    click button 1 of this_item
                on error

                    my closeNotif()
                end try
            end repeat
        end tell
    end tell
end closeNotif"))
      (mac-do-applescript applescript)))

  (defun set-system-dark-mode ()
    (interactive)
    (if (string=
         (shell-command-to-string "printf %s \"$( osascript -e \'tell application \"System Events\" to tell appearance preferences to return dark mode\' )\"")
         "true")
        (load-theme 'modus-vivendi t)
      (load-theme 'modus-operandi t)))

  (defun find-first-non-ascii-char ()
    "Find the first non-ascii character from point onwards."
    (interactive)
    (let (point)
      (save-excursion
        (setq point
              (catch 'non-ascii
                (while (not (eobp))
                  (or (eq (char-charset (following-char))
                          'ascii)
                      (throw 'non-ascii (point)))
                  (forward-char 1)))))
      (if point
          (goto-char point)
        (message "No non-ascii characters."))))

  (defun steal-mouse ()
    (interactive)
    (and window-system
         (set-mouse-position
          (selected-frame)
          (1- (frame-width)) -1)))

  :bind
  ("C-c U" . cavd-turn-off-shuffle)
  ("C-c x" . cavd-dismiss-notification)
  ("C-c S" . find-first-non-ascii-char)
  ("s-m" . steal-mouse))

;; Re-open file as sudo:
(use-package emacs
  :init
  (defun th-find-file-sudo (file)
    "Opens FILE with root privileges."
    (interactive "F")
    (set-buffer (find-file (concat "/sudo::" file)))
    (rename-buffer (concat "sudo::" (buffer-name))))

  (defun th-find-file-sudo-maybe ()
    "Re-finds the current file as root if it's read-only after
querying the user."
    (interactive)
    (let ((file (buffer-file-name)))
      (and (not (file-writable-p file))
           (y-or-n-p "File is read-only.  Open it as root? ")
           (progn
             (kill-buffer (current-buffer))
             (th-find-file-sudo file)))))

  :hook
  (find-file . th-find-file-sudo-maybe))

(cl-defun cavd-visit-edbi-buffer ()
  "Find the active EDBI buffer (if any), and switch to it."
  (interactive)
  (if (get-buffer "*edbi-dbviewer*")
      (switch-to-buffer "*edbi-dbviewer*")
    (edbi:open-db-viewer)))

(defun kill-word-at-point ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'word)))
    (kill-region (car bounds) (cdr bounds))))

(defun env-line-to-cons (env-line)
  "convert a string of the form \"VAR=VAL\" to a
cons cell containing (\"VAR\" . \"VAL\")."
  (if (string-match "\\([^=]+\\)=\\(.*\\)" env-line)
      (cons (match-string 1 env-line) (match-string 2 env-line))))

(defun interactive-env-alist (&optional shell-cmd env-cmd)
  "launch /usr/bin/env or the equivalent from an interactive
shell, parsing and returning the environment as an alist."
  (let ((cmd (concat (or shell-cmd "/bin/bash -ic")
                     " "
                     (or env-cmd "/usr/bin/env"))))
    (mapcar 'env-line-to-cons
            (cl-remove-if
             (lambda (str)
               (string-equal str ""))
             (split-string (shell-command-to-string cmd) "[\r\n]")))))


(defun misc-search-ddg (query)
  "Search the web with `browse-url'."
  (interactive "sQuery: ")
  (browse-url
   (format "https://duckduckgo.com/?q=%s&t=h_&ia=web"
           (url-encode-url query))))

(defun misc-search-google (query)
  "Search the web with `browse-url'."
  (interactive "sQuery: ")
  (browse-url
   (format "https://www.google.com/search?q=%s&ie=UTF-8&gbv=1"
           (url-encode-url query))))

(defun setenv-from-cons (var-val)
  "set an environment variable from a cons cell containing
two strings, where the car is the variable name and cdr is
the value, e.g. (\"VAR\" . \"VAL\")"
  (setenv (car var-val) (cdr var-val)))

(defun setenv-from-shell-environment (&optional shell-cmd env-cmd)
  "apply the environment reported by `/usr/bin/env' (or env-cmd)
as launched by `/bin/bash -ic' (or shell-cmd) to the current
environment."
  (mapc 'setenv-from-cons (interactive-env-alist shell-cmd env-cmd)))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-end-position)))))

(defun cavd-kill-file-name ()
  "Add the file or buffer name to the kill ring."
  (interactive)
  (kill-new (file-name-nondirectory (buffer-file-name))))

(defun cavd-insert-file-name ()
  "Insert the file or buffer name at point."
  (interactive)
  (insert (file-name-nondirectory (buffer-file-name))))

(defun cavd-ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

(defun cavd-play-sound-file (file)
  "Play sound stored in FILE."
  (interactive)
  (call-process "afplay" nil 0 nil file))

(defun cavd-show-current-file ()
  "Show the file name for the current buffer."
  (interactive)
  (let ((buffer-name (buffer-file-name (current-buffer))))
    (if buffer-name
        (message "%s: %s" (system-name) buffer-name)
      (message "No file associated with buffer."))))

(defun toggle-window-split ()
  "This snippet toggles between horizontal and vertical
layout of two windows."
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

;; Courtesy of Andreas Andreas Röhler
(defun ar-up-list (arg)
  "Move forward out of one level of parentheses.
With ARG, do this that many times.
A negative argument means move backward but still to a less deep spot."
  (interactive "p")
  (let ((orig (point))
        (pps (syntax-ppss))
        erg)
    (and (nth 8 pps) (goto-char (nth 8 pps)))
    (ignore-errors (up-list arg))
    (and (< orig (point)) (setq erg (point)))
    (when (called-interactively-p) (message "%s" erg))
    erg))

(defun cavd-upcase-rectangle-line (startcol endcol fill)
  (when (= (move-to-column startcol (if fill t 'coerce)) startcol)
    (upcase-region
     (point)
     (progn
       (move-to-column endcol 'coerce)
       (point)))))

(defun cavd-upcase-rectangle (start end)
  "Upcase rectangle contents on each line."
  (interactive "r")
  (goto-char
   (apply-on-rectangle 'cavd-upcase-rectangle-line start end nil)))

(defun cavd-sync-elpa-load-path ()
  "Update `load-path' to the contents of ~/.emacs.d/elpa"
  (interactive)
  (cl-dolist (d (directory-files package-user-dir nil "^[a-z]"))
    (if (not (cl-member
              (lambda (d1)
                (equal d d1))
              load-path))
        (cl-pushnew d load-path))))

;; Courtesy of johnw
(defun toggle-transparency ()
  (interactive)
  (if (and (cadr (frame-parameter nil 'alpha))
           (/= (cadr (frame-parameter nil 'alpha)) 100))
      (set-frame-parameter nil 'alpha '(100 100))
    (set-frame-parameter nil 'alpha '(40 40))))

(defun open-monday-ticket ()
  "Open the queried ticket number in monday.com."
  (interactive)
  (let ((ticket-number
         (read-from-minibuffer "Which ticket? ")))
    (browse-url
     (concat "https://kalkomey.monday.com/boards/4535831146/pulses/" ticket-number))))

(defun browse-ticket ()
  "Open the queried ticket number in whatever bug tracking is hard-coded below."
  (interactive)
  (let ((ticket-number
         (read-from-minibuffer "Which ticket? ")))
    (browse-url
     (concat "https://tickets.kalkomey.com/issues/" ticket-number))))

(defun celsius-to-fahrenheit ()
  "Convert celsius to fahrenheit."
  (interactive)
  (cl-letf ((temperature (string-to-number
                          (read-from-minibuffer "Celsius: "))))
    (message "%S" (+ 32.0 (* temperature (/ 9.0 5.0))))))

(defun fahrenheit-to-celsius ()
  "Convert celsius to fahrenheit."
  (interactive)
  (cl-letf ((temperature (string-to-number
                          (read-from-minibuffer "Fahrenheit: "))))
    (message "%S" (* (/ 5.0 9.0) (- temperature 32)))))

(defun compilation-ansi-color-process-output ()
  (ansi-color-process-output nil)
  (set (make-local-variable 'comint-last-output-start)
       (point-marker)))

(defun cavd-reformat ()
  "Reformat the current buffer, removing tabs along the way."
  (interactive)
  (with-current-buffer
      (current-buffer)
    (progn
      (untabify (point-min) (point-max))
      (indent-region (point-min) (point-max)))))

(defun cavd-xml-format ()
  "Format XML with xmllint."
  (interactive)
  (shell-command-on-region
   (point-min)
   (point-max)
   "xmllint -format -"
   (current-buffer) t "*Xmllint Error Buffer*" t))

(defun cavd-slack-region ()
  "Slack the current region to the recipient.
TODO:
if shell is not running, start it
better handling of region."
  (interactive)
  (copy-as-format)
  (let ((to (read-from-minibuffer "Recipient? "))
        (buf-string (first kill-ring)))
    (process-send-string
     "shell"
     (concat "echo '" buf-string "' | slackecho -c " to "\n"))))

(defun file-notify-rm-all-watches ()
  "Remove all existing file notification watches from Emacs."
  (interactive)
  (maphash
   (lambda (key _value)
     (file-notify-rm-watch key))
   file-notify-descriptors))

(defun open-terminal-in-workdir ()
  (interactive)
  (call-process-shell-command
   (concat "open -a iTerm " default-directory) nil 0))

(add-hook 'compilation-filter-hook #'compilation-ansi-color-process-output)

(defun cavd-install-packages ()
  "Ensure the packages I use are installed. See `package-selected-packages'."
  (interactive)
  (let ((missing-packages (cl-remove-if #'package-installed-p package-selected-packages)))
    (when missing-packages
      (message "Installing %d missing package(s)" (length missing-packages))
      (package-refresh-contents)
      (mapc #'package-install missing-packages))))

(use-package emacs
  :config
  (defun cavd-describe-package-at-point ()
    "`apropos' with `ivy-thing-at-point'."
    (interactive)
    (let ((thing (ivy-thing-at-point)))
      (when (use-region-p)
        (deactivate-mark))
      (describe-package (intern (regexp-quote thing)))))

  (defun cavd-humanize (start end)
    "Humanize the region between START and END."
    (interactive "r")
    (save-excursion
      (goto-char start)
      (while (< (point) end)
        (let ((word (buffer-substring (point) (line-end-position))))
          (setq word (replace-regexp-in-string "_" " " word t t))
          (setq word (replace-regexp-in-string "_id\\'" "" word t t))
          (setq word (capitalize word))
          (delete-region (point) (line-end-position))
          (insert word)
          (forward-line 1)))))

  (defun cavd-remove-tramp-hooks ()
    "Remove hooks that clear recentf."
    (interactive)
    (setq tramp-cleanup-all-connections-hook nil)
    (setq tramp-cleanup-connection-hook nil))

  (defun cavd-reset-text-scale ()
    "Reset text scale."
    (interactive)
    (text-scale-increase 0))

  ;; Courtesy of magnars
  (defun open-line-below ()
    (interactive)
    (end-of-line)
    (newline)
    (indent-for-tab-command))

  (defun open-line-above ()
    (interactive)
    (beginning-of-line)
    (newline)
    (forward-line -1)
    (indent-for-tab-command))

  ;; From http://pragmaticemacs.com/emacs/aligning-text/
  (defun bjm/align-whatever (start end)
    "Align columns by ampersand"
    (interactive "r")
    (let ((whatever (read-from-minibuffer "Delimiter: ")))
      (align-regexp start end
                    (concat "\\(\\s-*\\)" whatever) 1 1 t)))

  ;; Begin from https://krsoninikhil.github.io/2018/12/15/easy-moving-from-vscode-to-emacs/
  (defun duplicate-line ()
    (interactive)
    (let ((col (current-column)))
      (move-beginning-of-line 1)
      (kill-line)
      (yank)
      (newline)
      (yank)
      (move-to-column col)))

  (defun move-line-down ()
    (interactive)
    (let ((col (current-column)))
      (save-excursion
        (forward-line)
        (transpose-lines 1))
      (forward-line)
      (move-to-column col)))

  (defun move-line-up ()
    (interactive)
    (let ((col (current-column)))
      (save-excursion
        (forward-line)
        (transpose-lines -1))
      (forward-line -1)
      (move-to-column col)))
  ;; End from https://krsoninikhil.github.io/2018/12/15/easy-moving-from-vscode-to-emacs/

  (defun cavd-comment-line (arg)
    "Comment or uncomment the current line."
    (interactive "*P")
    (comment-normalize-vars)
    (comment-or-uncomment-region (point-at-bol) (point-at-eol) arg))

  (defun cavd-kill-line ()
    "Kill from point to beginning of line.  Equivalent to (`kill-line' 0)."
    (interactive)
    (kill-line 0))

  (defun cavd-join-to-next-line ()
    "Join line to next line."
    (interactive)
    (join-line -1))

  (defun cavd-line-to-top-of-window ()
    "Move the line point is on to top of window."
    (interactive)
    (recenter 0))

  (defun cavd-line-to-bottom-of-window ()
    "Move the line point is on to bottom of window."
    (interactive)
    (recenter -1))

  (defun cavd-backward-other-window ()
    "Move backward through the windows."
    (interactive)
    (other-window -1))

  (defun cavd-make-param ()
    "Wrap the region in parentheses and prefixed with a function."
    (interactive)
    (let ((cavd-thing
           (read-from-minibuffer
            "Calling what? ")))
      (sp-wrap-with-pair "(")
      (backward-char)
      (insert cavd-thing)))

  (defun delete-this-buffer-and-file ()
    "Removes file connected to current buffer and kills buffer."
    (interactive)
    (let ((filename (buffer-file-name))
          (buffer (current-buffer))
          (name (buffer-name)))
      (if (not (and filename (file-exists-p filename)))
          (error "Buffer '%s' is not visiting a file!" name)
        (when (yes-or-no-p "Are you sure you want to remove this file? ")
          (delete-file filename)
          (kill-buffer buffer)
          (message "File '%s' successfully removed" filename)))))

  (defun cavd-toggle-scroll-conservatively ()
    "Switch between single-line and multi-line
scrolling"
    (interactive)
    (if (= 0 scroll-conservatively)
        (setq scroll-conservatively most-positive-fixnum)
      (setq scroll-conservatively 0)))

  :bind
  ("C-<backspace>" . kill-whole-line)
  ("C-<kp-delete>" . cavd-kill-line)
  ("<C-return>" . open-line-below)
  ("<C-M-return>" . open-line-above)
  ("M-s-d" . duplicate-line)
  ("M-s-j" . move-line-down)
  ("M-s-k" . move-line-up)
  ("C-c C-/" . bjm/align-whatever)
  ("C-^" . cavd-join-to-next-line)
  ("C-c ;" . cavd-comment-line)
  ("s-;" . comment-line)
  ("<f7>" . cavd-line-to-bottom-of-window)
  ("<f8>" . cavd-line-to-top-of-window)
  ("s-}" . other-window)
  ("s-{" . cavd-backward-other-window)
  ("C-c :" . cavd-make-param)
  ("C-c k" . delete-this-buffer-and-file)
  ("s-\\" . cavd-toggle-scroll-conservatively)
  ("C-h z" . cavd-describe-package-at-point))

;; From #emacs
(defun convert-gt-lt-to-angle-brackets (beginning end)
  "< → &lt; and > → &gt;"
  (interactive "*r")
  (setq end (copy-marker end))
  (save-match-data
    (save-excursion
      (goto-char beginning)
      (while (re-search-forward "<\\|>" end t)
        (if (string= (match-string 0) "<")
            (replace-match "&lt;")
          (replace-match "&gt;"))))))

(use-package emacs
  :commands (
             me/date-iso
             me/date-iso-with-time
             me/date-long
             me/date-long-with-time
             me/date-short
             me/date-short-with-time
             )
  :config
  (defun me/date-iso ()
    "Insert the current date, ISO format, eg. 2016-12-09."
    (interactive)
    (insert (format-time-string "%F")))

  (defun me/date-iso-with-time ()
    "Insert the current date, ISO format with time, eg. 2016-12-09T14:34:54+0100."
    (interactive)
    (insert (format-time-string "%FT%T%z")))

  (defun me/date-long ()
    "Insert the current date, long format, eg. December 09, 2016."
    (interactive)
    (insert (format-time-string "%B %d, %Y")))

  (defun me/date-long-with-time ()
    "Insert the current date, long format, eg. December 09, 2016 - 14:34."
    (interactive)
    (insert (capitalize (format-time-string "%B %d, %Y - %H:%M"))))

  (defun me/date-short ()
    "Insert the current date, short format, eg. 2016.12.09."
    (interactive)
    (insert (format-time-string "%Y.%m.%d")))

  (defun me/date-short-with-time ()
    "Insert the current date, short format with time, eg. 2016.12.09 14:34"
    (interactive)
    (insert (format-time-string "%Y.%m.%d %H:%M"))))

(provide 'cavd-misc-funcs)
