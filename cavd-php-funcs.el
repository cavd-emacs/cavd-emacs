(use-package php-mode
  :ensure t
  :commands (
             php-beginning-of-defun
             php-end-of-defun
             php-sniff
             php-lint
             )
  :config
  (defun cavd-php-mode-hook ()
    "Set up some functionality for php mode."
    ;; (php-enable-pear-coding-style)
    (setq-local c-doc-face-name 'font-lock-comment-face)
    (setq c-basic-offset 4
          indent-tabs-mode nil)
    (setq lsp-file-watch-threshold nil)
    (smartparens-mode 1)
    (show-smartparens-mode -1)
    (company-mode t)
    (yas-minor-mode 1))

  (defun php-sniff ()
    "Performs a PHP code sniff on the current file."
    (interactive)
    (let ((compilation-error-regexp-alist '(php))
          (compilation-error-regexp-alist-alist ()))
      (cl-pushnew '(php "\\(syntax error.*\\) in \\(.*\\) on line \\([0-9]+\\)$" 2 3 nil nil 1)
               compilation-error-regexp-alist-alist)
      (compile (concat "phpcs --standard=PEAR --report=emacs \"" (buffer-file-name) "\""))
      (pop-to-buffer "*compilation*")))

  (defun php-lint ()
    "Performs a PHP lint check on the current file."
    (interactive)
    (let ((compilation-error-regexp-alist '(php))
          (compilation-error-regexp-alist-alist ()))
      (cl-pushnew '(php "\\(syntax error.*\\) in \\(.*\\) on line \\([0-9]+\\)$" 2 3 nil nil 1)
               compilation-error-regexp-alist-alist)
      (compile (concat php-executable " -l -f \"" (buffer-file-name) "\""))))

  (setq php-executable (executable-find "php")
        php-manual-path "~/php/php-chunked-xhtml")
  (require 'dap-php)
  (dap-php-setup)

  :bind (:map php-mode-map
              ("C-c C--" . php-current-class)
              ("C-c C-=" . php-current-namespace))
  :hook
  (php-mode . cavd-php-mode-hook)
  (php-mode . lsp-deferred))

(when (executable-find "psysh")
  (use-package psysh
    :hook
    (psysh-mode . cavd-php-mode-hook)
    :bind (:map psysh-mode-map
                ("C-c C-w" . subword-mode)
                ("C-c C-f" . php-search-documentation)
                ("M-<tab>" . php-complete-function)
                ("C-c C-m" . php-browse-manual)
                ("C-." . php-show-arglist))))

;; Define keybindings for php-related modes
(cl-dolist (m '("php-mode" "web-mode" "psysh-mode"))
  (eval-after-load m
    `(progn
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c .")
         nil)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c C-j")
         nil)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c C-w")
         nil)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c C-z")
         nil)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c l")
         'php-lint)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-c C-y")
         'yas/create-php-snippet)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-M-a")
         'php-beginning-of-defun)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "C-M-e")
         'php-end-of-defun)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "M-[")
         'paredit-wrap-square)
       (define-key
         (symbol-value (intern (concat ,m "-map")))
         (kbd "M-{")
         'paredit-wrap-curly))))

(use-package geben
  :after php-mode
  :load-path "~/elisp/old-geben"
  :commands (geben geben-end geben-where))

(provide 'cavd-php-funcs)
