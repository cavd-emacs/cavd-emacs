;;; cavd-emacs --- A .emacs init file

;;; Commentary:

;; Start the initialization process.

;;; Code:
;; Turn off unwanted stuff early

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(setq package-enable-at-startup nil)
;; (add-to-list 'package-archives
;;              '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))
(setq package-archive-priorities
      '(
        ("melpa-stable" . 1)
        ("gnu" . 7)
        ("org" . 7)
        ;; ("marmalade" . 5)
        ("melpa" . 10)))

;; Configure `use-package' prior to loading it.
(eval-and-compile
  (setq use-package-always-ensure nil)
  (setq use-package-always-defer nil)
  (setq use-package-always-demand nil)
  (setq use-package-expand-minimally nil)
  (setq use-package-verbose t)
  (setq use-package-compute-statistics t)
  (setq use-package-enable-imenu-support t))

(eval-when-compile
  (require 'use-package))

(when (eq window-system 'mac)
  (mac-auto-operator-composition-mode)
  (setq mac-system-move-file-to-trash-use-finder t)
  ;; (setq trash-directory  "~/.Trash")
  ;; https://github.com/jdtsmith/ultra-scroll-mac
  (use-package ultra-scroll-mac
    :load-path "~/elisp/ultra-scroll-mac"
    :init
    ;; Speedup cursor movement.
    ;; https://emacs.stackexchange.com/questions/28736/emacs-pointcursor-movement-lag/28746
    (setq auto-window-vscroll nil)
    (setq scroll-step 1
          scroll-conservatively 101     ; important!
          scroll-margin 0)
    :config
    (ultra-scroll-mac-mode 1)))

(set-frame-parameter nil 'fullscreen 'fullboth)

(push "~/.emacs.d/cavd-emacs" load-path)
(require 'tree-sitter)
(add-to-list 'tree-sitter-load-path "~/.emacs.d/tree-sitter/")

(when (or (daemonp)
          (memq window-system '(mac ns x)))
    (setq exec-path-from-shell-arguments nil)
    (exec-path-from-shell-initialize))

(use-package diminish
  :ensure t
  :commands diminish
  :after use-package)

;;
;; Constants, load and exec paths
;;
(defconst cavd-linux-p (or (eq system-type 'gnu/linux)(eq system-type 'linux))
  "Are we running on a GNU/Linux system?")
(defconst cavd-machine (substring (shell-command-to-string "hostname") 0 -1))
(defconst cavd-macosx-p (eq system-type 'darwin))

(defconst cavd-tmp-dir "~/.emacs.tmp")
(defconst cavd-backup-dir (concat cavd-tmp-dir "/backups"))
(defvar home-dir (concat (expand-file-name "~") "/"))
(defvar elisp-dir (concat home-dir "elisp"))
(defvar zsh-executable nil "Location of zsh executable.")

(push elisp-dir load-path)
(push "/usr/local/share/gtags" load-path)

(require 'cavd-local)

(if (not (file-exists-p cavd-backup-dir))
    (make-directory cavd-backup-dir t))

(use-package delsel
  :hook
  (after-init . delete-selection-mode))

(use-package emacs
  :commands (menu-bar-mode tool-bar-mode scroll-bar-mode fringe-mode line-number-mode column-number-mode size-indication-mode mouse-avoidance-mode transient-mark-mode global-font-lock-mode)
  :config
  (fringe-mode '(0 . 0))
  (line-number-mode t)
  (setq line-number-display-limit-width 400)
  (column-number-mode t)
  (size-indication-mode t)
  (set-fringe-mode 0)
  (mouse-avoidance-mode 'exile)
  (transient-mark-mode t)
  (fset 'yes-or-no-p 'y-or-n-p)
  (global-font-lock-mode t)
  (setq-default indent-tabs-mode nil
                blink-cursor-blinks 0
                switch-to-buffer-preserve-window-point t
                pop-up-windows nil
                set-mark-command-repeat-pop t)
  (setq disabled-command-function nil)
  (setq gc-cons-threshold 800001)
  (setq enable-recursive-minibuffers t ;; allow cmds in minibuffer
        history-delete-duplicates t    ;; keep history clean
        kill-do-not-save-duplicates t
        kill-read-only-ok t ;; don't tell me about it
        save-interprogram-paste-before-kill t
        use-dialog-box nil)

  (setq browse-url-browser-function 'browse-url-default-macosx-browser)
  (setq eval-expression-print-length nil)
  (setq vc-make-backup-files nil
        auto-save-default nil
        auto-save-timeout 20 ; number of seconds idle time before auto-save (default: 30)
        auto-save-interval 200 ; number of keystrokes between auto-saves (default: 300)
        )
  (add-function
   :after after-focus-change-function
   (lambda ()
     (if (not (frame-focus-state))
         (garbage-collect))))
  (add-hook 'find-file-hooks 'goto-address-prog-mode)
  :hook
  ((occur . (lambda () (occur-rename-buffer t)))
   (before-save . delete-trailing-whitespace)
   (after-init . blink-cursor-mode)
   (after-init . show-paren-mode)))

(use-package autorevert
  :commands global-auto-revert-mode
  :config
  (setq auto-revert-interval 11)
  (setq auto-revert-check-vc-info nil)
  (global-auto-revert-mode))

(use-package minibuf-eldef
  :commands minibuffer-electric-default-mode
  :init
  (setq minibuffer-eldef-shorten-default t)
  :hook
  (after-init . minibuffer-electric-default-mode))

(use-package emacs
  :config
  (defun cavd-comint-mode-hook ()
    "Stuff to run in comint-mode."
    (define-key comint-mode-map (kbd "<C-return>") #'comint-send-input-stay-on-line))

  (defun comint-send-input-stay-on-line ()
    (interactive)
    (call-interactively 'comint-send-input)
    (run-with-timer 0.05
                    nil
                    (lambda ()  (call-interactively 'comint-show-output))))

  (setq password-cache t)
  (setq password-cache-expiry nil)
  (setq backup-directory-alist
        '(("." . "~/.emacs.tmp/backups")))
  (setq make-backup-files nil)
  (setq backup-by-copying t)
  (setq version-control t)
  (setq delete-old-versions t)
  (setq kept-new-versions 9)
  (setq kept-old-versions 6)
  (setq create-lockfiles nil)
  :hook
  (comint-mode . cavd-comint-mode-hook))

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-selection-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8-unix)

(setq kill-ring-max 149
      load-prefer-newer t
      text-quoting-style 'grave
      require-final-newline t
      zsh-executable (executable-find "zsh"))

(setq ls-lisp-use-insert-directory-program nil)
(require 'ls-lisp)

(use-package server
  :hook
  (after-init . server-start))

;; Uniquify early
(use-package uniquify
  :demand t
  :config
  (setq uniquify-buffer-name-style 'forward))

(use-package winner
  :unless noninteractive
  :commands (winner-redo winner-undo)
  :config
  (setq winner-boring-buffers
        '("*Apropos*" "*Buffer List*" "*Completions*" "*Ibuffer*"))
  :hook
  (after-init . winner-mode))

(use-package buffer-move
  :ensure t
  :bind
  (("C-S-u" . buf-move-up)
   ("C-S-d" . buf-move-down)
   ("C-S-l" . buf-move-left)
   ("C-S-r" . buf-move-right)))

;; minimal version of my Emacs setup which approximates the effective points per inch on your screen
;; and then selects your default font pts based on that
;; Also works on wayland / WSLg because it parses out physical dims from weston.log (necessary in 2023-01)
;; LIMITATION: Will probably not work on multi-monitor setups. Exercise for the reader!
;; BSD 3-clause copyright Charl P. Botha <info@charlbotha.com>

(defun get-mon-attr (which)
  (cdr (assoc which (car (display-monitor-attributes-list)))))

(defun get-monitor-width-mm ()
  (if (getenv "WSL_DISTRO_NAME")
      ;; under WSLg 2021-01-21, physical dimensions are not correctly reported anywhere but weston.log,
      ;; (I hear this is a wayland issue, not just wslg)
      ;; so we use good old awk to get the last occurrence in the log file
      (let* ((awk-dims (shell-command-to-string "awk 'match($0, /physicalWidth:([0-9]+), physicalHeight:([0-9]+)/,a) {width=a[1];height=a[2]} END {print width \",\" height}' /mnt/wslg/weston.log"))
             (splitd (split-string awk-dims ","))
             (width-mm (string-to-number (car splitd))))
        width-mm)
    ;; on other systems, we use whatever Emacs thinks it is
    (car (get-mon-attr 'mm-size))))

(setq monitor-width (get-monitor-width-mm))

(if monitor-width
    (progn
      (setq monitor-hpix (nth 2 (get-mon-attr 'geometry)))
      ;; estimate *effective* PPI (i.e. taking into account scaling / super-sampling)
      (setq ppi-est (/ monitor-hpix (/ monitor-width 25.4)))

      ;; some examples. Adjust the cond below to your taste!
      ;; 4K at 100% because mac: 3840x => 139
      ;; thinkpad 14" at 100%: (/ 1920 (/ 344 24.4)) => 136.186
      ;; m1 mba built-in display: (/ 1440.0 (/ 286 25.4)) => 127.88
      ;; 4K at 125%: (/ 3072.0 (/ 697 25.4)) =>  111.949
      ;; 4K with WACKY 127% macOS scaling 3008x1692: => 109.147
      ;; 27" at 2560x1440 is 108.92 -- font size 130 looks fine there
      (setq my-font-pts (cond
                         ((< ppi-est 130) 140)
                         ((>= ppi-est 130) 160)
                         ((>= ppi-est 109) 150)
                         (t 150))))

  ;; fallback for when we DON'T have monitor-width, e.g. in text mode
;  (setq my-font-pts 140)
  )


;; FONTS =================================================================
;; MONOSPACE FONT
;; mac: brew install font-jetbrains-mono-nerd-font
;; ubu2204: sudo apt install fonts-jetbrains-mono
;; (set-face-attribute 'default nil :family "JetBrains Mono" :height my-font-pts)
;; (set-face-attribute 'fixed-pitch nil :family "JetBrains Mono" :height 1.0) ;; relative to default
;; ;; VARIABLE PITCH FONT
;; (set-face-attribute 'variable-pitch nil :family "JetBrains Mono" :height 1.05)


(require 'fontaine)

(setq fontaine-latest-state-file
      (locate-user-emacs-file "fontaine-latest-state.eld"))

(setq fontaine-presets
      '((small
         :default-family "Menlo"
         :default-height 130
         :variable-pitch-family "Iosevka Comfy Duo")
        (regular) ; like this it uses all the fallback values and is named `regular'
        (work
         :default-weight semilight
         :default-height 140
         :default-family "JetBrains Mono"

         :fixed-pitch-family nil ; falls back to :default-family
         :fixed-pitch-weight nil ; falls back to :default-weight
         :fixed-pitch-height 1.0)
        (medium
         :default-weight semilight
         :default-height 150
         :bold-weight extrabold)
        (large
         :inherit medium
         :default-height 170)
        (presentation
         :default-height 190)
        (t
         :default-family "JetBrains Mono"
         :default-weight regular
         :default-height my-font-pts

         :fixed-pitch-family nil ; falls back to :default-family
         :fixed-pitch-weight nil ; falls back to :default-weight
         :fixed-pitch-height 1.0

         :fixed-pitch-serif-family nil ; falls back to :default-family
         :fixed-pitch-serif-weight nil ; falls back to :default-weight
         :fixed-pitch-serif-height 1.0

         :variable-pitch-family "Iosevka Comfy Motion Duo"
         :variable-pitch-weight nil
         :variable-pitch-height 1.0

         :mode-line-active-family "Menlo" ; falls back to :default-family
         :mode-line-active-weight regular ; falls back to :default-weight
         :mode-line-active-height 1.0

         :mode-line-inactive-family "Menlo" ; falls back to :default-family
         :mode-line-inactive-weight regular ; falls back to :default-weight
         :mode-line-inactive-height 1.0

         :header-line-family nil ; falls back to :default-family
         :header-line-weight nil ; falls back to :default-weight
         :header-line-height 0.9

         :line-number-family nil ; falls back to :default-family
         :line-number-weight nil ; falls back to :default-weight
         :line-number-height 0.9

         :tab-bar-family nil ; falls back to :default-family
         :tab-bar-weight nil ; falls back to :default-weight
         :tab-bar-height 1.0

         :tab-line-family nil ; falls back to :default-family
         :tab-line-weight nil ; falls back to :default-weight
         :tab-line-height 1.0

         :bold-family nil ; use whatever the underlying face has
         :bold-weight bold

         :italic-family nil
         :italic-slant italic

         :line-spacing nil)))

;; Set the last preset or fall back to desired style from `fontaine-presets'
;; (the `regular' in this case).
(fontaine-set-preset (or (fontaine-restore-latest-preset) 'work))

;; Persist the latest font preset when closing/starting Emacs and
;; while switching between themes.
(fontaine-mode 1)

;; fontaine does not define any key bindings.  This is just a sample that
;; respects the key binding conventions.  Evaluate:
;;
;;     (info "(elisp) Key Binding Conventions")
(define-key global-map (kbd "C-c f") #'fontaine-set-preset)

;; (fontaine-restore-latest-preset)

;; ;; Use `fontaine-recovered-preset' if available, else fall back to the
;; ;; desired style from `fontaine-presets'.
;; (if-let ((state fontaine-recovered-preset))
;;     (fontaine-set-preset state)
;;   (fontaine-set-preset 'regular))

(add-hook 'kill-emacs-hook 'fontaine-store-latest-preset)

;; FONTS =================================================================
;; MONOSPACE FONT
;; mac: brew install font-jetbrains-mono-nerd-font
;; ubu2204: sudo apt install fonts-jetbrains-mono
;; (set-face-attribute 'default nil :family "JetBrains Mono" :height my-font-pts)
;; (set-face-attribute 'fixed-pitch nil :family "JetBrains Mono" :height 1.0) ;; relative to default
;; ;; VARIABLE PITCH FONT
;; (set-face-attribute 'variable-pitch nil :family "JetBrains Mono" :height 1.05)


;; Emacs 26 ns port new settings
(when (eq window-system 'ns)
  ;; Will at least display native Unicode emojis if the multicolor font
  ;; patch is applied
  (dolist (pair '((ns-transparent-titlebar . nil)
                  (ns-appearance . dark)))
    (push pair (alist-get 'ns window-system-default-frame-alist nil))
    (set-frame-parameter nil (car pair) (cdr pair)))
  (setq frame-title-format "%b"
        ns-auto-hide-menu-bar t
        ns-use-thin-smoothing t
        ns-use-mwheel-momentum t
        ns-use-mwheel-acceleration t
        ns-use-native-fullscreen nil
        pixel-scroll-mode t
        mouse-wheel-flip-direction t
        mouse-wheel-tilt-scroll t
        ;; MacPorts emacs-app port bug
        x-colors (ns-list-colors))
  (set-frame-parameter nil 'fullscreen 'maximized))

(use-package savehist
  :config
  (setq history-length 300
        savehist-save-minibuffer-history t)
  (setq savehist-additional-variables
        '(kill-ring
          search-ring
          regexp-search-ring
          last-kbd-macro
          kmacro-ring
          shell-command-history
          Info-history-list
          command-history
          mark-ring
          global-mark-ring
          extended-command-history
          ))
  (savehist-mode 1))

;; saveplace: save location in file when saving files
(use-package saveplace
  :config
  (setq save-place-file "~/.emacs.d/saveplace")
  (save-place-mode))

;; abbrevs (abbreviations)
(use-package abbrev
  :commands abbrev-mode
  :diminish
  :init
  (setq abbrev-file-name "~/elisp/abbrev_defs")
  :hook
  (kill-emacs . write-abbrev-file)
  (kill-emacs . package-quickstart-refresh)
  ;; (expand-load
  ;;  . (lambda ()
  ;;      (add-hook 'expand-expand-hook 'indent-according-to-mode)
  ;;      (add-hook 'expand-jump-hook 'indent-according-to-mode)))
  :config
  (setq-default abbrev-mode t
                save-abbrevs t)
  (when (file-exists-p abbrev-file-name)
    (quietly-read-abbrev-file)))

(use-package ffap
  :bind
  ("C-x s-f" . ffap-file-at-point))

(use-package imenu
  :commands (imenu prot/imenu-vertical prot/imenu-recenter-pulse)
  :init
  (setq imenu-use-markers t)
  (setq imenu-auto-rescan t)
  (setq imenu-auto-rescan-maxout 600000)
  (setq imenu-max-item-length 100)
  (setq imenu-use-popup-menu nil)
  (setq imenu-eager-completion-buffer t)
  (setq imenu-space-replacement " ")
  (setq imenu-level-separator "/")

  (defun prot/imenu-vertical ()
    "Use ivy for `imenu'.
Also configure the value of `orderless-matching-styles' to avoid
aggressive fuzzy-style matching for this particular command."
    (interactive)
    (let ((orderless-matching-styles    ; make sure to check `orderless'
           '(orderless-literal
             orderless-regexp
             orderless-prefixes)))
      (call-interactively 'imenu)))

  (defun prot/imenu-recenter-pulse ()
    "Recent `imenu' position at the top with subtle feedback.
Add this to `imenu-after-jump-hook'."
    (let ((pulse-delay .05))
      (recenter 0)
      (pulse-momentary-highlight-one-line (point))))
  :hook
  (imenu-after-jump . prot/imenu-recenter-pulse)
  :bind ("s-." . prot/imenu-vertical))

(use-package flimenu
  :ensure t
  :hook
  (after-init . flimenu-global-mode))

;; Download for dictionary: https://sourceforge.net/projects/aoo-extensions/files/17102/48/dict-en-20190701.oxt/download
(use-package ispell
  :commands (
             ispell
             ispell-minor-mode
             ispell-word
             ispell-region
             ispell-buffer
             )
  :init
  (setenv "DICTIONARY" "en_US")
  (setenv
   "DICPATH"
   (concat (getenv "HOME") "/Library/Spelling"))
  :config
  (when (executable-find "hunspell")
    (setq ispell-program-name (executable-find "hunspell")))
  (ispell-change-dictionary "en_US" t)
  (setq ispell-extra-args '("--sug-mode=ultra"))
  (when (string-match-p "--camel-case"
                        (shell-command-to-string
                         (concat ispell-program-name " --help")))
    (push "--camel-case" ispell-extra-args)))

(use-package flyspell
  :commands (flyspell-mode)
  :init
  (setq flyspell-sort-corrections t)
  (defun flyspell-ivy-prompt (event poss word)
    "A textual flyspell popup menu."
    (let* ((corrects (if flyspell-sort-corrections
                         (sort (car (cdr (cdr poss))) 'string<)
                       (car (cdr (cdr poss)))))
           (cor-menu (if (consp corrects)
                         (mapcar (lambda (correct)
                                   (list correct correct))
                                 corrects)
                       '()))
           (affix (car (cdr (cdr (cdr poss)))))
           show-affix-info
           (base-menu
            (let ((save (if (and (consp affix) show-affix-info)
                            (list
                             (list (concat "Save affix: " (car affix))
                                   'save)
                             '("Accept (session)" session)
                             '("Accept (buffer)" buffer))
                          '(("Save word" save)
                            ("Accept (session)" session)
                            ("Accept (buffer)" buffer)))))
              (if (consp cor-menu)
                  (append cor-menu (cons "" save))
                save)))
           (menu (mapcar
                  (lambda (arg) (if (consp arg) (car arg) arg))
                  base-menu)))
      (cadr (assoc (ivy-read "%d Choose: " menu) base-menu))))
  (setq flyspell-issue-message-flag nil)
  (setq flyspell-issue-welcome-flag nil)
  :hook
  (text-mode . turn-on-flyspell)
  (prog-mode . turn-off-flyspell))

(use-package flyspell-correct
  :ensure t
  :after flyspell
  :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper)))

(use-package windmove
  :commands (windmove-down windmove-left windmove-right windmove-up)
  :config
  (windmove-default-keybindings))

(use-package shr
  :commands (shr-render-buffer shr-render-region)
  :config
  (setq shr-use-fonts nil)
  (setq shr-use-colors nil)
  (setq shr-max-image-proportion 0.7)
  (setq shr-image-animate nil)
  (setq shr-width (current-fill-column)))

(defvar cavd-secrets-file "~/.emacs.d/cavd-emacs/cavd-secrets.el.gpg"
  "File containing secrets.")

(if (file-exists-p cavd-secrets-file)
    (load-library cavd-secrets-file))

(setq message-log-max t
      comint-prompt-read-only t)

(use-package auth-source
  :init
  (defun cavd-auth-get-field (host user prop)
    (let ((attr
           (plist-get
            (nth 0
                 (auth-source-search :host host
                                     :max 1
                                     :user user))
            prop)))
      (if (functionp attr)
          (funcall attr)
        attr)))
  :config
  (setq auth-sources '("~/.authinfo.gpg" "~/.authinfo"))
  (setq user-full-name "Chris Van Dusen"))

(use-package window
  :bind (("s-n" . next-buffer)
         ("s-p" . previous-buffer)
         ("s-2" . split-window-below)
         ("s-3" . split-window-right)
         ("s-0" . delete-window)
         ("s-1" . delete-other-windows)
         ("s-T" . tear-off-window)))

(use-package csv-mode
  :ensure t)

(use-package dired
  :commands (
             cavd-dired-mac-open-file
             dired-get-file-for-visit
             )
  :hook
  (dired-mode . auto-revert-mode)
  (dired-mode . me/dired-set-ongoing-hydra-body)
  (dired-mode . dired-hide-details-mode)

  :bind (:map dired-mode-map
              ("`" . cavd-dired-dispatch)
              ("C-c o" . cavd-dired-mac-open-file)
              (";" . dired-posframe-mode)
              ("'" . hydra-dired/body)
              ("r" . wdired-change-to-wdired-mode)
              (")" . dired-git-info-mode)
              ("TAB" . dired-subtree-toggle)
              ("<C-tab>" . dired-subtree-cycle)
              ("<S-tab>" . dired-subtree-remove))
  :config
  (add-to-list 'load-path "~/elisp/emacs-async")
  (autoload 'dired-async-mode "dired-async.el" nil t)
  (dired-async-mode 1)
  (defun me/dired-set-ongoing-hydra-body ()
    (setq me/ongoing-hydra-body #'hydra-dired/body))

  (defun cavd-dired-mac-open-file ()
    (interactive)
    (let ((file (dired-get-file-for-visit)))
      (shell-command (concat (executable-find "open") " " file))))

  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  (setq dired-listing-switches "-aDlh --group-directories-first")

  (setq dired-isearch-filenames 'dwim)
  (setq dired-dwim-target t)
  (setq dired-do-revert-buffer t)
  (setq dired-vc-rename-file t)
  (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil))

(use-package diredfl
  :ensure t
  :hook
  (after-init . diredfl-global-mode))

(use-package wdired
  :commands wdired-change-to-wdired-mode
  :config
  (setq wdired-allow-to-change-permissions 'advanced))

(use-package dired-git-info
  :ensure t
  :commands dired-git-info-mode
  :custom
  (dgi-commit-message-format "%cr	%s"))

(use-package dired-rsync
  :ensure t
  :commands dired-rsync)

(use-package dired-subtree
  :commands (
             dired-subtree-toggle
             dired-subtree-cycle
             dired-subtree-remove))

(use-package dired-x
  :commands (dired-jump dired-jump-other-window))

;; (require 'cc-dired-sort-by)
;; (define-key dired-mode-map (kbd "s") 'cc/dired-sort-by)

(use-package tramp
  :defer t
  :config
  (setq enable-remote-dir-locals t)
  (setq tramp-default-method "ssh")
  (tramp-set-completion-function
   "ssh"
   '((tramp-parse-sconfig "/etc/ssh_config")
     (tramp-parse-sconfig "~/.ssh/config")))
;  (require 'tramp-updates)
  (require 'tramp-container)
  (remove-hook 'tramp-cleanup-connection-hook 'tramp-recentf-cleanup))

(use-package recentf
  :config
  (setq recentf-auto-cleanup 'never
        recentf-save-file "~/.emacs.d/recentf"
        recentf-max-saved-items 131
        recentf-max-menu-items 11
        recentf-case-fold-search t
        recentf-show-file-shortcuts-flag nil)
  (add-to-list 'recentf-arrange-rules '("PHP files (%d)" ".\\.php\\'"))
  (add-to-list 'recentf-arrange-rules '("SQL files (%d)" ".\\.sql\\'"))
  (add-to-list 'recentf-arrange-rules '("Ruby files (%d)" ".\\.rb\\'"))
  (remove-hook 'tramp-cleanup-connection-hook 'tramp-recentf-cleanup)
  :hook
  (after-init . recentf-mode))

(use-package ibuffer
  :commands (ibuffer cavd-ibuffer-vc)
  :init
  (defun cavd-ibuffer-vc ()
    "Sort buffers by their vc root."
    (interactive)
    (setq ibuffer-formats
          '((mark modified read-only vc-status-mini " "
                  (name 18 18 :left :elide)
                  " "
                  (size 9 -1 :right)
                  " "
                  (mode 16 16 :left :elide)
                  " "
                  (vc-status 16 16 :left)
                  " "
                  filename-and-process)))
    (ibuffer-vc-set-filter-groups-by-vc-root))

  (setq ibuffer-show-empty-filter-groups nil)
  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 18 18 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " " filename-and-process)
          (mark " "
                (name 16 -1)
                " " filename)))
  (setq ibuffer-saved-filter-groups
        '(("default"
           ("Commands" (or
                        (mode . shell-mode)
                        (mode . eshell-mode)
                        (mode . term-mode)
                        (mode . compilation-mode)))
           ("Dired" (mode . dired-mode))
           ("Emacs" (or
                     (name . "^\\*scratch\\*$")
                     (name . "^\\*Messages\\*$")
                     (name . "^\\*Customize Group")
                     (name . "^\\*Apropos\\*$")))
           ("ERC" (mode . erc-mode))
           ("Lisp" (or (mode . lisp-mode) (mode . emacs-lisp-mode)))
           ("Org" (or
                   (name . "^\\*Calendar\\*$")
                   (name . "^diary$")
                   (mode . org-mode)))
           ("PHP" (mode . php-mode))
           ("Ruby" (or (mode . enh-ruby-mode) (mode . ruby-mode)))
           ("Shell" (mode . sh-mode))
           ("SQL" (or (mode . sql-mode)))
           ("Text" (or (mode . csv-mode) (mode . text-mode) (mode . fundamental-mode)))
           ("VCS" (or
                   (mode . magit-status-mode)
                   (mode . magit-log-mode)
                   (mode . magit-diff-mode)
                   (mode . vc-diff-mode)))
                                        ;("W3M" (mode . w3m-mode))
           )
          ("Web" (mode . web-mode))
          ("by-case"
           ("lower" (name . "\\`[^[:upper:]]*\\'"))
           ("Upper" (name . "[[:upper:]]")))))

  :bind (("<f6>" . ibuffer)
         :map ibuffer-mode-map
         ("'" . ibuffer-projectile-set-filter-groups)
         ("* f" . ibuffer-mark-by-file-name-regexp)
         ("* g" . ibuffer-mark-by-content-regexp) ; "g" is for "grep"
         ("* n" . ibuffer-mark-by-name-regexp)
         ("s n" . ibuffer-do-sort-by-alphabetic)  ; "sort name" mnemonic
         ("s p" . ibuffer-do-sort-by-mode-and-filename)
         ("s r" . ibuffer-do-sort-by-recency)
         ("s v" . cavd-ibuffer-vc))
  :hook
  (ibuffer-mode . (lambda ()
                    (define-ibuffer-sorter mode-and-filename
                      "Sort the buffers by their mode and pathname."
                      (:description "mode plus filenames")
                      (string-lessp
                       (with-current-buffer (car a)
                         (or (concat (symbol-name major-mode) buffer-file-name)
                             (if (eq major-mode 'dired-mode)
                                 (concat (symbol-name major-mode) (expand-file-name dired-directory)))
                             ;; so that all non pathnames are at the end
                             "~"))
                       (with-current-buffer (car b)
                         (or (concat (symbol-name major-mode) buffer-file-name)
                             (if (eq major-mode 'dired-mode)
                                 (concat (symbol-name major-mode) (expand-file-name dired-directory)))
                             ;; so that all non pathnames are at the end
                             "~"))))))
  (ibuffer-mode . ibuffer-auto-mode)
  (ibuffer-mode . hl-line-mode))

(use-package isearch
  :commands (prot/isearch-mark-and-exit prot/isearch-other-end)
  :init
  (setq isearch-allow-scroll t)
  (setq regexp-search-ring-max 23)
  (setq search-ring-max 23)
  (setq isearch-lazy-count t)

  (defun prot/isearch-mark-and-exit ()
    "Mark the current search string and exit the search."
    (interactive)
    (push-mark isearch-other-end t 'activate)
    (setq deactivate-mark nil)
    (isearch-done))

  (defun prot/isearch-other-end ()
    "End current search in the opposite side of the match.
Particularly useful when the match does not fall within the
confines of word boundaries (e.g. multiple words)."
    (interactive)
    (isearch-done)
    (when isearch-other-end
      (goto-char isearch-other-end)))
  :bind
  (:map isearch-mode-map
        ("M-y" . isearch-yank-pop)
        ("C-SPC" . prot/isearch-mark-and-exit)
        ("<C-return>" . prot/isearch-other-end)))

(use-package emacs
  :init
  (setq list-matching-lines-jump-to-current-line t)
  :hook ((occur-mode . hl-line-mode)
         (occur-mode . (lambda ()
                         (toggle-truncate-lines t))))
  :bind (("M-s M-o" . multi-occur)
         :map occur-mode-map
         ("t" . toggle-truncate-lines)))

(use-package eldoc
  :diminish
  :config
  (eldoc-add-command 'magit-next-line)
  (eldoc-add-command 'magit-previous-line)
  :hook
  (after-init . global-eldoc-mode))

(use-package bookmark
  :commands (bookmark-jump bookmark-set bookmark-bmenu-list)
  :config
  (setq bookmark-save-flag 1))

(use-package calendar
  :config
  (setq calendar-latitude 32.7
        calendar-longitude -96.8)
  (setq diary-file  "~/elisp/diary"
        calendar-location-name "Richardson"
        calendar-date-style 'iso)
  (setq calendar-mark-diary-entries-flag t)
  (setq calendar-time-display-form
        '(24-hours ":" minutes
                   (when time-zone
                     (concat " (" time-zone ")"))))
  (setq calendar-date-style 'iso)
  (setq calendar-holidays
        (append holiday-general-holidays holiday-local-holidays
                holiday-other-holidays holiday-christian-holidays
                holiday-solar-holidays))
  :bind
  ("s-c" . calendar)
  :hook
  (calendar-today-visible . calendar-mark-today))

(use-package time
  :config
  (setq display-time-default-load-average nil
        display-time-day-and-date  t
        display-time-mail-string   ""
        display-time-24hr-format   t
        display-time-format        nil
        display-time-use-mail-icon nil)
  :hook
  (after-init . display-time-mode))

(use-package apropos
  :commands (apropos apropos-command apropos-variable)
  :config
  (setq apropos-do-all t))

(use-package hcl-mode
  :ensure
  :config
  (setq hcl-indent-level 2))

(use-package info-look
  :commands (info-lookup-file info-lookup-symbol))

(use-package make-mode
  :init
  (add-to-list 'auto-mode-alist '("Makefile\\.*" . makefile-mode)))

(use-package password-store
  :ensure t
  :commands (password-store-copy
             password-store-edit
             password-store-insert)
  :config
  (setq password-store-time-before-clipboard-restore 45))

(use-package pass
  :ensure t
  :commands pass)

(use-package pinentry
  :ensure t
  :hook
  (after-init . pinentry-start))

(use-package paredit
  :ensure t
  :diminish
  :bind
  ("C-c M-w" . paredit-copy-as-kill)
  :bind (:map paredit-mode-map
              ("RET" . nil)
              ("M-s" . nil)
              ("M-|" . paredit-split-sexp)
              ("M-;" . nil)
              ("C-c M-w" . paredit-copy-as-kill)
              ("{" . paredit-open-curly)
              ("}" . paredit-close-curly)
              (")" . paredit-close-round-and-newline)
              ("M-)" . paredit-close-round)
              ("M-[" . paredit-wrap-square)
              ("M-{" . paredit-wrap-curly)
              ("M-;" . comment-dwim)
              ("s-;" . paredit-comment-dwim))
  :config
  (defun paredit-check-region-for-yank ()
    "Run after yank and yank-pop to verify balanced parens."
    (when paredit-mode
      (save-excursion
        (save-restriction
          (narrow-to-region (point) (mark))
          (condition-case nil
              (check-parens)
            (error
             (if (not (y-or-n-p "The text inserted has unbalanced parentheses, continue? "))
                 (delete-region (point-min) (point-max)))))))))

  (setq paredit-space-for-delimiter-predicates
        '((lambda (e d)
            (member
             (buffer-local-value 'major-mode (current-buffer))
             '(emacs-lisp-mode)))))
  (defadvice yank (after yank-check-parens activate)
    (paredit-check-region-for-yank))
  (defadvice yank-pop (after yank-pop-check-parens activate)
    (paredit-check-region-for-yank)))

(use-package ace-window
  :ensure t
  :commands (aw-flip-window
             enlarge-window-horizontally
             enlarge-window
             shrink-window-horizontally
             shrink-window)
  :config
  (defun joe-scroll-other-window()
    (interactive)
    (scroll-other-window 1))

  (defun joe-scroll-other-window-down ()
    (interactive)
    (scroll-other-window-down 1))

  (setq aw-dispatch-always nil
        aw-scope 'frame
        aw-minibuffer-flag t
        aw-ignore-current nil
        aw-display-mode-overlay t
        aw-background t)
  (add-to-list 'aw-dispatch-alist
               '(?b balance-windows))
  (add-to-list 'aw-dispatch-alist
               '(?s aw-swap-window " Ace - Swap Window"))

  (when (package-installed-p 'hydra)
    (defhydra hydra-window-size (:color red)
      "Windows size"
      ("h" shrink-window-horizontally "shrink horizontal")
      ("j" shrink-window "shrink vertical")
      ("k" enlarge-window "enlarge vertical")
      ("l" enlarge-window-horizontally "enlarge horizontal"))
    (defhydra hydra-window-frame (:color red)
      "Frame"
      ("f" make-frame "new frame")
      ("x" delete-frame "delete frame"))
    (defhydra hydra-window-scroll (:color red)
      "Scroll other window"
      ("n" joe-scroll-other-window "scroll")
      ("p" joe-scroll-other-window-down "scroll down"))
    (add-to-list 'aw-dispatch-alist '(?w hydra-window-size/body) t)
    (add-to-list 'aw-dispatch-alist '(?o hydra-window-scroll/body) t)
    (add-to-list 'aw-dispatch-alist '(?\; hydra-window-frame/body) t))

  :hook
  (after-init . ace-window-display-mode)
  :bind
  ("M-o" . ace-window))

(use-package avy
  :ensure t
  :commands (avy-next avy-prev)
  :bind
  ("M-g 1" . avy-goto-word-1)
  ("M-g 2" . avy-goto-char-2)
  ("M-g l" . avy-goto-line)
  ("M-g c" . avy-goto-char)
  ("M-g e" . avy-goto-word-0)
  ("M-g b" . avy-pop-mark)
  ("M-g r" . avy-resume)
  ("M-g t" . avy-goto-char-timer)
  ("M-i" . avy-goto-char-in-line))

(use-package image-mode
  :bind (:map image-mode-map
              ("'" . hydra-image/body)))

(use-package terraform-mode
  :ensure t
  :custom
  (setq terraform-indent-level 2)
  :config
  (defun cavd-terraform-mode-init ()
    (outline-minor-mode 1)
    )
  :hook
  (terraform-mode . cavd-terraform-mode-init))

(use-package vagrant-tramp
  :ensure t
  :commands vagrant-tramp-term)

;;
;; Requires
;;
(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (cmake "https://github.com/uyha/tree-sitter-cmake")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (go "https://github.com/tree-sitter/tree-sitter-go")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (php "https://github.com/tree-sitter/tree-sitter-php")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (ruby "https://github.com/tree-sitter/tree-sitter-ruby")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

(use-package eglot
  ;; The first 5 bindings aren't needed here, but are a good
  ;; reminder of what they are bound too
  :bind (("M-TAB" . completion-at-point)
         ("M-g i" . imenu)
         ("C-h ." . display-local-help)
         ("M-." . xref-find-definitions)
         ("M-," . xref-go-back)
         :map
         eglot-mode-map
         ("C-c c a" . eglot-code-actions)
         ("C-c c o" . eglot-code-actions-organize-imports)
         ("C-c c r" . eglot-rename)
         ("C-c c f" . eglot-format))
  :config
  (defvar complete-at-point--timer nil "Timer for triggering complete-at-point.")

  (defun auto-complete-at-point (&rest _)
    "Set a time to complete the current symbol at point in 0.1 seconds"
    (when (and (not (minibufferp)))
      ;; If a user inserts a character while a timer is active, reset
      ;; the current timer
      (when (timerp complete-at-point--timer)
        (cancel-timer complete-at-point--timer))
      (setq complete-at-point--timer
            (run-at-time 0.2 nil
                         (lambda ()
                           ;; Clear out the timer and run
                           ;; completion-at-point
                           (when (timerp complete-at-point--timer)
                             (cancel-timer complete-at-point--timer))
                           (setq complete-at-point--timer nil)
                           (completion-at-point))))))
  ;; Add a hook to enable auto-complete-at-point when eglot is enabled
  ;; this allows use to remove the hook on 'post-self-insert-hook if
  ;; eglot is disabled in the current buffer
  (add-hook 'eglot-managed-mode-hook (lambda ()
                                       (if eglot--managed-mode
                                           (add-hook 'post-self-insert-hook #'auto-complete-at-point nil t)
                                         (remove-hook 'post-self-insert-hook #'auto-complete-at-point t)))))

(require 'cavd-misc-funcs)
(require 'cavd-init-shell)
(require 'cavd-js-funcs)
(require 'cavd-php-funcs)
(require 'cavd-erc-funcs)
(require 'cavd-sql-funcs)
(require 'cavd-ruby-funcs)
(require 'cavd-elisp-funcs)
(when (or (executable-find "dx86cl64")
          (executable-find "sbcl"))
  (require 'cavd-lisp-funcs))
(require 'cavd-docker)
(require 'cavd-completions)
(require 'cavd-ivy-counsel-swiper)
;(require 'cavd-consult-embark-vertico)
(require 'cavd-vcs)
(require 'cavd-keybinds)
(when (executable-find "pytunes")
    (require 'cavd-music))
;(require 'cavd-mu4e)
;(require 'cavd-nov)

(use-package apache-mode
  :ensure t)

(use-package beginend
  :ensure t
  :diminish
  :commands beginend-global-mode
  :init
  (diminish 'beginend-global-mode "")
  (diminish 'beginend-prog-mode "")
  :hook
  (after-init . beginend-global-mode))

(use-package css-mode
  :config
  (defun cavd-css-mode-hook ()
    "Do some stuff when entering css-mode."
    (rainbow-mode)
    (smartparens-mode))
  (setq css-indent-offset 2)
  :mode
  ("\\.css\\.erb$" . css-mode)
  :hook
 (css-mode . cavd-css-mode-hook))

(use-package browse-at-remote
  :ensure t
  :bind
  ("C-c S-b" .  'browse-at-remote)
  ("C-c S-k" .  'browse-at-remote-kill))

(use-package browse-kill-ring
  :ensure t
  :commands browse-kill-ring-default-keybindings
  :hook
  (after-init . browse-kill-ring-default-keybindings))

(use-package eww
  :config
  (defun prot-eww--rename-buffer ()
    "Rename EWW buffer using page title or URL.
To be used by `eww-after-render-hook'."
    (let ((name (if (eq "" (plist-get eww-data :title))
                    (plist-get eww-data :url)
                  (plist-get eww-data :title))))
      (rename-buffer (format "*%s # eww*" name) t)))
  (advice-add 'eww-back-url :after #'prot-eww--rename-buffer)
  (advice-add 'eww-forward-url :after #'prot-eww--rename-buffer)

  (defun eww-more-readable ()
    "Makes eww more pleasant to use.
Run it after eww buffer is loaded."
    (interactive)
    (setq eww-header-line-format nil)               ;; removes page title
    (setq mode-line-format nil)                     ;; removes mode-line
    (set-window-margins (get-buffer-window) 20 20)  ;; increases size of margins
    (redraw-display)                                ;; apply mode-line changes
    (eww-reload 'local))                            ;; apply eww-header changes

  (defun prot-eww-browse-dwim (url &optional arg)
    "Visit a URL, maybe from `eww-prompt-history', with completion.

With optional prefix ARG (\\[universal-argument]) open URL in a
new eww buffer.

If URL does not look like a valid link, run a web query using
`eww-search-prefix'.

When called from an eww buffer, provide the current link as
initial input."
    (interactive
     (list
      (completing-read "Run EWW on: " eww-prompt-history
                       nil nil (plist-get eww-data :url) 'eww-prompt-history)
      current-prefix-arg))
    (eww url (if arg 4 nil)))

  :bind
  ("C-c w" . prot-eww-browse-dwim)
  (:map eww-mode-map
        ("'" . hydra-eww/body))
  :hook
  (eww-after-render . prot-eww--rename-buffer))

(use-package expand-region
  :ensure t
  :commands (er/expand-region er/contract-region)
  :config
  (require 'yaml-mode-expansions)
  :bind
  ("C->" . er/expand-region)
  ("C-<" . er/contract-region))

(use-package expreg
  :ensure t
  :bind
  ("s->" . expreg-expand)
  ("s-<" . expreg-contract))

(use-package nxml-mode
  :config
  (defun cavd-nxml-mode-hook ()
    (turn-off-show-smartparens-mode))
  :hook
  (nxml-mode . cavd-nxml-mode-hook))

(use-package scratch
  :ensure t
  :config
  (defun prot/scratch-buffer-setup ()
    "Add contents to `scratch' buffer and name it accordingly."
    (let* ((mode (format "%s" major-mode))
           (string (concat "Scratch buffer for: " mode "\n\n")))
      (when scratch-buffer
        (save-excursion
          (insert string)
          (goto-char (point-min))
          (comment-region (point-at-bol) (point-at-eol)))
        (forward-line 2))
      (rename-buffer (concat "*Scratch for " mode "*") t)))
  :hook (scratch-create-buffer . prot/scratch-buffer-setup)
  :bind ("C-c s" . scratch))

(use-package ts
  :defer t
  :ensure t)

(use-package web-mode
  :ensure t
  :after company
  :preface
  (defun cavd-web-mode-hook ()
    "Set up some functionality for web mode."
    (smartparens-mode 1)
    (add-to-list 'company-backends 'company-web-html)
    (company-mode t)
    (yas-minor-mode 1))
  :mode
  ((("\\.erb\\'" . web-mode))
   (("\\.ctp\\'" . web-mode))
   (("\\.twig\\'" . web-mode)))
  :hook
  (web-mode . cavd-web-mode-hook)
  (web-mode . web-mode-edit-element-minor-mode)

  :config
  (make-local-variable 'web-mode-code-indent-offset)
  (make-local-variable 'web-mode-markup-indent-offset)
  (make-local-variable 'web-mode-css-indent-offset)
  (setq-default web-mode-code-indent-offset 2
                web-mode-css-indent-offset 2
                web-mode-markup-indent-offset 2
                web-mode-enable-auto-indentation nil)
  (setq web-mode-indent-style 2
        web-mode-content-types-alist '(("jsx"  . ".*\\.js[x]?\\'"))))

(use-package which-key
  :ensure t
  :diminish
  :commands which-key-mode
  :custom
  (which-key-show-early-on-C-h t)
  (which-key-idle-delay 3)
  (which-key-idle-secondary-delay 0.05)
  (which-key-popup-type 'side-window)
  :hook
  (after-init . which-key-mode))

;;
;; Whitespace
;;
(use-package whitespace
  :diminish
  :config
  (setq whitespace-style
        '(trailing space-before-tab
                   indentation space-after-tab)
        show-trailing-whitespace t)
  (setq show-trailing-whitespace t)
  :hook
  (after-init . whitespace-mode))

;; Installed from https://github.com/myrkr/dictionary-el
(use-package dictionary
  :commands (
             dictionary
             dictionary-close
             dictionary-lookup-definition
             dictionary-new-search
             dictionary-search
             )
  :ensure t)

;; Synosaurus
(use-package synosaurus
  :commands (synosaurus-lookup synosaurus-choose-and-replace)
  :config
  (defun synosaurus-choose (list)
    "Choose among a `LIST' of values."
    (let ((completion-prompt "Replacement: "))
      (completing-read completion-prompt list))))

(use-package newcomment
  :commands (comment-box prot/comment-dwim)
  :init
  ;; 2020-12-28 Trying the following
  (setq comment-multi-line t)
  (setq comment-style 'indent)

  (defun prot/comment-dwim (&optional arg)
    "Alternative to `comment-dwim': offers a simple wrapper
around `comment-line' and `comment-dwim'.

If the region is active, then toggle the comment status of the
region or, if the major mode defines as much, of all the lines
implied by the region boundaries.

Else toggle the comment status of the line at point."
    (interactive "*P")
    (if (use-region-p)
        (comment-dwim arg)
      (save-excursion
        (comment-line arg))))

  :bind (("C-;" . prot/comment-dwim)
         ("C-x C-;" . comment-box)))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-at-point helpful-function)
  :bind
  (("C-h s-f" . helpful-callable)
   ("C-h s-v" . helpful-variable)
   ("C-h s-k" . helpful-key)
   ("C-h s-p" . helpful-at-point)
   ("C-h F" . helpful-function)
   ("C-h s-c" . helpful-command)))

;; (use-package eyebrowse
;;   :ensure t
;;   :hook
;;   (after-init . eyebrowse-mode)
;;   :custom
;;   (eyebrowse-new-workspace t))

(use-package link-hint
  :commands (link-hint-copy-link link-hint-open-link)
  :ensure t
  :bind
  ("M-s-c" . link-hint-copy-link)
  ("M-s-o" . link-hint-open-link))

;; wgrep
(use-package wgrep
  :ensure t
  :commands (
             wgrep
             wgrep-change-to-wgrep-mode
             wgrep-finish-edit
             )
  :init
  (setq wgrep-auto-save-buffer t)
  (setq wgrep-change-readonly-file t)
  :bind (:map grep-mode-map
              ("e" . wgrep-change-to-wgrep-mode)
              ("C-x C-q" . wgrep-change-to-wgrep-mode)
              ("C-c C-c" . wgrep-finish-edit)))

(use-package quick-buffer-switch
  :init
  (setq qbs-prefix-key "C-x C-b")
  (qbs-init))

(when (executable-find "rg")
  (use-package rg
    :ensure
    :commands (
               rg
               prot/rg-ref-in-dir
               prot/rg-save-search-as-name
               prot/rg-vc-or-dir
               )
    :init
    (setq rg-group-result t)
    (setq rg-hide-command t)
    (setq rg-show-columns nil)
    (setq rg-show-header t)
    (setq rg-custom-type-aliases nil)
    (setq rg-default-alias-fallback "all")

    (rg-define-search prot/rg-vc-or-dir
      "RipGrep in project root or present directory."
      :query ask
      :format regexp
      :files "everything"
      :dir (let ((vc (vc-root-dir)))
             (if vc
                 vc                         ; search root project dir
               default-directory))          ; or from the current dir
      :confirm prefix
      :flags '("--hidden -g !.git"))

    (rg-define-search prot/rg-ref-in-dir
      "RipGrep for thing at point in present directory."
      :query point
      :format regexp
      :files "everything"
      :dir default-directory
      :confirm prefix
      :flags '("--hidden -g !.git"))

    (defun prot/rg-save-search-as-name ()
      "Save `rg' buffer, naming it after the current search query.

This function is meant to be mapped to a key in `rg-mode-map'."
      (interactive)
      (let ((pattern (car rg-pattern-history)))
        (rg-save-search-as-name (concat "«" pattern "»"))))

    :bind
    (("M-g v" . prot/rg-vc-or-dir)
     ("M-g ." . prot/rg-ref-in-dir)
     :map rg-mode-map
     ("s" . prot/rg-save-search-as-name)
     ("C-n" . forward-line)
     ("C-p" . previous-line)
     ("M-n" . rg-next-file)
     ("M-p" . rg-prev-file))))

(when rg-executable
  (use-package deadgrep
    :ensure t
    :bind
    ("s-d" . deadgrep))

  (use-package ripgrep
    :hook
    (ripgrep-search-mode . hl-line-mode)))

;; Ag settings
(when (executable-find "ag")
  (use-package ag
    :ensure t
    :commands (ag-files ag-regexp)
    :bind
    ("M-s a" . ag-files)
    ("M-s x" . ag-regexp)
    :config
    (setq ag-reuse-window t)
    :hook
    (ag-mode . wgrep-ag-setup))

  (use-package wgrep-ag
    :ensure t
    :after ag))

(use-package transpose-frame
  :ensure t
  :commands (transpose-frame
             flip-frame
             flop-frame
             rotate-frame
             rotate-frame-clockwise
             rotate-frame-anticlockwise)
  :bind (("C-s-t" . flop-frame) ; what I consider "transpose" in this context
         ("C-s-c" . rotate-frame-clockwise)))

(use-package shackle
  :ensure t
  :config
  (setq shackle-rules
   '(((help-mode helpful-mode) :select t)
     (sql-interactive-mode :same t :select t)
     ("*Completions*" :select t)
     (compilation-mode :select t)
     (vterm-mode :same t :select t)))
  (setq shackle-select-reused-windows t)
  :hook
  (after-init . shackle-mode))

(use-package define-word
  :ensure t
  :commands (define-word define-word-at-point))

;; Following Requires npm install -g yaml-language-server
(use-package yaml-mode
  :ensure t
  :mode "\\.\\(e?ya?\\|ra\\)ml\\.sample\\'"
  :config
  (defun cavd-yaml-mode-hook ()
    "Code to run in lsp-mode."
    (when (executable-find "yaml-language-server")
      (smartparens-mode)
      (setq lsp-yaml-single-quote t)
        ;(lsp-deferred)
      ))

  (setq yaml-indent-offset 2)
  :hook
  (yaml-mode . cavd-yaml-mode-hook))

;; Org mode
(use-package org
  :bind
  (:map org-mode-map
        ([remap backward-paragraph] . me/backward-paragraph-dwim)
        ([remap forward-paragraph] . me/forward-paragraph-dwim)
        ("<C-return>" . nil)
        ("<C-S-down>" . nil)
        ("<C-S-up>" . nil)
        ("<M-S-down>" . nil)
        ("<M-S-up>" . nil))
  :hook
  ((org-mode . me/org-set-ongoing-hydra-body)
   (org-mode . turn-on-font-lock))
  :config
  (defun me/org-src-buffer-name (org-buffer-name language)
    "Construct the buffer name for a source editing buffer. See
`org-src--construct-edit-buffer-name'."
    (format "*%s*" org-buffer-name))
  (define-key org-mode-map (kbd "C-c C-r") verb-command-map)
  (defun me/org-set-ongoing-hydra-body ()
    (setq me/ongoing-hydra-body #'hydra-org/body))

  (setq org-log-done t)
  (setq org-todo-keywords
        (list
         (cons 'sequence
               (cl-pushnew
                "IN-PROGRESS"
                (cl-rest (car org-todo-keywords))))))
  (setq org-todo-keyword-faces
        '(("IN-PROGRESS" . "yellow") ("TODO" . "red") ("DONE" . "green")))
  (setq org-link-descriptive nil)
  (setq org-edit-src-content-indentation 0)
  (setq org-edit-src-persistent-message nil)
  (setq org-fontify-done-headline t)
  (setq org-fontify-quote-and-verse-blocks t)
  (setq org-src-window-setup 'current-window)
  (setq org-startup-folded nil)
  (setq org-startup-truncated nil)
  (setq org-support-shift-select 'always)
  (advice-add 'org-src--construct-edit-buffer-name :override #'me/org-src-buffer-name)
  (add-to-list 'org-babel-load-languages '("ruby" . ruby-mode)))

;;
;; PDF Tools
;;
(defvar cavd-pdf-tools-path nil "The path to pdf-tools.")

(when (executable-find "epdfinfo")
  (use-package pdf-tools
    :load-path "/usr/local/share/emacs/site-lisp/pdf-tools"
    :mode
    ("\\.\\(?:PDF\\|DVI\\|OD[FGPST]\\|DOCX?\\|XLSX?\\|PPTX?\\|pdf\\|djvu\\|dvi\\|od[fgpst]\\|docx?\\|xlsx?\\|pptx?\\)\\'" . pdf-view-mode)
    :init
    (setq pdf-info-epdfinfo-program (executable-find "epdfinfo"))))

(use-package calculator
  :bind
  ("C-x s-c" . calculator))

(use-package hi-lock
  :commands global-hi-lock-mode
  :hook
  (after-init . global-hi-lock-mode))

(electric-indent-mode -1)

;; Load customizations
(setq custom-file
      "~/.emacs.d/cavd-emacs/custom.el")

(load custom-file)

;; (whole-line-or-region-mode 1)

(use-package projectile
  :ensure t
  :diminish
  :commands projectile-mode
  ;; :init
  :config
  (load-file "~/.emacs.d/cavd-emacs/+projectile-find-file.el")
  (setq projectile-sort-order 'recentf)
  (setq projectile-enable-caching t)
  ;;; Default rg arguments
  ;; From https://github.com/kaushalmodi/.emacs.d/blob/master/setup-files/setup-projectile.el
  (when rg-executable
    (progn
      (defconst modi/rg-arguments
        `("--line-number"                     ; line numbers
          "--smart-case"
          "--follow"                          ; follow symlinks
          "--mmap")                           ; apply memory map optimization when possible
        "Default rg arguments used in the functions in `projectile' package.")

      (defun modi/advice-projectile-use-rg (&rest _args)
        "Always use `rg' for getting a list of all files in the project."
        (let* ((prj-user-ignore-name (expand-file-name
                                      (concat ".ignore." user-login-name)
                                      (projectile-project-root)))
               (prj-user-ignore (when (file-exists-p prj-user-ignore-name)
                                  (concat "--ignore-file " prj-user-ignore-name))))
          (mapconcat #'shell-quote-argument
                     (if prj-user-ignore
                         (append '("rg")
                                 modi/rg-arguments
                                 `(,prj-user-ignore)
                                 '("--null" ;Output null separated results
                                   ;; Get names of all the to-be-searched files,
                                   ;; same as the "-g ''" argument in ag.
                                   "--files"))
                       (append '("rg")
                               modi/rg-arguments
                               '("--null"
                                 "--files")))
                     " ")))

      (advice-add 'projectile-get-ext-command :override #'modi/advice-projectile-use-rg)))

  :hook
  (after-init . projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map))

(use-package ibuffer-projectile
  :ensure t
  :after (ibuffer projectile)
  :commands ibuffer-projectile-set-filter-groups
  :init
  (defun ibuffer-projectile-hook ()
    (ibuffer-projectile-set-filter-groups)
    (unless (eq ibuffer-sorting-mode 'recency)
      (ibuffer-do-sort-by-recency))))

(use-package undo-tree
  :ensure t
  :diminish
  :config
  (defvar cavd-undo-tree-history-dir nil
    "Directory for holding undo-tree-history.")
  (setq cavd-undo-tree-history-dir
        (let ((dir (concat user-emacs-directory
                           "undo-tree-history/")))
          (make-directory dir :parents)
          dir))
  (setq undo-tree-enable-undo-in-region t
        undo-tree-history-directory-alist `((".*" . ,cavd-undo-tree-history-dir))
        undo-tree-auto-save-history t)
  (global-undo-tree-mode))

(use-package goto-last-change
  :ensure t
  :bind ("C-c C-z" . goto-last-change))

(use-package restclient
  :ensure t
  :commands cavd-insert-restclient-mode
  :mode ("\\.rest\\'" . restclient-mode)
  :config
  (defun cavd-insert-restclient-mode ()
    (interactive)
    (insert "# -*- mode: restclient; -*-"))

  (setq restclient-same-buffer-response nil))

(use-package verb
  :after org
  :ensure t
  :hook
  (org-mode . verb-mode))

(use-package dash
  :ensure t
  :custom
  (dash-enable-fontlock t))

;; Replace 'lisp-mode-hook with 'prog-mode-hook to enable everywhere
;; (add-hook 'lisp-mode-hook 'easy-escape-minor-mode)
;; (add-hook 'emacs-lisp-mode-hook 'easy-escape-minor-mode)
;; (add-hook 'prog-mode-hook 'easy-escape-minor-mode)

(use-package edit-server
  :ensure t
  :commands edit-server-start
  :config
  (setq edit-server-new-frame t)
  (setq edit-server-new-frame-alist
        '((name . "Edit with Emacs FRAME")
          (top . 0)
          (left . 0)
          (width . 121)
          (height . 45)
          (minibuffer . t)
          (window-system . mac)))
    :hook
    (after-init . edit-server-start))

(use-package vlf
  :commands (vlf))

(use-package anzu
  :ensure t
  :diminish
  :commands (global-anzu-mode anzu-isearch-query-replace anzu-isearch-query-replace-regexp)
  :init
  (setq anzu-search-threshold 100)
  (setq anzu-replace-threshold nil)
  (setq anzu-deactivate-region nil)
  (setq anzu-replace-to-string-separator "")
  :config
  (global-anzu-mode)
  :bind
  (([remap isearch-query-replace] . anzu-isearch-query-replace)
   ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp))
  ([remap query-replace] . anzu-query-replace)
  ([remap query-replace-regexp] . anzu-query-replace-regexp)
  ("M-s %" . anzu-query-replace-at-cursor))

(use-package cliphist
  :ensure t
  :config
  (setq cliphist-cc-kill-ring t)
  :bind
  ("C-c s-v" . cliphist-paste-item)
  ("C-c s-s" . cliphist-select-item))

;; Easy kill
(use-package easy-kill
  :ensure t
  :commands (easy-kill easy-mark)
  :bind
  ([remap kill-ring-save] . easy-kill)
  ([remap mark-sexp] . easy-mark))

(use-package mark-thing-at
  :ensure t
  :hook
  (after-init . mark-thing-at-mode))

(use-package objed
  :ensure t
  :commands (objed-mode)
  ;; :hook
  ;; (after-init . objed-mode)
  )

(use-package flx-isearch
  :ensure t
  :bind
  ("C-s-s" . flx-isearch-forward)
  ("C-s-r" . flx-isearch-backward))

;; (use-package kill-or-bury-alive
;;   :ensure t
;;   :bind
;;   ("C-s-k" . kill-or-bury-alive)
;;   ("C-s-p" . kill-or-bury-alive-purge-buffers))

(use-package so-long
  :commands global-so-long-mode
  :hook
  (after-init . global-so-long-mode))

;; (use-package direnv
;;   :config
;;   (direnv-mode))

(use-package nginx-mode
  :ensure t
  :init
  (defun cavd-nginx-mode-hooks ()
    "Set nginx stuff."
    (setq nginx-indent-level 2)
    (delete-selection-mode +1)
    (smartparens-mode))
  :config
  (add-hook 'nginx-mode-hook 'cavd-nginx-mode-hooks))

(use-package company-nginx
  :ensure t
  :config
  (add-hook
   'nginx-mode-hook
   (lambda ()
     (add-to-list 'company-backends 'company-nginx))))

(use-package epg
  :hook
  (after-init . (lambda ()
                  (setq epg-pinentry-mode 'loopback)))) ; This may only be required for gog2

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(when (and cavd-macosx-p (executable-find "trash"))
  (defun  system-move-file-to-trash (file)
    "Use \"trash\" to move FILE to the system trash."
    (call-process  "trash" nil 0 nil  "-F"  file)))

(require 'cavd-ui)

;; Midnight
(use-package midnight
  :hook
  (midnight . file-notify-rm-all-watches)
  :config
  (midnight-mode))

(when (executable-find "direnv")
  (use-package envrc
    :config
    (envrc-global-mode)
    (with-eval-after-load 'envrc
      (define-key envrc-mode-map (kbd "C-c r") 'envrc-command-map))))

;; desktop
(use-package desktop
  :config
  (setq desktop-restore-eager 47
        desktop-buffers-not-to-save "^Dired"
        desktop-load-locked-desktop t
        desktop-save-buffer t
        desktop-save t)
  (add-to-list 'desktop-modes-not-to-save 'dired-mode)
  (add-to-list 'desktop-globals-to-save 'eshell-history-ring)
  (add-to-list 'desktop-globals-to-save 'extended-command-history)
  (add-to-list 'desktop-globals-to-save 'grep-history)
  (add-to-list 'desktop-globals-to-save 'deadgrep-history)
  (add-to-list 'desktop-globals-to-save 'kill-ring)
  (add-to-list 'desktop-globals-to-save 'minibuffer-history)
  (add-to-list 'desktop-globals-to-save 'regexp-history)
  (add-to-list 'desktop-globals-to-save 'shell-command-history)
  (add-to-list 'desktop-globals-to-save 'Info-history)
  (desktop-save-mode 1))
