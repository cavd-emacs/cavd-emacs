;;; cavd-completions.el --- Completions

;;; Commentary:

;;; Code:
;(use-package flx :defer t)

;; (use-package fuz
;;   :load-path "~/elisp/fuz.el"
;;   :config
;;   (unless (require 'fuz-core nil t)
;;     (fuz-build-and-load-dymod)))

(setq pcomplete-ignore-case t)

(use-package hippie-exp
  :commands hippie-expand
  :bind
  ("C-M-/" . hippie-expand)
  ("s-/" . hippie-expand)
  :config
  (setq hippie-expand-try-functions-list
        '(try-expand-dabbrev
          try-expand-all-abbrevs
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill
          try-expand-list
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-line
          try-complete-lisp-symbol-partially
          try-complete-lisp-symbol)))

;;; dabbrev
(use-package dabbrev
  :commands dabbrev-expand
  :bind
  ("M-/" . dabbrev-expand)
  :config
  (setq dabbrev-abbrev-char-regexp "\\(\\sw\\|\\s_\\)"
        dabbrev-abbrev-skip-leading-regexp "\\$\\|\\*\\|/\\|="
        dabbrev-case-distinction nil
        dabbrev-case-replace nil
        dabbrev-upcase-means-case-search t))

(use-package amx
  :ensure t
  :commands (execute-extended-command counsel-M-x)
  :config
  (setq amx-backend 'ivy
        amx-history-length 111)
  (amx-mode 1))

;; Ivy/Swiper/Counsel
(use-package historian
  :ensure t
  :hook
  (kill-emacs . historian-save))

(use-package ivy-historian
  :ensure t
  :config
  (setq ivy-historian-freq-boost-factor most-positive-fixnum)
  (setq ivy-historian-recent-boost 1111))

(use-package ivy
  :ensure t
  :diminish
  :commands (ivy-avy ivy-resume)

  :bind
  (("C-c C-o" . ivy-occur)
   ("s-r" . ivy-resume)
   ("C-x C-b" . ivy-switch-buffer)
   ("<s-up>" . ivy-push-view)
   ("<s-down>" . ivy-switch-view)
   ("C-x C-b" . ivy-switch-buffer))
  :hook
  (ivy-occur-mode . hl-line-mode)
  (ivy-occur-grep-mode . toggle-truncate-lines)
  :config
;  (ivy-historian-mode +1)

  ;; From ivy manual
  (defun ivy-yank-action (x)
    (kill-new x))

  (defun ivy-copy-to-buffer-action (x)
    (with-ivy-window
      (insert x)))

  (setq ivy-use-virtual-buffers t)
  (setq ivy-wrap t)
  (setq ivy-use-selectable-prompt t)
  (ivy-set-occur 'swiper 'swiper-occur)
  (ivy-set-occur 'swiper-isearch 'swiper-occur)
  (ivy-set-occur 'counsel-fzf 'counsel-fzf-occur)
  (ivy-set-occur 'counsel-git-grep 'counsel-git-grep-occur)
  (ivy-set-occur 'counsel-ag 'counsel-ag-occur)
  (ivy-set-occur 'counsel-rg 'counsel-rg-occur)
  (ivy-set-occur 'counsel-grep 'counsel-grep-occur)
  (ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)
  ;; (setq ivy-re-builders-alist '((swiper . ivy--regex-plus)
  ;;                               (counsel-grep-or-swiper . ivy--regex-plus)
  ;;                               (t . orderless-ivy-re-builder)))
  ;; My way
;;   (setq ivy-re-builders-alist
;;         '((swiper . ivy--regex-plus)
;;           (counsel-ag . ivy--regex-plus)
;;           (counsel-grep-or-swiper . ivy--regex-plus)
;;           (counsel-M-x . orderless-ivy-re-builder)
;;           (counsel-fzf . ivy--regex)
;;           (t . ivy--regex-fuzzy)))
;;   (push '(counsel-describe-function .  "^") ivy-initial-inputs-alist)
;;   (push '(counsel-describe-variable .  "^") ivy-initial-inputs-alist)
;; ;  (push '(counsel-M-x .  "^") ivy-initial-inputs-alist)
;;   (push '(counsel-ag .  "--file-search-regex '' -- ") ivy-initial-inputs-alist)
;;   (push '(counsel-rg .  "--glob '**' -- ") ivy-initial-inputs-alist)
;;   (push '(ivy-switch-buffer . "^") ivy-initial-inputs-alist)
;;   (setq ivy-flx-limit 1000
;;         ivy-sort-matches-functions-alist
;;         '((ivy-switch-buffer . ivy-sort-function-buffer)
;;           (counsel-find-file . ivy-sort-function-buffer)
;;           (counsel-M-x . ivy-sort-function-buffer)
;;           (t . nil)))
   (setq ivy-re-builders-alist
         '((swiper . ivy--regex-plus)
           (counsel-ag . ivy--regex-plus)
           (counsel-find-file . ivy--regex-fuzzy)
           (counsel-grep-or-swiper . ivy--regex-plus)
           (counsel-fzf . ivy--regex-fuzzy)
           (counsel-M-x . ivy--regex-plus)
           (describe-package . ivy--regex-plus)
           (t . ivy--regex-plus)))
   (push '(counsel-ag .  "--file-search-regex '' -- ") ivy-initial-inputs-alist)
   (push '(counsel-rg .  "--glob '**' -- ") ivy-initial-inputs-alist)
   (push '(counsel-M-x . "^") ivy-initial-inputs-alist)
;  (push '(ivy-switch-buffer . "^") ivy-initial-inputs-alist)
   (setq ivy-flx-limit 1000
         ivy-sort-matches-functions-alist
         '((ivy-switch-buffer . ivy-sort-function-buffer)
           (counsel-find-file . ivy-sort-function-buffer)
           (counsel-M-x . ivy-sort-function-buffer)
           (t . nil)))

  (setq ivy-views
        '(("cavd-init {}"
           (horz
            (file "cavd-local.el")
            (file "cavd-erc-funcs.el")))))
  (ivy-set-actions
   'ivy-switch-buffer
   '(("k"
      (lambda (x)
        (kill-buffer x)
        (ivy--reset-state ivy-last))
      "kill")
     ("j"
      ivy--switch-buffer-other-window-action
      "other")))
  (ivy-set-actions
   t
   '(("i" ivy-copy-to-buffer-action "insert")
     ("y" ivy-yank-action "yank"))))

(use-package swiper
  :ensure t
  :bind
  (("s-g" . swiper-thing-at-point)
   ("M-s s" . swiper-all)
   ("s-'" . swiper-avy)
   ("M-s m" . swiper-multi))
  :bind (:map swiper-map
              ("M-y" . yank)
              ("M-%" . swiper-query-replace)
              ("C-." . swiper-avy)
              ("M-c" . swiper-mc))
  :bind (:map isearch-mode-map
              ("C-o" . swiper-from-isearch))
  :init
  (setq swiper-action-recenter t)
  (setq swiper-include-line-number-in-search t)
  (setq swiper-goto-start-of-match t))

(use-package counsel
  :after ivy
  :ensure t
  :commands (counsel-fzf aw-window-list prot/counsel-fzf-rg-files)
  :custom
  (counsel-rg-base-command
   "rg -SHn --no-heading --color never --no-follow --hidden %s")
  (counsel-find-file-occur-cmd          ; TODO Simplify this
   "ls -a | grep -i -E '%s' | tr '\\n' '\\0' | xargs -0 ls -d --group-directories-first")

  :bind
  (("s-f" . counsel-grep-or-swiper)
   ("M-x" . counsel-M-x)
   ("C-c s-i" . counsel-imenu)
   ("s-o" . counsel-find-file)
   ("C-h f" . counsel-describe-function)
   ("C-h v" . counsel-describe-variable)
   ("C-h b" . counsel-descbinds)
   ("C-h i" . counsel-info-lookup-symbol)
   ("C-h y" . counsel-find-library)
   ("s-a" . counsel-ag)
   ("C-c !" . cavd-counsel-ag)
   ("M-s t" . counsel-git-grep)
   ("C-c t" . counsel-git)
   ("C-c j" . counsel-ag-projectile)
   ("C-c L" . counsel-git-log)
   ("M-s e" . counsel-rg)
   ("M-s z" . prot/counsel-fzf-rg-files)
   ("C-c A" . cavd-counsel-rg)
   ("C-c u" . counsel-unicode-char)
   ("s-z" . counsel-fzf)
   ("s-y" . counsel-yank-pop)
   ("C-x C-r" . counsel-recentf)
   ("C-x M-x" . counsel-command-history)
   ("C-x d" . counsel-dired)
   :map ivy-minibuffer-map
   ("C-r" . counsel-minibuffer-history)
   ("s-y" . ivy-next-line)        ; Avoid 2× `counsel-yank-pop'
   ("C-SPC" . ivy-restrict-to-matches))

  :init
  ;; From prot
  (defun prot/counsel-fzf-rg-files (&optional input dir)
    "Run `fzf' in tandem with `ripgrep' to find files in the
present directory.  If invoked from inside a version-controlled
repository, then the corresponding root is used instead."
    (interactive)
    (let* ((process-environment
            (cons (concat "FZF_DEFAULT_COMMAND=rg -Sn --color never --files --no-follow --hidden")
                  process-environment))
           (vc (vc-root-dir)))
      (if dir
          (counsel-fzf input dir)
        (if (eq vc nil)
            (counsel-fzf input default-directory)
          (counsel-fzf input vc)))))

  (defun prot/counsel-fzf-dir (arg)
    "Specify root directory for `counsel-fzf'."
    (prot/counsel-fzf-rg-files ivy-text
                               (read-directory-name
                                (concat (car (split-string counsel-fzf-cmd))
                                        " in directory: "))))

  (defun prot/counsel-rg-dir (arg)
    "Specify root directory for `counsel-rg'."
    (let ((current-prefix-arg '(4)))
      (counsel-rg ivy-text nil "")))

  ;; TODO generalise for all relevant file/buffer counsel-*?
  (defun prot/counsel-fzf-ace-window (arg)
    "Use `ace-window' on `prot/counsel-fzf-rg-files' candidate."
    (ace-window t)
    (let ((default-directory (if (eq (vc-root-dir) nil)
                                 counsel--fzf-dir
                               (vc-root-dir))))
      (if (> (length (aw-window-list)) 1)
          (progn
            (find-file arg))
        (find-file-other-window arg))
      (balance-windows)))

  ;; Pass functions as appropriate Ivy actions (accessed via M-o)
  (ivy-add-actions
   'counsel-fzf
   '(("r" prot/counsel-fzf-dir "change root directory")
     ("g" prot/counsel-rg-dir "use ripgrep in root directory")
     ("a" prot/counsel-fzf-ace-window "ace-window switch")))

  (ivy-add-actions
   'counsel-rg
   '(("r" prot/counsel-rg-dir "change root directory")
     ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))

  (ivy-add-actions
   'counsel-find-file
   '(("g" prot/counsel-rg-dir "use ripgrep in root directory")
     ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))

  (defun counsel-ag-projectile ()
    "Run `counsel-ag' within the current project."
    (interactive)
    (counsel-ag nil (projectile-project-root)))

  (defun counsel-rg-occur ()
    "Generate a custom occur buffer for `counsel-rg'."
    (counsel-grep-like-occur
     counsel-rg-base-command))

  (defun cavd-counsel-ag ()
    "Call `counsel-ag' with `symbol-at-point'."
    (interactive)
    (counsel-ag (symbol-name (symbol-at-point))))

  (defun cavd-counsel-rg ()
    "Call `counsel-rg' with `symbol-at-point'."
    (interactive)
    (counsel-rg (symbol-name (symbol-at-point))))

  (setq counsel-grep-base-command
        "rg -i -M 120 --no-heading --line-number --color never '%s' %s")
  (setq counsel-find-file-at-point t
        counsel-yank-pop-preselect-last t)
  (setq counsel-ag-base-command
        (concat "ag "
                "--nocolor "
                "--line-number "
                "--column "
                "--nogroup "
                "--smart-case "
                "--follow "             ; follow symlinks
                "%s")))

(use-package counsel-tramp
  :ensure t
  :after (counsel vagrant-tramp)
  :bind
  ("s-v" . counsel-tramp)
  :hook
  (counsel-tramp-pre-command . (lambda ()
                                 (projectile-mode 0)
                                 ))
  (counsel-tramp-quit . (lambda ()
                          (projectile-mode 1)
                          )))

(use-package ivy-rich
  :after ivy
  :init
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (setq ivy-rich-display-transformers-list
        '(ivy-switch-buffer
          (:columns
           (
            (ivy-switch-buffer-transformer (:width 50))
            (ivy-rich-switch-buffer-size (:width 7 :align right))
            (ivy-rich-switch-buffer-indicators (:width 3 :face error :align right))
            (ivy-rich-switch-buffer-major-mode (:width 13 :face warning))
            (ivy-rich-switch-buffer-project (:width 15 :face success))
            (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
           :predicate
           (lambda (cand) (get-buffer cand)))
          counsel-M-x
          (:columns
           ((counsel-M-x-transformer (:width 40)) ; thr original transformer
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face)))) ; return the docstring of the command
          counsel-describe-function
          (:columns
           ((counsel-describe-function-transformer (:width 40)) ; the original transformer
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face)))) ; return the docstring of the function
          counsel-describe-variable
          (:columns
           ((counsel-describe-variable-transformer (:width 40)) ; the original transformer
            (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face)))) ; return the docstring of the variable
          counsel-recentf
          (:columns
           ((ivy-rich-candidate (:width 0.8 :face font-lock-string-face)) ; return the candidate itself
            (ivy-rich-file-last-modified-time (:face font-lock-string-face))))
          counsel-find-file
          (:columns
           ((ivy-read-file-transformer)
            (ivy-rich-counsel-find-file-truename
             (:face font-lock-doc-face))))))
  (setq ivy-virtual-abbreviate 'full
        ivy-rich-parse-remote-buffer nil
        ivy-rich-path-style 'relative)
  (ivy-mode +1)
  (historian-mode +1)

  :hook
  (ivy-mode . ivy-rich-mode))

(use-package prescient
  :ensure t
  :config
  (setq prescient-history-length 100)
  (setq prescient-save-file "~/.emacs.d/var/prescient-save.el")
  (setq prescient-aggressive-file-save t)
  (setq prescient-filter-method '(literal regexp fuzzy))
  ;; The following works well when using opening a file from the top of the project
  ;; (setq prescient-filter-method '(initialism literal regexp fuzzy))
  (prescient-persist-mode +1)
  ;(prescient--load)
)

(use-package ivy-prescient
  :ensure t
  :after counsel
  :config
  (setq ivy-prescient-sort-commands
        '(:not swiper swiper-isearch ivy-switch-buffer))
  (setq prescient-sort-length-enable nil)
  (setq ivy-prescient-retain-classic-highlighting t)
  (setq ivy-prescient-enable-filtering t)
  (setq ivy-prescient-enable-sorting t)

  (defun cavd-ivy-prescient-fuzzy-filter (str)
    "Specify an exception for `prescient-filter-method'.

This new rule can be used to tailor the results of individual
Ivy-powered commands, using `ivy-prescient-re-builder'."
    (let ((prescient-filter-method '(fuzzy)))
      (ivy-prescient-re-builder str)))

  (defun prot/ivy-prescient-filters (str)
    "Specify an exception for `prescient-filter-method'.

This new rule can be used to tailor the results of individual
Ivy-powered commands, using `ivy-prescient-re-builder'."
    (let ((prescient-filter-method '(regexp)))
      (ivy-prescient-re-builder str)))

  (cl-pushnew
   '(counsel-find-file . cavd-ivy-prescient-fuzzy-filter) ivy-re-builders-alist)
  (cl-pushnew
   '(counsel-describe-function . prot/ivy-prescient-filters) ivy-re-builders-alist)
  (cl-pushnew
   '(counsel-describe-variable . prot/ivy-prescient-filters) ivy-re-builders-alist)
;; (setq ivy-re-builders-alist
;;       '((counsel-rg . prot/ivy-prescient-filters)
;;         (counsel-grep . prot/ivy-prescient-filters)
;;         (counsel-yank-pop . prot/ivy-prescient-filters)
;;         (counsel-M-x . ivy--regex-plus)
;;         (swiper . prot/ivy-prescient-filters)
;;         (swiper-isearch . prot/ivy-prescient-filters)
;;         (swiper-all . prot/ivy-prescient-filters)
;;         (t . ivy-prescient-re-builder)))
  (ivy-prescient-mode 1)
  )

;; Trying this out, as well
(setq confirm-nonexistent-file-or-buffer nil)

;; (use-package async-completing-read
;;   :load-path
;;   "~/elisp/async-completing-read"
;;   :config
;;   (setq completing-read-function #'async-completing-read))

;; company-mode
(use-package company
  :ensure t
  :diminish company-mode eldoc-mode
  :commands (company-complete company-mode)
  :config
  (setq company-minimum-prefix-length 2)
  (setq company-idle-delay 0.5)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  (setq company-dabbrev-ignore-case nil)
  (setq company-show-numbers t)
  (setq company-require-match 'never)
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-code-other-buffers 'all)
  (setq company-frontends '(company-pseudo-tooltip-frontend
                            company-echo-metadata-frontend))
  (setq company-transformers
        '(company-sort-by-backend-importance
          company-sort-prefer-same-case-prefix
          company-sort-by-occurrence))
  :bind
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous))
  :hook
  (after-init . global-company-mode))

(use-package company-flx
  :ensure t
  :hook
  (company-mode . company-flx-mode))

(use-package company-dict
  :after (company yasnippet)
  :commands company-dict-refresh
  :config
  (setq company-dict-dir
        (concat user-emacs-directory "company-dict/"))
  (setq company-dict-enable-yasnippet t)
  (add-to-list 'company-backends 'company-dict)
  :bind
  ("s-m" . company-dict))

; company-dict-minor-mode-list

;(require 'company-prescient)
;(company-prescient-mode 1)
;(prescient-persist-mode 1)
;;
;; yasnippet
;;
(use-package yasnippet
  :ensure t
  :diminish
  :commands yas-expand-from-trigger-key
  :bind
  ("<C-tab>" . tab-indent-or-complete)
  :config
  (defun check-expansion ()
    (save-excursion
      (if (looking-at "\\_>") t
        (backward-char 1)
        (if (looking-at "\\.") t
          (backward-char 1)
          (if (looking-at "->") t nil)))))

  (defun do-yas-expand ()
    (let ((yas-expand 'return-nil))
      (yas-expand)))

  (defun tab-indent-or-complete ()
    (interactive)
    (if (minibufferp)
        (minibuffer-complete)
      (if (or (not yas-minor-mode)
              (null (do-yas-expand)))
          (if (check-expansion)
              (company-complete-common)
            (indent-for-tab-command)))))

;  (diminish 'yas-minor-mode)
  (setq yas-wrap-around-region t)
  (setq yas-triggers-in-field t)
  (add-to-list 'auto-mode-alist '("yas/.*" . snippet-mode))
  (advice-add 'company-complete-common :before (lambda () (setq my-company-point (point))))
  (advice-add
   'company-complete-common
   :after (lambda () (when (equal my-company-point (point)) (yas-expand))))
  :hook
  (after-init . yas-global-mode))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

(use-package auto-yasnippet
  :ensure t
  :after yasnippet
  :commands (aya-create aya-expand))


;; Dumb & Smart Jump
(use-package dumb-jump
  :ensure t
  :commands (dumb-jump-go dumb-jump-back)
  :config
  (setq dumb-jump-selector 'ivy
        dumb-jump-grep-cmd 'rg
        dumb-jump-force-searcher 'rg))

(use-package smart-jump
  :ensure t
  :commands (smart-jump-go smart-jump-back)
  :bind
  ("C-c h j" . hydra-smart-dumb-jump/body)
  :config
  (smart-jump-setup-default-registers))

;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
(setq lsp-keymap-prefix "C-c s-l")

(use-package lsp-mode
  :ensure t
  :after (php-mode ruby-mode js-mode company-mode)
  :commands (lsp lsp-deferred)
  :config
  (lsp-modeline-diagnostics-mode -1)
  (setq lsp-file-watch-threshold nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-enable-folding nil)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-enable-links nil)
  (setq lsp-restart 'auto-restart)
  (setq lsp-auto-configure nil)
  ;; don't scan 3rd party javascript libraries
  (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$"
        lsp-file-watch-ignored-directories) ; json

  ;; don't ping LSP lanaguage server too frequently
  (defvar lsp-on-touch-time 0)
  (defadvice lsp-on-change (around lsp-on-change-hack activate)
    ;; don't run `lsp-on-change' too frequently
    (when (> (- (float-time (current-time))
                lsp-on-touch-time) 30) ;; 30 seconds
      (setq lsp-on-touch-time (float-time (current-time)))
      ad-do-it))
  (push 'company-capf company-backends)
  :hook
  (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;   (php-mode . lsp)
   ;; if you want which-key integration
   (lsp-mode . lsp-enable-which-key-integration)))

;; optionally
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

;; if you are ivy user
(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)

;; optionally
;; (use-package lsp-ui
;;   :commands lsp-ui-mode
;;   :bind (:map lsp-ui-mode-map
;;   ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
;;   ([remap xref-find-references] . lsp-ui-peek-find-references)))

;; (use-package company-lsp
;;   :after (company-mode lsp)
;;   :commands company-lsp
;;   :config
;;   (setq company-lsp-cache-candidates 'auto)
;;   (push 'company-lsp company-backends))

;; optionally if you want to use debugger
;; (use-package dap-mode :defer t)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; (use-package orderless
;;   :ensure t
;;   :after ivy
;;   :init
;;     ;;; NOTE: Not sure why I had that value before.  Keeping it around
;;     ;;; it case the one below proves problematic in some edge case…
;;   ;; (setq orderless-component-separator "[/\s_-]+")
;;   (setq orderless-component-separator " +")
;;   (setq orderless-matching-styles
;;         '(orderless-flex
;;           orderless-strict-leading-initialism
;;           orderless-regexp
;;           orderless-prefixes
;;           orderless-literal))

;;   (defun prot/orderless-literal-dispatcher (pattern _index _total)
;;     (when (string-suffix-p "=" pattern)
;;       `(orderless-literal . ,(substring pattern 0 -1))))

;;   (defun prot/orderless-initialism-dispatcher (pattern _index _total)
;;     (when (string-suffix-p "," pattern)
;;       `(orderless-strict-leading-initialism . ,(substring pattern 0 -1))))

;;   (setq orderless-style-dispatchers
;;         '(prot/orderless-literal-dispatcher
;;           prot/orderless-initialism-dispatcher))
;;   ;; (setq completion-styles
;;   ;;       '(orderless partial-completion))

;;   :bind (:map minibuffer-local-completion-map
;;               ("SPC" . nil)))
                                        ; Space should never complete: use
                                        ; it for `orderless' groups.

(use-package minibuffer
  :init
  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)

;;; General minibuffer functions
  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           (completions (get-buffer-window "*Completions*")))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

;;; Completions' buffer actions
  ;; NOTE In practice I only use those while inspecting a long list
  ;; produced by C-h {f,o,v}.  To pop the Completions buffer, use
  ;; `minibuffer-completion-help', by default bound to ? from inside the
  ;; minibuffer.

  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

  (defmacro prot/completions-buffer-act (name doc &rest body)
    `(defun ,name ()
       ,doc
       (interactive)
       (let ((completions-window (get-buffer-window "*Completions*"))
             (completions-buffer (get-buffer "*Completions*"))
             (symbol (thing-at-point 'symbol)))
         (if (window-live-p completions-window)
             (with-current-buffer completions-buffer
               ,@body)
           (user-error "No live window with Completions")))))

  (prot/completions-buffer-act
   prot/completions-kill-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to the kill ring."
   (kill-new `,symbol)
   (message "Copied %s to kill-ring"
            (propertize `,symbol 'face 'success)))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to active window."
   (let ((window (window-buffer (get-mru-window))))
     (with-current-buffer window
       (insert `,symbol)
       (message "Inserted %s"
                (propertize `,symbol 'face 'success)))))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point-exit
   "Like `prot/completions-insert-symbol-at-point' plus exit."
   (prot/completions-insert-symbol-at-point)
   (top-level))

;;; Miscellaneous functions and key bindings

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  (setq completion-category-defaults nil)
  (setq completion-cycle-threshold 5)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-pcm-word-delimiters "-_./:| ")
  (setq completion-show-help nil)
  (setq completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)
  (setq completions-format 'vertical)   ; *Completions* buffer
  (setq read-answer-short t)
  (setq resize-mini-windows t)

  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           ;; This could be hardened a bit, but I am okay with it.
           (completions (or (get-buffer-window "*Completions*")
                            (get-buffer-window "*Embark Live Occur*"))))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  ;; This will be deprecated in favour of the `embark' package
  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

;;;; DEPRECATED in favour of the `embark' package (see further below),
;;;; which implements the same functionality in a more efficient way.
;;  (defun prot/complete-kill-or-insert-candidate (&optional arg)
;;     "Place the matching candidate to the top of the `kill-ring'.
;; This will keep the minibuffer session active.
;;
;; With \\[universal-argument] insert the candidate in the most
;; recently used buffer, while keeping focus on the minibuffer.
;;
;; With \\[universal-argument] \\[universal-argument] insert the
;; candidate and immediately exit all recursive editing levels and
;; active minibuffers.
;;
;; Bind this function in `icomplete-minibuffer-map'."
;;     (interactive "*P")
;;     (let ((candidate (car completion-all-sorted-completions)))
;;       (when (and (minibufferp)
;;                  (or (bound-and-true-p icomplete-mode)
;;                      (bound-and-true-p live-completions-mode))) ; see next section
;;         (cond ((eq arg nil)
;;                (kill-new candidate))
;;               ((= (prefix-numeric-value arg) 4)
;;                (with-minibuffer-selected-window (insert candidate)))
;;               ((= (prefix-numeric-value arg) 16)
;;                (with-minibuffer-selected-window (insert candidate))
;;                (top-level))))))

  ;; Defines, among others, aliases for common actions to Super-KEY.
  ;; Normally these should go in individual package declarations, but
  ;; their grouping here makes things easier to understand.
  :bind (
         ("s-l" . prot/describe-symbol-at-point)
         ("s-L" . (lambda ()
                    (interactive)
                    (prot/describe-symbol-at-point '(4))))
         ("s-e" . prot/focus-minibuffer-or-completions)
         :map minibuffer-local-completion-map
         ("<return>" . minibuffer-force-complete-and-exit) ; exit with completion
         ("C-j" . exit-minibuffer)      ; force input unconditionally
         :map completion-list-mode-map
         ("h" . prot/describe-symbol-at-point)
         ("w" . prot/completions-kill-symbol-at-point)
         ("i" . prot/completions-insert-symbol-at-point)
         ("j" . prot/completions-insert-symbol-at-point-exit)
         ("n" . next-line)
         ("p" . previous-line)
         ("f" . next-completion)
         ("b" . previous-completion)
         ("M-v" . prot/focus-minibuffer)))

(provide 'cavd-prescient-completions)

;;; cavd-completions.el ends here.
