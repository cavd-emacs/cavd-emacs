;; ERC

(use-package erc
  :defer t
  :commands (erc erc-track-switch-buffer)
  :init
  ;; Require ERC-SASL package
  (require 'erc-sasl)

  ;; Add SASL server to list of SASL servers (start a new list, if it did not exist)
  ;; (add-to-list 'erc-sasl-server-regexp-list "irc\\.libera\\.chat")

  :bind
  (("C-c e" . cavd-erc-libera-start-or-switch)
   ("s-#" . cavd-switch-to-emacs)

   :map erc-mode-map
   ("C-c s-k" . erc-kill-input)
   ("C-c C-x" . cavd-erc-quit))
  :config
  ;; Redefine/Override the erc-login() function from the erc package, so that
  ;; it now uses SASL
  ;; (defun erc-login ()
  ;;   "Perform user authentication at the IRC server. (PATCHED)"
  ;;   (erc-log (format "login: nick: %s, user: %s %s %s :%s"
  ;;                    (erc-current-nick)
  ;;                    (user-login-name)
  ;;                    (or erc-system-name (system-name))
  ;;                    erc-session-server
  ;;                    erc-session-user-full-name))
  ;;   (if erc-session-password
  ;;       (erc-server-send (format "PASS %s" erc-session-password))
  ;;     (message "Logging in without password"))
  ;;   (when (and (featurep 'erc-sasl) (erc-sasl-use-sasl-p))
  ;;     (erc-server-send "CAP REQ :sasl"))
  ;;   (erc-server-send (format "NICK %s" (erc-current-nick)))
  ;;   (erc-server-send
  ;;    (format "USER %s %s %s :%s"
  ;;            ;; hacked - S.B.
  ;;            (if erc-anonymous-login erc-email-userid (user-login-name))
  ;;            "0" "*"
  ;;            erc-session-user-full-name))
  ;;   (erc-update-mode-line))

  (defun cavd-switch-to-emacs ()
    (interactive)
    (switch-to-buffer "#emacs"))

  (defun cavd-erc-quit ()
    "Quit erc with a canned message
and close the window."
    (interactive)
    (erc-quit-server "quit")
    (delete-window))

  (defun cavd-erc-mode-hook ()
    "Set things up for erc."
    (turn-on-flyspell))

  (defun cavd-erc-libera-start-or-switch ()
    "Connect to ERC, or switch to last active buffer"
    (interactive)
    (if (ad-is-advised 'open-network-stream)
        (ad-deactivate 'open-network-stream))
    (if (get-buffer "irc.libera.chat:6697/irc.libera.chat") ;; ERC already active?
        (erc-track-switch-buffer 1)    ;; yes: switch to last active
      (when (y-or-n-p "Start ERC on Libera? ")   ;; no: maybe start ERC
        (erc-tls
         :server "irc.libera.chat"
         :port 6697
         :nick "cvandusen"
         :full-name "Chris Van Dusen")
        ;; (erc
        ;;  :server "irc.libera.chat"
        ;;  :port 6667
        ;;  :nick "cvandusen"
        ;;  :full-name "Chris Van Dusen.")
        )))

  (require 'erc-spelling)
  (require 'erc-services nil t)
  (require 'erc-truncate)
  (require 'erc-replace)
  (require 'erc-dcc)
  (require 'erc-hl-nicks)
  (erc-replace-mode 1)
  (erc-track-mode t)
  (erc-autojoin-mode t)
  (erc-services-mode 1)
  (setq erc-reuse-buffers t)
  (setq erc-prompt-for-nickserv-password nil)
  (setq erc-autojoin-channels-alist
        '((".*\\.libera.chat" "#emacs")))
  (setq erc-max-buffer-size 30000)
  (setq erc-kill-buffer-on-part t)
  (setq erc-kill-queries-on-quit t)
  (setq erc-kill-server-buffer-on-quit t)
  (setq erc-nick "cvandusen")
  (setq erc-port 6697)
  (setq erc-public-away-p t)
  (setq erc-show-my-nick nil)
  (setq erc-spelling-mode 1)
  (setq erc-truncate-mode t)
  (setq erc-user-full-name "Chris Van Dusen")
  (setq erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE"
                                  "324" "329" "332" "333" "353" "477"))
  (setq erc-replace-alist '((",t8*$" . (replace-match ""))))
  (setq erc-hide-list '("JOIN" "PART" "QUIT" "NICK" "MODE"))
  (add-to-list 'erc-modules 'replace)
  (add-to-list 'erc-modules 'spelling)
  ;; (add-to-list 'erc-modules 'notifications)
  (erc-update-modules)
  :hook
  (erc-mode . turn-on-eldoc-mode)
  (erc-insert-post . erc-truncate-buffer)
  (erc-insert-modify . erc-replace-insert)
  (erc-mode . cavd-erc-mode-hook))

(provide 'cavd-erc-funcs)
