;; VCS Config

(use-package vc
  :commands (vc-annotate vc-update-change-log vc-region-history)
  :init
  (setq vc-find-revision-no-save t)
  :bind
  (("C-x v a" . vc-annotate)
   ("C-x v g" . vc-update-change-log)
   ("C-x v H" . vc-region-history)))

(use-package git-timemachine
  :ensure t
  :commands (git-timemachine git-timemachine-toggle)
  :bind
  (("C-c v t" . git-timemachine-toggle)))

(use-package magit
  :ensure t
  :defer t
  :commands (
             magit-status
             magit-blame-addition
             global-magit-file-mode
             magit-list-repositories
             )
  :config
  (defun th/magit-eldoc-for-commit ()
    (let ((commit (magit-commit-at-point)))
      (when commit
        (with-temp-buffer
          (magit-git-insert "show"
                            "--format=format:%an <%ae>, %ar"
                            (format "--stat=%d" (window-width))
                            commit)
          (goto-char (point-min))
          (put-text-property (point-min)
                             (line-end-position)
                             'face 'bold)
          (buffer-string)))))

  (defun th/magit-eldoc-setup ()
    (add-function :before-until
                  (local 'eldoc-documentation-function)
                  #'th/magit-eldoc-for-commit)
    (eldoc-mode 1))

  (when (executable-find "choose")
    ;; better find-file-in-repository
    ;; assumes you have magit and maybe other stuff
    (defun choose/find-file-in-git-repo ()
      (interactive)
      (require 's)
      (let ((root-dir (magit-toplevel default-directory)))
        (if root-dir
            (let ((default-directory root-dir))
              (let ((f (s-trim
                        (shell-command-to-string
                         "git ls-files -co --exclude-standard | choose"))))
                (unless (string= "" f)
                  (find-file f))))
          (call-interactively 'find-file))))

    (global-set-key (kbd "C-x f") 'choose/find-file-in-git-repo))

  (setq magit-status-margin '(t "%d %b %y %H:%M" magit-log-margin-width t 18))
  (setq magit-log-margin '(t "%d %b %y %H:%M" magit-log-margin-width t 18))
  (setq magit-status-sections-hook
        '(magit-insert-status-headers
          magit-insert-merge-log
          magit-insert-recent-commits
          magit-insert-rebase-sequence
          magit-insert-am-sequence
          magit-insert-sequencer-sequence
          magit-insert-bisect-output
          magit-insert-bisect-rest
          magit-insert-bisect-log
          magit-insert-untracked-files
          magit-insert-unstaged-changes
          magit-insert-staged-changes
          magit-insert-stashes
          magit-insert-unpushed-to-upstream
          magit-insert-unpulled-from-upstream
          magit-insert-unpulled-from-pushremote
          magit-insert-unpushed-to-pushremote))
  (setq magit-completing-read-function 'magit-builtin-completing-read)
  (setq magit-auto-revert-mode t)
  (setq magit-diff-refine-hunk 'all)
  (setq magit-status-initial-section '(2))
  (setq magit-repository-directories
        '(("~/.emacs.d/cavd-emacs" . 0)
          ("~/.emacs.d/snippets" . 0)
          ("~/dotfiles" . 0)))
  :bind (:map magit-mode-map
              ("I" . magit-gitignore-locally))
  :hook
  (magit-status-mode . th/magit-eldoc-setup)
  (magit-log-mode . th/magit-eldoc-setup)
  )

;; (require 'magithub)
;; (magithub-feature-autoinject 'all)

(use-package git-gutter
  :ensure t
  :diminish
  :commands (git-gutter-mode global-git-gutter-mode)
  :config
  (defun cavd-git-gutter ()
    (global-git-gutter-mode)
    (diminish 'git-gutter-mode nil))
  (defun me/git-gutter-set-ongoing-hydra-body ()
    (setq me/ongoing-hydra-body #'hydra-magit/body))

  :hook
  (git-gutter-mode . me/git-gutter-set-ongoing-hydra-body)
  (after-init . cavd-git-gutter))

(provide 'cavd-vcs)
