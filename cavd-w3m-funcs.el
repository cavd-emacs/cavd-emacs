;;
;; w3m code
;;
(use-package w3m
  :bind (:map w3m-mode-map
         (("s-t" . w3m-goto-url-new-session)
          ("s-w" . w3m-delete-buffer)))
  :hook
  (w3m-mode . w3m-register-desktop-save)
  :config
  (defun w3m-browse-current-buffer ()
    (interactive)
    (let ((filename (concat (make-temp-file "w3m-") ".html")))
      (unwind-protect
          (progn
            (write-region (point-min) (point-max) filename)
            (w3m-find-file filename))
        (delete-file filename))))

  (defun w3m-register-desktop-save ()
    "Set `desktop-save-buffer' to a function returning the current URL."
    (interactive)
    (setq desktop-save-buffer (lambda (desktop-dirname) w3m-current-url)))

  (defun w3m-restore-desktop-buffer (d-b-file-name d-b-name d-b-misc)
    "Restore a `w3m' buffer on `desktop' load."
    (when (eq 'w3m-mode desktop-buffer-major-mode)
      (let ((url d-b-misc))
        (when url
          (require 'w3m)
          (if (string-match "^file" url)
              (w3m-find-file (substring url 7))
            (w3m-goto-url-new-session url))
          (current-buffer)))))
  (setq w3m-session-file "~/elisp/.w3m-session"
        w3m-session-autosave t
        w3m-session-load-last-sessions t
        w3m-use-title-buffer-name t
        browse-url-new-window-flag t
        w3m-use-cookies t
        w3m-command (executable-find "zsh")
        w3m-cookie-accept-domains '("handmark.com")
        w3m-default-display-inline-images t
        w3m-history-reuse-history-elements t
        w3m-mailto-url-function nil)
  (unless window-system
    (setq browse-url-browser-function 'w3m-browse-url)
    (setq browse-url-new-window-flag t)))

;;
;; Webjump
;;
;; (require 'webjump-plus)
;; (setq webjump-sites
;;       (append
;;        '(("Duck" . [simple-query "duckduckgo.com" "http://duckduckgo.com?q=" ""]))
;;        webjump-plus-sites
;;        webjump-sample-sites))

(provide 'cavd-w3m-funcs)
