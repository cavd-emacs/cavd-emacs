(require 'slack)
(require 'lui)

(setq slack-prefer-current-team t
      slack-current-team nil
      slack-buffer-create-on-notify t
      slack-completing-read-function #'ivy-completing-read
      slack-buffer-function #'switch-to-buffer-other-window
      slack-display-team-name nil
      )

(define-key slack-mode-map (kbd "s-t") 'slack-thread-show-or-create)

(define-key slack-mode-map "@"
  (defun endless/slack-message-embed-mention ()
    (interactive)
    (call-interactively #'slack-message-embed-mention)
    (insert " ")))

(with-eval-after-load 'tracking
  (define-key tracking-mode-map (kbd "<C-f11>")
    #'tracking-next-buffer))

(add-to-list
 'alert-user-configuration
 '(((:message . "@cvandusen\\|cvd\\|CVD")
    (:title . "\\(licensing-war-room\\|licensing-lunch\\)")
    (:category . "slack"))
   libnotify nil))

;;; Channels
(setq slack-message-notification-title-format-function
      (lambda (_team room threadp)
        (concat (if threadp "Thread in #%s") room)))

(defun endless/-cleanup-room-name (room-name)
  "Make group-chat names a bit more human-readable."
  (replace-regexp-in-string
   "--" " "
   (replace-regexp-in-string "#mpdm-" "" room-name)))

;;; Private messages and group chats
(setq
 slack-message-im-notification-title-format-function
 (lambda (_team room threadp)
   (concat (if threadp "Thread in %s")
           (endless/-cleanup-room-name room))))

(defun cavd-slack-hook ()
  (flyspell-mode 1))

(add-hook 'slack-mode-hook 'cavd-slack-hook)

(slack-register-team
   :name "ke"
   :default t
   :client-id slack-client-id
   :client-secret slack-client-secret
   :token slack-token)

(provide 'cavd-slack)
