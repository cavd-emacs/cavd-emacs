(use-package popper
  :ensure t ; or :straight t
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          compilation-mode
          help-mode
          inf-ruby-mode
          psysh-mode))
  (setq popper-group-function
        #'popper-group-by-projectile) ; projectile projects
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

(provide 'cavd-popper)
