(setq shell-file-name zsh-executable)
(setq explicit-shell-file-name zsh-executable)
(setq comint-process-echoes t)
(setenv "SHELL" zsh-executable)
(add-to-list 'auto-mode-alist '("\\.env$" . sh-mode))
(add-to-list 'auto-mode-alist '("\\.env.local$" . sh-mode))

(use-package eterm-256color
  :ensure t
  :hook
  (vterm-mode . eterm-256color-mode))

;; (use-package shell
;;   :config
;;   (defun my-shell-mode-hook ()
;;     (with-editor-export-editor)
;;     (setq comint-input-ring-file-name "~/.zsh_history")
;;                                         ; Ignore timestamps in history file.  Assumes that zsh
;;                                         ; EXTENDED_HISTORY option is in use.
;;     (setq comint-input-ring-separator "\n: \\([0-9]+\\):\\([0-9]+\\);")
;;     (comint-read-input-ring t))

;;   (define-key shell-mode-map
;;     (kbd "C-r") 'counsel-shell-history)
;;   :hook
;;   (shell-mode . my-shell-mode-hook))

(use-package with-editor
  :ensure t
  :commands (
             shell-command-with-editor-mode
             with-editor-async-shell-command
             with-editor-shell-command
             )
  :config
  (define-key (current-global-map)
    [remap async-shell-command] 'with-editor-async-shell-command)
  (define-key (current-global-map)
    [remap shell-command] 'with-editor-shell-command)
  (shell-command-with-editor-mode))

(use-package eshell
  :config
  (defun slot/unmetafy ()
    (cl-flet
        ((unmetafy (input)
           (let ((i 0) output)
             (while-let
                 ((char (nth i input))
                  (inc-and-char
                   (if (= char #x83)
                       ;; Skip meta character and unmetafy.
                       `(2 . ,(logxor (nth (1+ i) input) 32))
                     ;; Advance as usual.
                     `(1 . ,char))))
               (cl-incf i (car inc-and-char))
               (setq output (cons (cdr inc-and-char) output)))
             (decode-coding-string
              (apply #'unibyte-string (nreverse output))
              'utf-8-unix
              t))))
      (let ((hist-file "~/.zsh_history"))
        (with-temp-buffer
          (insert (mapconcat (-compose #'unmetafy #'string-to-list)
                             (s-lines (f-read-bytes hist-file))
                             "\n"))
          (write-file hist-file)))))

  (defun slot/eshell-exit (&optional arg)
    "Exit eshell and kill the current frame."
    (interactive "P")
    (slot/unmetafy)
    (eshell-write-history)
    (save-buffers-kill-terminal))

  (defun prot/eshell-complete-history ()
    "Insert element from `eshell' history using completion."
    (interactive)
    (let ((hist (ring-elements eshell-history-ring)))
      (insert
       (completing-read "Input history: " hist nil t))))

  (defun prot/eshell-complete-recent-dir (&optional arg)
    "Switch to a recent `eshell' directory using completion.
With \\[universal-argument] also open the directory in a `dired'
buffer."
    (interactive "P")
    (let* ((dirs (ring-elements eshell-last-dir-ring))
           (dir (icomplete-vertical-do ()
                                       (completing-read "Switch to recent dir: " dirs nil t))))
      (insert dir)                      ; Not good enough
      (eshell-send-input)               ; Should cd directly…
      (when arg
        (dired dir))))

  ;; `cl-remove-if' is used right below
  (declare-function cl-remove-if "cl-seq")

  (defun prot/eshell-find-subdirectory-recursive ()
    "Recursive `eshell/cd' to subdirectory.
This command has the potential for infinite recursion: use it
wisely or prepare to use `eshell-interrupt-process'."
    (interactive)
    (let* ((dir (abbreviate-file-name (eshell/pwd)))
           (contents (directory-files-recursively dir ".*" t nil nil))
           (dirs (cl-remove-if
                  (lambda (x)
                    (or (not (file-directory-p x))
                        (string-match-p "\\.git" x)))
                  contents))
           (selection (completing-read
                       (format "Find sub-dir from %s (%s): "
                               (propertize dir 'face 'success)
                               (length dirs))
                       dirs nil t)))
      (insert selection)
      (eshell-send-input)))

  (defun setup-company-eshell-autosuggest ()
    ;; (setq-local company-backends '(company-eshell-autosuggest))
    (setq-local company-frontends '(company-preview-frontend)))

  ;; TODO write defmacro for all those file-at-point
  (defun prot/eshell-insert-file-at-point ()
    "Insert (cat) contents of file at point."
    (interactive)
    (let ((file (ffap-file-at-point)))
      (if file
          (progn
            (end-of-buffer)
            (insert (concat "cat " file))
            (eshell-send-input))
        (user-error "No file at point"))))

  (defun prot/eshell-kill-save-file-at-point ()
    "Add to kill-ring the absolute path of file at point."
    (interactive)
    (let ((file (ffap-file-at-point)))
      (if file
          (kill-new (concat (eshell/pwd) "/" file))
        (user-error "No file at point"))))

  (defun prot/eshell-find-file-at-point ()
    "Run `find-file' for file at point (ordinary file or dir).
Recall that this will produce a `dired' buffer if the file is a
directory."
    (interactive)
    (let ((file (ffap-file-at-point)))
      (if file
          (find-file file)
        (user-error "No file at point"))))

  (defun prot/eshell-file-parent-dir ()
    "Open `dired' with the parent directory of file at point."
    (interactive)
    (let ((file (ffap-file-at-point)))
      (if file
          (dired (file-name-directory (concat (eshell/pwd) "/" file)))
        (user-error "No parent dir for file to jump to"))))

  (defun prot/eshell-mkcd (dir)         ; TODO define alias
    "Make a directory, or path, and switch to it."
    (interactive (list
                  (completing-read "Directory: " nil)))
    (eshell/mkdir "-p" dir)
    (eshell/cd dir))

  (defun prot/eshell-put-last-output-to-buffer ()
    "Produce a buffer with output of last `eshell' command."
    (interactive)
    (let ((eshell-output (kill-ring-save (eshell-beginning-of-output)
                                         (eshell-end-of-output))))
      (with-current-buffer (get-buffer-create  "*last-eshell-output*")
        (erase-buffer)
        (yank)           ; TODO do it with `insert' and `delete-region'?
        (switch-to-buffer-other-window (current-buffer)))))

  (defun prot/eshell-complete-redirect-to-buffer ()
    "Complete the syntax for appending to a buffer via `eshell'."
    (interactive)
    (insert
     (format " >>> #<%s>"
             (read-buffer-to-switch "Switch to buffer: "))))

  (defun prot/eshell-narrow-output-highlight-regexp ()
    (interactive)
    (let ((regexp (read-regexp "Regexp to highlight")))
      (narrow-to-region (eshell-beginning-of-output)
                        (eshell-end-of-output))
      (goto-char (point-min))
      (highlight-regexp regexp 'hi-yellow)))

  ;;;; NOTE by Prot 2020-06-16: the following two advice-add snippets
  ;;;; will need to be reviewed to make sure they do not produce
  ;;;; undesirable side effects.

  ;; syntax highlighting implementation modified from
  ;; https://emacs.stackexchange.com/questions/50385/use-emacs-syntax-coloring-when-not-in-emacs
  ;;
  ;; This command also makes it possible to, e.g., cat an encrypted and/or
  ;; compressed file.
  (defun contrib/eshell-cat-with-syntax-highlight (&rest args)
    "Like `eshell/cat' but with syntax highlighting.
To be used as `:override' advice to `eshell/cat'."
    (setq args (eshell-stringify-list (eshell-flatten-list args)))
    (dolist (filename args)
      (let ((existing-buffer (get-file-buffer filename))
            (buffer (find-file-noselect filename)))
        (eshell-print
         (with-current-buffer buffer
           (if (fboundp 'font-lock-ensure)
               (font-lock-ensure)
             (with-no-warnings
               (font-lock-fontify-buffer)))
           (let ((contents (buffer-string)))
             (remove-text-properties 0 (length contents) '(read-only nil) contents)
             contents)))
        (unless existing-buffer
          (kill-buffer buffer)))))

  (advice-add 'eshell/cat
              :override #'contrib/eshell-cat-with-syntax-highlight)

  ;; Turn ls results into clickable links.  Especially useful when
  ;; combined with link-hint.  Modified from
  ;; https://www.emacswiki.org/emacs/EshellEnhancedLS
  (define-button-type 'eshell-ls
    'supertype 'button
    'help-echo "RET, mouse-2: visit this file"
    'follow-link t)

  (defun contrib/electrify-ls (name)
    "Buttonise `eshell' ls file names.
Visit them with RET or mouse click.  This function is meant to be
used as `:filter-return' advice to `eshell-ls-decorated-name'."
    (add-text-properties 0 (length name)
                         (list 'button t
                               'keymap button-map
                               'mouse-face 'highlight
                               'evaporate t
                               'action #'find-file
                               'button-data (expand-file-name name)
                               'category 'eshell-ls)
                         name)
    name)

  (advice-add 'eshell-ls-decorated-name
              :filter-return #'contrib/electrify-ls)

  (require 'em-term)
  (require 'esh-module)
  (require 'em-dirs)
  (require 'em-tramp)
  (require 'em-hist)

  (add-to-list 'eshell-visual-subcommands
               '("git" ("diff" "hist" "log" "show")))
  (add-to-list 'eshell-visual-commands "ssh")
  (setq eshell-hist-ignoredups t)
  (setq eshell-save-history-on-exit t)
  (setq eshell-cd-on-directory t)
  (setq eshell-hist-ignoredups t)
  (setq eshell-save-history-on-exit t)
  (setq eshell-modules-list
        '(eshell-alias
          eshell-basic
          eshell-cmpl
          eshell-dirs
          eshell-glob
          eshell-hist
          eshell-ls
          eshell-pred
          eshell-prompt
          eshell-script
          eshell-term
          eshell-tramp
          eshell-unix))
  :hook
  (eshell-hist-load . slot/unmetafy)
  (eshell-mode . with-editor-export-editor)
  (eshell-mode . setup-company-eshell-autosuggest)
  :bind
  (("M-<f4>" . eshell)
   :map eshell-mode-map
   ("C-x C-c" . slot/eshell-exit)
   ("M-k" . eshell-kill-input)
   ("C-c M-w" . prot/eshell-kill-save-file-at-point)
   ("C-c i" . prot/eshell-insert-file-at-point)
   ("C-c f" . prot/eshell-find-file-at-point)
   ("C-c C-f" . prot/eshell-find-file-at-point)
   ("C-c o" . prot/eshell-put-last-output-to-buffer)
   ("C-c >" . prot/eshell-complete-redirect-to-buffer)
   ("C-c C-j" . prot/eshell-file-parent-dir)
   ("C-c r" . prot/eshell-narrow-output-highlight-regexp)
   ("M-r" . prot/eshell-complete-history) ; use this to find input history
   ("C-c d" . prot/eshell-find-subdirectory-recursive)
   ("C-c =" . prot/eshell-complete-recent-dir)
   )
  )

(use-package eshell-git-prompt
  :ensure t
  :config
  (eshell-git-prompt-use-theme 'powerline))

(use-package eshell-autojump
  :ensure t
  :hook
  (eshell-mode . eshell-autojump-load))

(when (executable-find "fish")
  (use-package fish-completion
    :ensure t
    ;; :init
    ;; (unless (executable-find "fish")
    ;;   (shell-command "brew install fish"))
    :config
    (global-fish-completion-mode)
    :hook
    (eshell-mode . global-fish-completion-mode)))

; Remember lots of previous commands in shell-mode
(setq comint-input-ring-size 100000)

(use-package term
  :commands term
  :hook
  (term-mode . (lambda ()
                 (setq term-buffer-maximum-size 10000)))
  (term-exec . with-editor-export-editor))

(use-package vterm
  :ensure t
  :commands (vterm cavd-visit-vterm)
  :config
  (setq vterm-term-environment-variable "eterm-color")
  (setq vterm-kill-buffer-on-exit t)
  (defun cavd-visit-vterm ()
    "If we are in an vterm, rename it.
   If there is no vterm, run it.
   If there is one running, switch to that buffer."
    (interactive)
    (if (equal "vterm" (buffer-name))
        (call-interactively 'rename-buffer)
      (if (get-buffer "vterm")
          (pop-to-buffer "vterm")
        (vterm "vterm"))))

  (setq vterm-min-window-width 203)
  (push (list "ffb"
              (lambda (path)
                (if-let* ((buf (find-file-noselect path))
                          (window (display-buffer-below-selected buf nil)))
                    (select-window window)
                  (message "Failed to open file: %s" path))))
        vterm-eval-cmds)
  (push (list "ff"
              (lambda (path)
                (find-file path)))
        vterm-eval-cmds)
  :bind
  ("<f4>" . cavd-visit-vterm)
  ;; (setq vterm-buffer-name-string t)
  ;; :hook
  ;; (vterm-mode . (lambda ()
  ;;                 ;(setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")
  ;;                 (set (make-local-variable 'buffer-face-mode-face) 'fixed-pitch)
  ;;                 (buffer-face-mode)))
  )

(use-package vterm-toggle)
(use-package multi-vterm)

(defun my-project-shell ()
  "Start an inferior shell in the current project's root directory.
If a buffer already exists for running a shell in the project's root,
switch to it.  Otherwise, create a new shell buffer.
With \\[universal-argument] prefix arg, create a new inferior shell buffer even
if one already exists."
  (interactive)
  (require 'comint)
  (let* ((default-directory (project-root (project-current t)))
         (default-project-shell-name (project-prefixed-buffer-name "shell"))
         (shell-buffer (get-buffer default-project-shell-name)))
    (if (and shell-buffer (not current-prefix-arg))
        (if (comint-check-proc shell-buffer)
            (pop-to-buffer shell-buffer (bound-and-true-p display-comint-buffer-action))
          (vterm shell-buffer))
      (vterm (generate-new-buffer-name default-project-shell-name)))))

(advice-add 'project-shell :override #'my-project-shell)

(provide 'cavd-init-shell)
