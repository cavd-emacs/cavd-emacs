;;;  cavd-keybinds.el --- Define keybindings

;;; Commentary:

;;; Code:
(require 'transient)

(use-package emacs
  :commands (mac-switch-meta mac-switch-hyper)
  :config
  (defun mac-switch-meta ()
    "Switch meta between Option and Command"
    (interactive)
    (setq mac-option-modifier 'meta)
    (setq mac-command-modifier 'super))

  (defun mac-switch-hyper ()
    "Switch fn to Hyper"
    (interactive)
    (setq mac-function-modifier 'hyper)
    (global-set-key (kbd "H-<right>") 'end-of-buffer)
    (global-set-key (kbd "H-<left>") 'beginning-of-buffer))

  (mac-switch-meta)
  (mac-switch-hyper))

(use-package free-keys
  :ensure t
  :commands free-keys
  :config
  (add-to-list 'free-keys-modifiers "s"))

;;
;; Global key bindings:
;;

(defun cavd-ignore () (interactive))
(cl-loop for key in (list
                     (kbd "<mouse-5>")
                     (kbd "C-<mouse-5>")
                     (kbd "S-<mouse-5>")
                     (kbd "<mouse-4>")
                     (kbd "C-<mouse-4>")
                     (kbd "S-<mouse-4>")
                     (kbd "<wheel-up>")
                     (kbd "<wheel-down>")
                     (kbd "<wheel-right>")
                     (kbd "<wheel-left>"))
         do (global-set-key key 'cavd-ignore))

(use-package accent
  :ensure t
  :bind ("C-c C-a" . accent-menu))

(use-package char-menu
  :commands char-menu
  :bind ("C-c s-c" . char-menu)
  :config
  (setq char-menu
        '("—" "‘’" "“”" "…" "«»" "–"
          ("Typography" "•" "©" "†" "‡" "°" "·" "§" "№" "★" "®" "¢")
          ("Math"       "≈" "≡" "≠" "∞" "×" "±" "∓" "÷" "√")
          ("Arrows"     "←" "→" "↑" "↓" "⇐" "⇒" "⇑" "⇓")
          ("Greek"      "α" "β" "Y" "δ" "ε" "ζ" "η" "θ" "ι" "κ" "λ" "μ"
           "ν" "ξ" "ο" "π" "ρ" "σ" "τ" "υ" "φ" "χ" "ψ" "ω")
          "")))

;; key-chord
(use-package key-chord
  :ensure t
  :commands key-chord-mode
  :init
  (key-chord-mode 1)
  :config
  (setq key-chord-two-keys-delay 0.05)
  (key-chord-define-global ",b" 'ivy-switch-buffer)
  (key-chord-define-global ",f" 'counsel-find-file)
  (key-chord-define-global ",m" 'magit-status)
  (key-chord-define-global "zp" 'projectile-switch-project)
  (key-chord-define-global ",r" 'ivy-resume)
  (key-chord-define-global "zu" 'undo-tree-visualize))

(use-package smartparens
  :ensure t
  :diminish
  :commands smartparens-mode
  :config
  (require 'smartparens-config))

;; Function Keys
(global-set-key (kbd "<f2>") 'delete-other-windows)
(global-set-key (kbd "<S-f2>") 'split-window-vertically)
(global-set-key (kbd "<f3>") 'kill-this-buffer)
(global-set-key (kbd "<S-f3>") 'split-window-horizontally)
(global-set-key (kbd "<C-S-f4>") 'edbi:open-db-viewer)
(global-set-key (kbd "<C-M-S-f4>") 'cavd-visit-edbi-buffer)

(global-set-key (kbd "<f9>") 'window-toggle-side-windows)
(global-set-key (kbd "<f10>") 'scroll-right)
(global-set-key (kbd "<f11>") 'scroll-left)

;; Home/End/Delete/Backspace
(global-set-key (kbd "<home>") 'beginning-of-buffer)
(global-set-key (kbd "<end>") 'end-of-buffer)

;; Various kill/delete bindings
(global-set-key (kbd "<kp-delete>") 'delete-char)
(global-set-key (kbd "<C-M-delete>") 'backward-kill-sexp)
(global-set-key (kbd "<C-M-backspace>") 'backward-kill-sexp)
(global-set-key (kbd "<C-M-kp-delete>") 'backward-kill-sexp)
(global-set-key (kbd "M-<kp-delete>") 'backward-kill-word)
(global-set-key (kbd "M-z") 'zap-up-to-char)

;; C-*, etc.
(global-set-key (kbd "C-x r U") 'cavd-upcase-rectangle)
(global-set-key (kbd "C-x T") 'toggle-transparency)
(global-set-key (kbd "C-x w") 'browse-url-at-point)
(global-set-key (kbd "C-c s-f") 'cavd-show-current-file)
(global-set-key (kbd "C-s-f") 'find-file-at-point)
(global-set-key (kbd "s-i") 'cavd-insert-file-name)
(global-set-key (kbd "C-s-i") 'cavd-kill-file-name)
(global-set-key (kbd "C-c /") 'align-regexp)

(global-set-key (kbd "C-c ^") 'resize-window)

(global-set-key (kbd "C-h a") 'apropos)

(define-key read-expression-map (kbd "\t") 'lisp-complete-symbol)

(global-set-key (kbd "M-*") 'xref-pop-marker-stack)

(global-set-key (kbd "s-k") 'kill-word-at-point)

;; Keybinds from emacs mac port
(global-set-key (kbd "s-s") 'save-buffer)
(global-set-key (kbd "s-w") (lambda () (interactive) (delete-window)))

;; Keybindings for HHK Lite
(global-set-key (kbd "<H-down>") 'scroll-up-command)
(global-set-key (kbd "<H-up>") 'scroll-down-command)

;;* Hydras
(use-package hydra
  :ensure t
  :commands defhydra
  :preface
  (defvar-local me/ongoing-hydra-body nil)
  (defun me/ongoing-hydra ()
    (interactive)
    (if me/ongoing-hydra-body
        (funcall me/ongoing-hydra-body)
      (user-error "me/ongoing-hydra: me/ongoing-hydra-body is not set")))
  :bind
  ;; ("C-c <tab>" . hydra-fold/body)
  ("C-c h a" . hydra-avy/body)
  ("C-c h c" . hydra-counsel/body)
  ("C-c h d" . hydra-dates/body)
  ("C-c h o" . hydra-org/body)
  ("C-c h e" . hydra-eyebrowse/body)
  ("C-c h g" . hydra-magit/body)
  ;; ("C-c o" . me/ongoing-hydra)
  ("C-c h l" . hydra-words/body)
  ("C-c h m" . hydra-mark/body)
  ("C-c h p" . hydra-projectile/body)
  ("C-c h s" . hydra-system/body)
  ("C-c h w" . hydra-windows/body)
  ("C-c h f" . hydra-flymake/body)
  ("C-c h y" . hydra-yasnippet/body)
  ("s-b" . hydra-window/body)
  :custom
  (hydra-default-hint nil))

(defvar hydra-stack nil)

(defun hydra-push (expr)
  (push `(lambda () ,expr) hydra-stack))

(defun hydra-pop ()
  (interactive)
  (let ((x (pop hydra-stack)))
    (when x
      (funcall x))))

(defhydra hydra-counsel (:color blue)
  "
^
^Words    ^Replace                ^Search             ^
^─────────^───────────────────────^───────────────────^
_q_ quit  _C-r_ recent            _D_ dired
^^        _U_   unicode           _X_ command history
^^        _Y_   yank-pop          _Z_ prot/fzf-rg-files
^^        _a_   counsel-ag        _b_ descbins
^^        _f_   find file         _g_ counsel-git
^^        _i_   imenu             _j_ ripgrep
^^        _l_   find lib          _o_ git log
^^        _p_   ag projectile     _r_ git grep
^^        _s_   swiper or grep    _u_ desc function
^^        _v_   desc variable     _w_ counsel-rg
^^        _y_   info symbol       _z_ counsel-fzf
"
  ("q" nil)
  ("C-r"  counsel-recentf)
  ("D"  counsel-dired)
  ("U"  counsel-unicode-char)
  ("X"  counsel-command-history)
  ("Y"  counsel-yank-pop)
  ("Z"  prot/counsel-fzf-rg-files)
  ("a"  cavd-counsel-ag)
  ("b"  counsel-descbinds)
  ("f"  counsel-find-file)
  ("g"  counsel-git)
  ("i"  counsel-imenu)
  ("j"  counsel-rg)
  ("l"  counsel-find-library)
  ("o"  counsel-git-log)
  ("p"  counsel-ag-projectile)
  ("r"  counsel-git-grep)
  ("s"  counsel-grep-or-swiper)
  ("u"  counsel-describe-function)
  ("v"  counsel-describe-variable)
  ("w"  cavd-counsel-rg)
  ("x"  counsel-M-x)
  ("y"  counsel-info-lookup-symbol)
  ("z"  counsel-fzf)
)

;; C-x m s 	mark-symbol 	Lisp symbol
;; C-x m n 	mark-number 	Integer or float number
;; C-x m l 	mark-line-this 	Entire line (not including prompt in a shell)
;; C-x l, C-x m i 	mark-line 	Eentire line with the newline
;; C-x m t 	mark-list 	List for the given language mode
;; C-M-@, C-M-SPC 	mark-sexp 	Lisp symbol expression
;; C-M-h 	mark-defun 	Function definition
;; C-x m f 	mark-filename 	File name
;; C-x m u 	mark-url 	Universal resource locator
;; M-@ 	mark-word 	Natural language word
;; C-x m c 	mark-sentence 	Natural language sentence
;; C-x m h 	mark-whitespace 	Tabs and spaces

(defhydra hydra-mark (:color red :columns nil)
  "mark"
("q" nil "cancel")
("b" mark-whitespace "whitespace" :exit t)
("c" mark-sentence "sentence" :exit t)
("d" mark-defun "defun" :exit t)
("f" mark-filename "filename" :exit t)
("l" mark-line-this "line" :exit t)
("L" mark-line "line+newline" :exit t)
("n" mark-number "number" :exit t)
("s" mark-symbol "symbil" :exit t)
("t" mark-list "list" :exit t)
("u" mark-url "url" :exit t)
("w" mark-word "word" :exit t)
("x" mark-sexp "sexp" :exit t))

(defhydra hydra-words (:color blue)
  "
^Replace                ^Search          ^
^───────────────────────^────────────────^
_c_ correct             _d_ definition
_r_ replace w synonym   _m_ match
^^                      _s_ find synonym
_q_ quit                _w_ define word
"
  ("q" nil)
  ("c" flyspell-correct-word-before-point)
  ("d" dictionary-search)
  ("m" dictionary-match-words)
  ("r" synosaurus-choose-and-replace)
  ("s" synosaurus-lookup)
  ("w" define-word))

(defhydra hydra-dates (:color blue)
  "
^
^Dates^             ^Insert^            ^Insert with Time^
^─────^─────────────^──────^────────────^────────────────^──
_q_ quit            _d_ short           _D_ short
^^                  _i_ iso             _I_ iso
^^                  _l_ long            _L_ long
^^                  ^^                  ^^
"
  ("q" nil)
  ("d" me/date-short)
  ("D" me/date-short-with-time)
  ("i" me/date-iso)
  ("I" me/date-iso-with-time)
  ("l" me/date-long)
  ("L" me/date-long-with-time))


(defhydra hydra-eww (:color blue)
  "
_&_ Open external browser         _B_ List bookmarks
_D_ Toggle paragraph direction    _E_ Set encoding
_F_ Toggle fonts                  _G_ Exit
_H_ List Histories                _R_ Readable
_S_ List buffers                  _b_ Add bookmark
_d_ Download                      _g_ Reload
_l_ Back                          _n_ Next
_p_ Previous                      _q_ Quit
_r_ Forward URL                   _s_ Switch buffer
_t_ Top URL                       _u_ up URL
_v_ View Source                   _w_ Copy URL
_C_ Cookie List                   _i_ Previous Link
_c_ Toggle colors                 _N_ Next bookmark
_P_ Previous bookmark             ^^"
  ("&" eww-browse-with-external-browser :exit t)
  ("B" eww-list-bookmarks :exit t)
  ("D" eww-toggle-paragraph-direction :exit t)
  ("E" eww-set-character-encoding :exit t)
  ("F" eww-toggle-fonts :exit t)
  ("G" eww :exit t)
  ("H" eww-list-histories :exit t)
  ("R" eww-readable :exit t)
  ("S" eww-list-buffers :exit t)
  ("b" eww-add-bookmark :exit t)
  ("d" eww-download :exit t)
  ("g" eww-reload :exit t)
  ("h" describe-mode :exit t)
  ("l" eww-back-url :exit t)
  ("n" eww-next-url :exit t)
  ("p" eww-previous-url :exit t)
  ("q" quit-window :exit t)
  ("r" eww-forward-url :exit t)
  ("s" eww-switch-to-buffer :exit t)
  ("t" eww-top-url :exit t)
  ("u" eww-up-url :exit t)
  ("v" eww-view-source :exit t)
  ("w" eww-copy-page-url :exit t)
  ("C" url-cookie-list :exit t)
  ("i" shr-previous-link :exit t)
  ("c" eww-toggle-colors :exit t)
  ("N" eww-next-bookmark :exit t)
  ("P" eww-previous-bookmark :exit t))

(defhydra hydra-eyebrowse (:color blue)
  "
^
^Eyebrowse^         ^Do^                ^Switch^
^─────────^─────────^──^────────────────^──────^────────────
_q_ quit            _c_ create          _<_ previous
^^                  _k_ kill            _>_ next
^^                  _r_ rename          _e_ last
^^                  ^^                  _s_ switch
^^                  ^^                  ^^
"
  ("q" nil)
  ("<" eyebrowse-prev-window-config :color red)
  (">" eyebrowse-next-window-config :color red)
  ("c" eyebrowse-create-window-config)
  ("e" eyebrowse-last-window-config)
  ("k" eyebrowse-close-window-config :color red)
  ("r" eyebrowse-rename-window-config)
  ("s" eyebrowse-switch-to-window-config))

(defhydra hydra-magit (:color red)
  "
^
^Magit^             ^Do^                ^See^
^─────^─────────────^──^────────────────^-----------
_q_ quit            _b_ magit blame     _a_ annotate
^^                  _c_ magit clone     _d_ magit diff
^^                  _i_ magit init      _l_ magit log
^^                  _s_ magit status    _n_ next
^^                  _f_ magit stage     _p_ prev
^^                  _o_ magit popup     _t_ timemachine
"
  ("q" nil)
  ("a" vc-annotate)
  ("b" magit-blame-addition)
  ("c" magit-clone)
  ("d" magit-diff-buffer-file)
  ("f" magit-stage-file)
  ("i" magit-init)
  ("l" magit-log-buffer-file)
  ("m" magit-status :exit t)
  ("n" git-gutter:next-hunk)
  ("o" magit-file-popup)
  ("p" git-gutter:previous-hunk)
  ("s" git-gutter:stage-hunk)
  ("t" git-timemachine :exit t))

(defhydra hydra-system (:color blue)
  "
^
^System^            ^Packages^          ^Emacs lisp^         ^Shell^
^──────^────────────^────────^──────────^──────-───^─────────^─────^─────────────
_q_ quit            _p_ list            _i_ ielm             _e_ eshell
^^                  _P_ upgrade         ^^                   ^^
^^                  ^^                  ^^                   _t_ external term
"
  ("q" nil)
  ("e" cavd-visit-eshell)
  ("i" ielm)
  ("p" paradox-list-packages)
  ("P" paradox-upgrade-packages)
  ("t" open-terminal-in-workdir))

(defhydra hydra-smart-dumb-jump (:color blue :columns 3)
  "Smart Dumb Jump"
  ("j" dumb-jump-go "Go")
  ("o" dumb-jump-go-other-window "Other window")
  ("e" dumb-jump-go-prefer-external "Go external")
  ("x" dumb-jump-go-prefer-external-other-window "Go external other window")
  ("i" dumb-jump-go-prompt "Prompt")
  ("l" dumb-jump-quick-look "Quick look")
  ("b" dumb-jump-back "Back"))

(defhydra hydra-yasnippet (:color red :columns nil)
  "yasnippet"
  ("c" aya-create "create" :exit t)
  ("e" aya-expand "expand" :exit t)
  ("o" aya-open-line "open" :exit t)
  ("p" aya-persist-snippet "persist" :exit t))

;(require 'hydra-examples)

(defhydra hydra-avy (:color green :columns nil :hint nil)
  "
    Avy:
------------------------------------------------------------------------------------------
_0_: Jump to start of any word        _1_: Jump to word start             _2_: Jump to char1 char2
_a_: Go to a line above current       _b_: Go to a line below current     _c_: Jump to char
_t_: Jump to first of many chars      _i_: Jump to char in line           _s_: Jump to any word or subward part
_l_: Jump to line                     _n_: Go to next match               _q_: Quit
_p_: Go to previous match             _r_: Return to last avy-push-mark
_w_: Jump to subword start            _y_: Jump to swiper candidate
"
  ("0" avy-goto-word-0 :exit t)
  ("1" avy-goto-word-1 :exit t)
  ("2" avy-goto-char-2 :exit t)
  ("a" avy-goto-line-above :exit t)
  ("b" avy-goto-line-below :exit t)
  ("c" avy-goto-char :exit t)
  ("t" avy-goto-char-timer :exit t)
  ("i" avy-goto-char-in-line :exit t)
  ("l" avy-goto-line :exit t)
  ("n" avy-next)
  ("p" avy-prev)
  ("r" avy-pop-mark :exit t)
  ("s" avy-goto-subword-0 :exit t)
  ("w" avy-goto-subword-1 :exit t)
  ("y" swiper-avy :exit t)
  ("q" nil))

(defhydra hydra-windows (:color pink)
  "
^Window^            ^Zoom^
^──────^────────────^────^──────
_b_ balance         _-_ out
_h_ heighten        _+_ in
_n_ narrow          _=_ reset
_l_ lower           ^^
_w_ widen           ^^
_q_ quit            ^^
"
  ("q" nil)
  ("b" balance-windows)
  ("h" enlarge-window)
  ("l" shrink-window)
  ("n" shrink-window-horizontally)
  ("w" enlarge-window-horizontally)
  ("-" text-scale-decrease)
  ("+" text-scale-increase)
  ("=" (text-scale-increase 0)))

(defhydra hydra-window (:color red :columns nil)
  "window"
  ("<f4>" cavd-switch-sql-buffer "sql" :exit t)
  (";" avy-goto-word-1 "avy1" :exit t)
  ("<" beginning-of-buffer nil)
  (">" end-of-buffer nil)
  ("D" hydra-move-splitter-down nil)
  ("L" hydra-move-splitter-left nil)
  ("R" hydra-move-splitter-right nil)
  ("U" hydra-move-splitter-up nil)
  ("W" hydra-windows/body :exit t)
  ("a" ace-window "ace")
  ("b" ivy-switch-buffer "buf")
  ("d" buf-move-down nil)
  ("f" counsel-find-file "file")
  ("g" counsel-rg "ripgrep" :exit t)
  ("h" (lambda () (interactive) (split-window-below) (windmove-down)) "horz")
  ("i" aw-flip-window "flip")
  ("l" buf-move-left nil)
  ("m" magit-status "magit" :exit t)
  ("o" delete-other-windows "one" :exit t)
  ("p" (set-window-dedicated-p (selected-window) t) "proprietary")
  ("q" nil "cancel")
  ("r" buf-move-right nil)
  ("s" ace-swap-window "swap")
  ("t" transpose-frame "'")
  ("u" buf-move-up nil)
  ("v" (lambda () (interactive) (split-window-right) (windmove-right)) "vert")
  ("w" ace-delete-window "del")
  ("x" (progn (winner-undo) (setq this-command 'winner-undo)) "undo")
  ("z" (progn (winner-redo) (setq this-command 'winner-redo)) "redo"))


(defhydra hydra-projectile-other-window (:color teal)
  "projectile-other-window"
  ("f"  projectile-find-file-other-window        "file")
  ("g"  projectile-find-file-dwim-other-window   "file dwim")
  ("d"  projectile-find-dir-other-window         "dir")
  ("b"  projectile-switch-to-buffer-other-window "buffer")
  ("q"  nil                                      "cancel" :color blue))

(defhydra hydra-projectile (:color teal
                            :hint nil)
  "
     PROJECTILE: %(projectile-project-root)

     Dired File          Search/Tags          Window/Buffers         Cache
------------------------------------------------------------------------------------------
 _f_: file             _a_: ag                _i_: Ibuffer           _c_: cache clear
 _F_: file dwim        _A_: counsel ag        _b_: switch to buffer  _x_: remove known project
 _D_: file curr dir    _g_: update gtags      _k_: Kill all buffers  _X_: cleanup non-existing
 _R_: recent file      _m_: multi-occur       _`_: Otheer window     _z_: cache current
 _d_: dir              _r_: ripgrep

"
 ("a" projectile-ag)
 ("A" counsel-ag-projectile)
 ("b" projectile-switch-to-buffer)
 ("c" projectile-invalidate-cache)
 ("d" projectile-find-dir)
 ("D" projectile-find-file-in-directory)
 ("f" projectile-find-file)
 ("F" projectile-find-file-dwim)
 ("g" ggtags-update-tags)
 ("i" projectile-ibuffer)
 ("k" projectile-kill-buffers)
 ("m" projectile-multi-occur)
 ("p" counsel-projectile-switch-project "switch project")
 ("s" projectile-switch-project)
 ("S" projectile-save-project-buffers)
 ("r" projectile-ripgrep)
 ("R" projectile-recentf)
 ("x" projectile-remove-known-project)
 ("X" projectile-cleanup-known-projects)
 ("z" projectile-cache-current-file)
 ("`" hydra-projectile-other-window/body "other window")
 ("q" nil "cancel" :color blue))

(defhydra hydra-helpful (:color red
                         :columns nil)
"
_c_ Command        _f_ Function        _l_ Callable        _m_ Macro
_s_ At Point       _v_ Variable        _q_ Quit
"
  ("f"  helpful-function)
  ("c" helpful-command)
  ("l" helpful-callable)
  ("s" helpful-at-point)
  ("m" helpful-macro)
  ("v" helpful-variable)
  ("q" nil))

(defhydra hydra-flymake (:color red :columns nil)
  "window"
  ("n" flymake-goto-next-error "next")
  ("p" flymake-goto-prev-error "prev")
  ("q" nil "cancel"))

(use-package helpful
  :bind
  ("C-c h h" . hydra-helpful/body))

(defhydra hydra-org (:color pink)
  "
^
^Org^               ^Links^             ^Outline^           ^Tables^
^───^───────────────^─────^─────────────^───────^───────────^───────────
_q_ quit            _i_ insert          _<_ previous        _c_ convert
^^                  _n_ next            _>_ next
^^                  _p_ previous        _a_ all
^^                  _s_ store           _v_ overview
^^                  ^^                  ^^
^^                  ^^                  ^^
"
  ("q" nil)
  ("<" org-backward-element)
  (">" org-forward-element)
  ("a" outline-show-all)
  ("c" org-table-convert-region)
  ("i" org-insert-link :color blue)
  ("n" org-next-link)
  ("p" org-previous-link)
  ("s" org-store-link)
  ("v" org-overview))


(defhydra hydra-image (:color red)
  "
_+_ Increase size        _-_ Decrease size        _<_ Beginning             _>_ End                  _?_ Describe
_F_ Goto Frame           _a_ Speed                _b_ Previous Frame        _f_ Next frame           _r_ Revert
_h_ Describe             _k_ Kill buffer          _n_ Next file             _o_ Save                 _p_ Previous file
_q_ Quit                 _r_ Rotate               _DEL_ Scroll Down         _S-SPC_ SEcroll Up       _x_ Quit
"
  ("C-c" hydra-image-toggle/body :exit t)
  ("RET" image-toggle-animation)
  ("SPC" image-scroll-up)
  ("+" image-increase-size)
  ("-" image-decrease-size)
  ("<" beginning-of-buffer)
  (">" end-of-buffer)
  ("?" describe-mode)
  ("F" image-goto-frame)
  ("a" hydra-image-speed/body :exit t)
  ("b" image-previous-frame)
  ("f" image-next-frame)
  ("g" revert-buffer)
  ("h" describe-mode)
  ("k" image-kill-buffer)
  ("n" image-next-file)
  ("o" image-save)
  ("p" image-previous-file)
  ("q" quit-window)
  ("r" image-rotate)
  ("DEL" image-scroll-down)
  ("S-SPC" image-scroll-down)
  ("x" nil))

(defhydra hydra-image-speed (:color red)
  "
_+_ Increase Speed        _-_ Decrease Speed        _0_ Reset Speed        _r_ Reverse Speed
"
("+" image-increase-speed)
("-" image-decrease-speed)
("0" image-reset-speed)
("r" image-reverse-speed))

(defhydra hydra-image-toggle (:color red)
  "
_c_ Toggle Display        _x_ Hex Display
"
  ("c" image-toggle-display)
  ("x" image-toggle-hex-display))

;(define-key image-mode-map "'" 'hydra-image/body)

(require 'cavd-hydra-dired)

;; (use-package transient
;;   :ensure t)

;; (transient-define-prefix cavd-window-transient-menu ()
;;   "Window/Text"
;;   [["Window"
;;     ("b" "Balance" balance-windows)
;;     ("h" "Height" enlarge-window)
;;     ("i" "Shrink" shrink-window)
;;     ("n" "Narrow" shrink-window-horizontally)
;;     ("w"  "Widen" enlarge-window-horizontally)]
;;    ["Zoom"
;;     ("-"  "Out" text-scale-decrease)
;;     ("+" "In" text-scale-increase)
;;     ("=" "Reset" cavd-reset-text-scale)]
;;   ])

(transient-define-prefix dired-operate-dispatch ()
  "Operate on files."
  ["Operate"
   ("C" "dired-do-copy" dired-do-copy)
   ("R" "dired-do-rename" dired-do-rename)
   ("D" "dired-do-delete" dired-do-delete)
   ("!" "dired-do-shell-command" dired-do-shell-command)
   ("&" "dired-do-async-shell-command" dired-do-async-shell-command)
   ("S" "dired-do-symlink" dired-do-symlink)
   ("H" "dired-do-hardlink" dired-do-hardlink)
   ("P" "dired-do-print" dired-do-print)
   ("Z" "dired-do-compress" dired-do-compress)
   ("B" "dired-do-byte-compile" dired-do-byte-compile)
   ("L" "dired-do-load" dired-do-load)
   ("T" "dired-do-touch" dired-do-touch)
   ("M" "dired-do-chmod" dired-do-chmod)
   ("G" "dired-do-chgrp" dired-do-chgrp)
   ("O" "dired-do-chown" dired-do-chown)
   ("A" "dired-do-find-regexp" dired-do-find-regexp)
   ("Q" "dired-do-find-regexp-and-replace" dired-do-find-regexp-and-replace)
   ("W" "browse-url-of-dired-file" browse-url-of-dired-file)
   ("a" "dired-find-alternate-file" dired-find-alternate-file)
   ("c" "dired-do-compress-to" dired-do-compress-to)
   ("x" "dired-do-flagged-delete" dired-do-flagged-delete)
   ("k" "dired-do-kill-lines" dired-do-kill-lines)
   ])

(transient-define-prefix dired-immediate-dispatch ()
  "Perform immediate action on file."
  [["Immediate"
    ("+" "dired-downcase" dired-downcase)
    ("j" "dired-goto-file" dired-goto-file)
    ("l" "dired-do-redisplay" dired-do-redisplay)
    ("o" "dired-find-file-other-window" dired-find-file-other-window)
    ("s" "dired-sort-toggle-or-edit" dired-sort-toggle-or-edit)
    ("v" "dired-view-file" dired-view-file)
    ("w" "dired-copy-filename-as-kill" dired-copy-filename-as-kill)
    ("y" "dired-show-file-type" dired-show-file-type)
    ("=" "dired-diff" dired-diff)
    ("* N" "dired-number-of-marked-files" dired-number-of-marked-files)
    ("g" "revert-buffer" revert-buffer)
    ("(" "dired-hide-details-mode" dired-hide-details-mode)
    ("+" "dired-create-directory" dired-create-directory)
    (";" "dired-posframe-mode" dired-posframe-mode)
    ("?" "dired-summary" dired-summary)
    ]])

(transient-define-prefix dired-regexp-dispatch ()
  "Perform regular expression on files."
  [["Regexp"
    ("l" "dired-downcase" dired-downcase)
    ("r" "dired-do-rename-regexp" dired-do-rename-regexp)
    ("u" "dired-upcase" dired-upcase)
    ("C" "dired-do-copy-regexp" dired-do-copy-regexp)
    ("H" "dired-do-hardlink-regexp" dired-do-hardlink-regexp)
    ("R" "dired-do-rename-regexp" dired-do-rename-regexp)
    ("S" "dired-do-symlink-regexp" dired-do-symlink-regexp)
    ("d" "dired-flag-files-regexp" dired-flag-files-regexp)
]])

(transient-define-prefix dired-mark-dispatch ()
  "Mark files."
  ["Mark:"
   ["Mark:"
    ("d" "dired-flag-file-deletion" dired-flag-file-deletion)
    ("m" "dired-mark" dired-mark)
    ("% g" "dired-mark-files-containing-regexp" dired-mark-files-containing-regexp)
    ("% m" "dired-mark-files-regexp" dired-mark-files-regexp)
    ("t" "dired-toggle-marks" dired-toggle-marks)
    ("* C-n" "dired-next-marked-file" dired-next-marked-file)
    ("* C-p" "dired-prev-marked-file" dired-prev-marked-file)
    ("* %" "dired-mark-files-regexp" dired-mark-files-regexp)
    ("* c" "dired-change-marks" dired-change-marks)
    ("* m" "dired-mark" dired-mark)
    ("* t" "dired-toggle-marks" dired-toggle-marks)]
   ["Special"
    ("% &" "dired-flag-garbage-files" dired-flag-garbage-files)
    ("#" "dired-flag-auto-save-files" dired-flag-auto-save-files)
    ("~" "dired-flag-backup-files" dired-flag-backup-files)
    ("* *" "dired-mark-executables" dired-mark-executables)
    ("* /" "dired-mark-directories" dired-mark-directories)
    ("* @" "dired-mark-symlinks" dired-mark-symlinks)
    ("* s" "dired-mark-subdir-files" dired-mark-subdir-files)]
   ["Unmark:"
    ("* u" "dired-unmark" dired-unmark)
    ("u" "dired-unmark" dired-unmark)
    ("U" "dired-unmark-all-marks" dired-unmark-all-marks)
    ("* !" "dired-unmark-all-marks" dired-unmark-all-marks)
    ("* ?" "dired-unmark-all-files" dired-unmark-all-files)]
   ])

(transient-define-prefix dired-subdir-dispatch ()
  "Actions for subdirectories."
  ["Subdirectories:"
   [("<" "dired-prev-dirline" dired-prev-dirline)]
   [(">" "dired-next-dirline" dired-next-dirline)]
   [("i" "dired-maybe-insert-subdir" dired-maybe-insert-subdir)]
   [("$" "dired-hide-subdir" dired-hide-subdir)]
   [("^" "dired-up-directory" dired-up-directory)]
   ])

(transient-define-prefix dired-epa-dispatch ()
  "Encryption actions"
  ["Encrypt:"
   [("d" "epa-dired-do-decrypt" epa-dired-do-decrypt)]
   [("e" "epa-dired-do-encrypt" epa-dired-do-encrypt)]
   [("s" "epa-dired-do-sign" epa-dired-do-sign)]
   [("v" "epa-dired-do-verify" epa-dired-do-verify)]
   ])

(transient-define-prefix cavd-dired-dispatch ()
  "Mark things in dired."
  ["Menu:"
   [ ("e" "Epa" dired-epa-dispatch)]
   [ ("i" "Immediate" dired-immediate-dispatch)]
   [ ("m" "Mark" dired-mark-dispatch)]
   [ ("o" "Operate" dired-operate-dispatch)]
   [ ("x" "Regexp" dired-regexp-dispatch)]
   [ ("s" "Subdirs" dired-subdir-dispatch)]
   ])

(provide 'cavd-keybinds)
;;; cavd-keybinds.el ends here.
