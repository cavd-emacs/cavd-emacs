;;-*-coding: utf-8;-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '())

(define-abbrev-table 'Custom-mode-abbrev-table '())

(define-abbrev-table 'Info-edit-mode-abbrev-table '())

(define-abbrev-table 'Man-mode-abbrev-table '())

(define-abbrev-table 'PDDL-mode-abbrev-table '())

(define-abbrev-table 'ag-mode-abbrev-table '())

(define-abbrev-table 'amrita-mode-abbrev-table '())

(define-abbrev-table 'apropos-mode-abbrev-table '())

(define-abbrev-table 'asdf-mode-abbrev-table '())

(define-abbrev-table 'asm-mode-abbrev-table '())

(define-abbrev-table 'awk-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'bbdb-mode-abbrev-table '())

(define-abbrev-table 'bibtex-mode-abbrev-table '())

(define-abbrev-table 'bookmark-bmenu-mode-abbrev-table '())

(define-abbrev-table 'bookmark-edit-annotation-mode-abbrev-table '())

(define-abbrev-table 'browse-kill-ring-edit-mode-abbrev-table '())

(define-abbrev-table 'browse-kill-ring-mode-abbrev-table '())

(define-abbrev-table 'bs-mode-abbrev-table '())

(define-abbrev-table 'c++-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'c-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'calculator-mode-abbrev-table '())

(define-abbrev-table 'calendar-mode-abbrev-table '())

(define-abbrev-table 'campfire-room-mode-abbrev-table '())

(define-abbrev-table 'change-log-mode-abbrev-table '())

(define-abbrev-table 'clojure-mode-abbrev-table '())

(define-abbrev-table 'cobol-mode-abbrev-table
  '(
    ("$i" "" cobol-if-skel 0)
   ))

(define-abbrev-table 'comint-mode-abbrev-table '())

(define-abbrev-table 'completion-list-mode-abbrev-table '())

(define-abbrev-table 'conf-colon-mode-abbrev-table '())

(define-abbrev-table 'conf-javaprop-mode-abbrev-table '())

(define-abbrev-table 'conf-ppd-mode-abbrev-table '())

(define-abbrev-table 'conf-space-mode-abbrev-table '())

(define-abbrev-table 'conf-unix-mode-abbrev-table '())

(define-abbrev-table 'conf-windows-mode-abbrev-table '())

(define-abbrev-table 'conf-xdefaults-mode-abbrev-table '())

(define-abbrev-table 'css-mode-abbrev-table '())

(define-abbrev-table 'csv-mode-abbrev-table '())

(define-abbrev-table 'custom-new-theme-mode-abbrev-table '())

(define-abbrev-table 'custom-theme-choose-mode-abbrev-table '())

(define-abbrev-table 'dcl-mode-abbrev-table '())

(define-abbrev-table 'debbugs-gnu-mode-abbrev-table '())

(define-abbrev-table 'debbugs-gnu-usertags-mode-abbrev-table '())

(define-abbrev-table 'debugger-mode-abbrev-table '())

(define-abbrev-table 'diary-fancy-display-mode-abbrev-table '())

(define-abbrev-table 'diary-mode-abbrev-table '())

(define-abbrev-table 'diff-mode-abbrev-table '())

(define-abbrev-table 'dig-mode-abbrev-table '())

(define-abbrev-table 'display-time-world-mode-abbrev-table '())

(define-abbrev-table 'doc-view-mode-abbrev-table '())

(define-abbrev-table 'doctex-mode-abbrev-table '())

(define-abbrev-table 'doctor-mode-abbrev-table '())

(define-abbrev-table 'dsssl-mode-abbrev-table '())

(define-abbrev-table 'dun-mode-abbrev-table '())

(define-abbrev-table 'edbi:dbview-query-result-quicklook-mode-abbrev-table '())

(define-abbrev-table 'edebug-eval-mode-abbrev-table '())

(define-abbrev-table 'edit-abbrevs-mode-abbrev-table '())

(define-abbrev-table 'eieio-custom-mode-abbrev-table '())

(define-abbrev-table 'el4r-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-byte-code-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-mode-abbrev-table
  '(
    ("hws" "hello-world-skel" nil 1)
   ))

(define-abbrev-table 'enh-ruby-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'epa-info-mode-abbrev-table '())

(define-abbrev-table 'epa-key-list-mode-abbrev-table '())

(define-abbrev-table 'epa-key-mode-abbrev-table '())

(define-abbrev-table 'erc-dcc-chat-mode-abbrev-table '())

(define-abbrev-table 'erc-list-menu-mode-abbrev-table '())

(define-abbrev-table 'erc-mode-abbrev-table '())

(define-abbrev-table 'ert-results-mode-abbrev-table '())

(define-abbrev-table 'ert-simple-view-mode-abbrev-table '())

(define-abbrev-table 'eshell-mode-abbrev-table '())

(define-abbrev-table 'eww-bookmark-mode-abbrev-table '())

(define-abbrev-table 'eww-history-mode-abbrev-table '())

(define-abbrev-table 'eww-mode-abbrev-table '())

(define-abbrev-table 'finder-mode-abbrev-table '())

(define-abbrev-table 'flycheck-error-list-mode-abbrev-table '())

(define-abbrev-table 'ftp-mode-abbrev-table '())

(define-abbrev-table 'fundamental-mode-abbrev-table '())

(define-abbrev-table 'gdb-script-mode-abbrev-table '())

(define-abbrev-table 'gfm-mode-abbrev-table '())

(define-abbrev-table 'ggtags-global-mode-abbrev-table '())

(define-abbrev-table 'ggtags-view-search-history-mode-abbrev-table '())

(define-abbrev-table 'git-commit-mode-abbrev-table '())

(define-abbrev-table 'git-rebase-mode-abbrev-table '())

(define-abbrev-table 'global-abbrev-table
  '(
    ("cert0" "certifications" nil 6)
    ("crev0" "certification_revisions" nil 0)
    ("ets0" "event_template_student" nil 32)
    ("etsa" "event_template_student_acl" nil 4)
    ("ld0" "location_detail" nil 18)
    ("pc0" "program_categories" nil 3)
    ("rdi0" "report_detail_instructor" nil 118)
   ))

(define-abbrev-table 'gnus-article-edit-mode-abbrev-table '())

(define-abbrev-table 'gnus-article-mode-abbrev-table '())

(define-abbrev-table 'gnus-browse-mode-abbrev-table '())

(define-abbrev-table 'gnus-category-mode-abbrev-table '())

(define-abbrev-table 'gnus-group-mode-abbrev-table '())

(define-abbrev-table 'gnus-score-mode-abbrev-table '())

(define-abbrev-table 'gnus-sticky-article-mode-abbrev-table '())

(define-abbrev-table 'graphviz-dot-mode-abbrev-table '())

(define-abbrev-table 'grep-mode-abbrev-table '())

(define-abbrev-table 'gud-mode-abbrev-table '())

(define-abbrev-table 'haskell-mode-abbrev-table '())

(define-abbrev-table 'helm-grep-mode-abbrev-table '())

(define-abbrev-table 'helm-moccur-mode-abbrev-table '())

(define-abbrev-table 'help-mode-abbrev-table '())

(define-abbrev-table 'html-helper-mode-abbrev-table '())

(define-abbrev-table 'html-mode-abbrev-table '())

(define-abbrev-table 'ibuffer-mode-abbrev-table '())

(define-abbrev-table 'idl-mode-abbrev-table '())

(define-abbrev-table 'image-dired-display-image-mode-abbrev-table '())

(define-abbrev-table 'image-dired-thumbnail-mode-abbrev-table '())

(define-abbrev-table 'inferior-emacs-lisp-mode-abbrev-table '())

(define-abbrev-table 'inferior-js-mode-abbrev-table '())

(define-abbrev-table 'inferior-maxima-mode-abbrev-table '())

(define-abbrev-table 'inferior-moz-mode-abbrev-table '())

(define-abbrev-table 'inferior-php-mode-abbrev-table '())

(define-abbrev-table 'inferior-python-mode-abbrev-table '())

(define-abbrev-table 'inferior-scheme-mode-abbrev-table '())

(define-abbrev-table 'inferior-tcl-mode-abbrev-table '())

(define-abbrev-table 'internal-ange-ftp-mode-abbrev-table '())

(define-abbrev-table 'jabber-console-mode-abbrev-table '())

(define-abbrev-table 'java-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'js-mode-abbrev-table '())

(define-abbrev-table 'js2-jsx-mode-abbrev-table '())

(define-abbrev-table 'js2-mode-abbrev-table '())

(define-abbrev-table 'jython-mode-abbrev-table '())

(define-abbrev-table 'latex-mode-abbrev-table '())

(define-abbrev-table 'lisp-mode-abbrev-table '())

(define-abbrev-table 'literate-haskell-mode-abbrev-table '())

(define-abbrev-table 'load-path-shadows-mode-abbrev-table '())

(define-abbrev-table 'log-edit-mode-abbrev-table '())

(define-abbrev-table 'log-view-mode-abbrev-table '())

(define-abbrev-table 'magit-branch-manager-mode-abbrev-table '())

(define-abbrev-table 'magit-cherry-mode-abbrev-table '())

(define-abbrev-table 'magit-commit-mode-abbrev-table '())

(define-abbrev-table 'magit-diff-mode-abbrev-table '())

(define-abbrev-table 'magit-log-edit-mode-abbrev-table '())

(define-abbrev-table 'magit-log-mode-abbrev-table '())

(define-abbrev-table 'magit-log-select-mode-abbrev-table '())

(define-abbrev-table 'magit-merge-preview-mode-abbrev-table '())

(define-abbrev-table 'magit-mode-abbrev-table '())

(define-abbrev-table 'magit-popup-mode-abbrev-table '())

(define-abbrev-table 'magit-popup-sequence-mode-abbrev-table '())

(define-abbrev-table 'magit-process-mode-abbrev-table '())

(define-abbrev-table 'magit-reflog-mode-abbrev-table '())

(define-abbrev-table 'magit-refs-mode-abbrev-table '())

(define-abbrev-table 'magit-revision-mode-abbrev-table '())

(define-abbrev-table 'magit-show-branches-mode-abbrev-table '())

(define-abbrev-table 'magit-stash-mode-abbrev-table '())

(define-abbrev-table 'magit-stashes-mode-abbrev-table '())

(define-abbrev-table 'magit-status-mode-abbrev-table '())

(define-abbrev-table 'magit-wazzup-mode-abbrev-table '())

(define-abbrev-table 'makefile-automake-mode-abbrev-table '())

(define-abbrev-table 'makefile-bsdmake-mode-abbrev-table '())

(define-abbrev-table 'makefile-gmake-mode-abbrev-table '())

(define-abbrev-table 'makefile-imake-mode-abbrev-table '())

(define-abbrev-table 'makefile-makepp-mode-abbrev-table '())

(define-abbrev-table 'makefile-mode-abbrev-table '())

(define-abbrev-table 'markdown-mode-abbrev-table '())

(define-abbrev-table 'matlab-mode-abbrev-table '())

(define-abbrev-table 'matlab-shell-help-mode-abbrev-table '())

(define-abbrev-table 'matlab-shell-topic-mode-abbrev-table '())

(define-abbrev-table 'maxima-noweb-mode-abbrev-table '())

(define-abbrev-table 'message-mode-abbrev-table '())

(define-abbrev-table 'messages-buffer-mode-abbrev-table '())

(define-abbrev-table 'mumamo-border-mode-abbrev-table '())

(define-abbrev-table 'mumamo-comment-mode-abbrev-table '())

(define-abbrev-table 'net-utils-mode-abbrev-table '())

(define-abbrev-table 'network-connection-mode-abbrev-table '())

(define-abbrev-table 'noshell-process-mode-abbrev-table '())

(define-abbrev-table 'nroff-mode-abbrev-table '())

(define-abbrev-table 'nslookup-mode-abbrev-table '())

(define-abbrev-table 'nxhtml-genshi-mode-abbrev-table '())

(define-abbrev-table 'nxhtml-mjt-mode-abbrev-table '())

(define-abbrev-table 'nxhtml-mode-abbrev-table '())

(define-abbrev-table 'nxml-mode-abbrev-table '())

(define-abbrev-table 'objc-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'occur-edit-mode-abbrev-table '())

(define-abbrev-table 'occur-mode-abbrev-table '())

(define-abbrev-table 'org-mode-abbrev-table '())

(define-abbrev-table 'outline-mode-abbrev-table '())

(define-abbrev-table 'package-menu-mode-abbrev-table '())

(define-abbrev-table 'perl-mode-abbrev-table '())

(define-abbrev-table 'php-doc-mode-abbrev-table '())

(define-abbrev-table 'php-mode-abbrev-table
  '(
    ("lln0" "Logger::log_notice" nil 1)
    ("t:d" "Tool::dump" nil 0)
    ("td0" "Tool::dump" nil 44)
    ("todu" "Tool::dump" nil 36)
   ))

(define-abbrev-table 'phpunit-run-mode-abbrev-table '())

(define-abbrev-table 'pike-mode-abbrev-table
  '(
   ))

(define-abbrev-table 'plain-tex-mode-abbrev-table '())

(define-abbrev-table 'proced-mode-abbrev-table '())

(define-abbrev-table 'process-menu-mode-abbrev-table '())

(define-abbrev-table 'prog-mode-abbrev-table '())

(define-abbrev-table 'projectile-rails-compilation-mode-abbrev-table '())

(define-abbrev-table 'projectile-rails-generate-mode-abbrev-table '())

(define-abbrev-table 'projectile-rails-server-mode-abbrev-table '())

(define-abbrev-table 'python-mode-abbrev-table '())

(define-abbrev-table 'python-mode-skeleton-abbrev-table '())

(define-abbrev-table 'rake-compilation-mode-abbrev-table '())

(define-abbrev-table 'reb-lisp-mode-abbrev-table '())

(define-abbrev-table 'reb-mode-abbrev-table '())

(define-abbrev-table 'restclient-mode-abbrev-table '())

(define-abbrev-table 'rmail-summary-mode-abbrev-table '())

(define-abbrev-table 'rspec-compilation-mode-abbrev-table '())

(define-abbrev-table 'rst-mode-abbrev-table '())

(define-abbrev-table 'rst-toc-mode-abbrev-table '())

(define-abbrev-table 'rt-liber-browser-mode-abbrev-table '())

(define-abbrev-table 'rt-liber-viewer-mode-abbrev-table '())

(define-abbrev-table 'ruby-compilation-mode-abbrev-table '())

(define-abbrev-table 'ruby-mode-abbrev-table '())

(define-abbrev-table 'scheme-mode-abbrev-table '())

(define-abbrev-table 'select-tags-table-mode-abbrev-table '())

(define-abbrev-table 'sgml-mode-abbrev-table '())

(define-abbrev-table 'sh-mode-abbrev-table '())

(define-abbrev-table 'shell-mode-abbrev-table '())

(define-abbrev-table 'sldb-mode-abbrev-table '())

(define-abbrev-table 'slime-compiler-notes-mode-abbrev-table '())

(define-abbrev-table 'slime-connection-list-mode-abbrev-table '())

(define-abbrev-table 'slime-fuzzy-completions-mode-abbrev-table '())

(define-abbrev-table 'slime-inspector-mode-abbrev-table '())

(define-abbrev-table 'slime-thread-control-mode-abbrev-table '())

(define-abbrev-table 'slime-trace-dialog--detail-mode-abbrev-table '())

(define-abbrev-table 'slime-trace-dialog--mode-abbrev-table '())

(define-abbrev-table 'slime-trace-dialog-mode-abbrev-table '())

(define-abbrev-table 'slime-xref-mode-abbrev-table '())

(define-abbrev-table 'slitex-mode-abbrev-table '())

(define-abbrev-table 'smarty-mode-abbrev-table
  '(
    ("assign" "" smarty-template-assign-hook 0)
    ("bbcodetohtml" "" smarty-template-bbcodetohtml-hook 0)
    ("btosmilies" "" smarty-template-btosmilies-hook 0)
    ("capitalize" "" smarty-template-capitalize-hook 0)
    ("capture" "" smarty-template-capture-hook 0)
    ("cat" "" smarty-template-cat-hook 0)
    ("clipcache" "" smarty-template-clipcache-hook 0)
    ("config_load" "" smarty-template-config-load-hook 0)
    ("count_characters" "" smarty-template-count-characters-hook 0)
    ("count_paragraphs" "" smarty-template-count-paragraphs-hook 0)
    ("count_sentences" "" smarty-template-count-sentences-hook 0)
    ("count_words" "" smarty-template-count-words-hook 0)
    ("counter" "" smarty-template-counter-hook 0)
    ("cycle" "" smarty-template-cycle-hook 0)
    ("date_format" "" smarty-template-date-format-hook 0)
    ("date_formatto" "" smarty-template-date-formatto-hook 0)
    ("debug" "" smarty-template-debug-hook 0)
    ("default" "" smarty-template-default-hook 0)
    ("else" "" smarty-template-else-hook 0)
    ("elseif" "" smarty-template-elseif-hook 0)
    ("escape" "" smarty-template-escape-hook 0)
    ("eval" "" smarty-template-eval-hook 0)
    ("fetch" "" smarty-template-fetch-hook 0)
    ("foreach" "" smarty-template-foreach-hook 0)
    ("foreachelse" "" smarty-template-foreachelse-hook 0)
    ("formtool_checkall" "" smarty-template-formtool-checkall-hook 0)
    ("formtool_copy" "" smarty-template-formtool-copy-hook 0)
    ("formtool_count_chars" "" smarty-template-formtool-count-chars-hook 0)
    ("formtool_init" "" smarty-template-formtool-init-hook 0)
    ("formtool_move" "" smarty-template-formtool-move-hook 0)
    ("formtool_moveall" "" smarty-template-formtool-moveall-hook 0)
    ("formtool_movedown" "" smarty-template-formtool-movedown-hook 0)
    ("formtool_moveup" "" smarty-template-formtool-moveup-hook 0)
    ("formtool_remove" "" smarty-template-formtool-remove-hook 0)
    ("formtool_rename" "" smarty-template-formtool-rename-hook 0)
    ("formtool_save" "" smarty-template-formtool-save-hook 0)
    ("formtool_selectall" "" smarty-template-formtool-selectall-hook 0)
    ("html_checkboxes" "" smarty-template-html-checkboxes-hook 0)
    ("html_image" "" smarty-template-html-image-hook 0)
    ("html_options" "" smarty-template-html-options-hook 0)
    ("html_radios" "" smarty-template-html-radios-hook 0)
    ("html_select_date" "" smarty-template-html-select-date-hook 0)
    ("html_select_time" "" smarty-template-html-select-time-hook 0)
    ("html_table" "" smarty-template-html-table-hook 0)
    ("if" "" smarty-template-if-hook 0)
    ("include" "" smarty-template-include-hook 0)
    ("include_clipcache" "" smarty-template-include-clipcache-hook 0)
    ("include_php" "" smarty-template-include-php-hook 0)
    ("indent" "" smarty-template-indent-hook 0)
    ("insert" "" smarty-template-insert-hook 0)
    ("ldelim" "" smarty-template-ldelim-hook 0)
    ("literal" "" smarty-template-literal-hook 0)
    ("lower" "" smarty-template-lower-hook 0)
    ("mailto" "" smarty-template-mailto-hook 0)
    ("math" "" smarty-template-math-hook 0)
    ("nl2br" "" smarty-template-nl2br-hook 0)
    ("paginate_first" "" smarty-template-paginate-first-hook 0)
    ("paginate_last" "" smarty-template-paginate-last-hook 0)
    ("paginate_middle" "" smarty-template-paginate-middle-hook 0)
    ("paginate_next" "" smarty-template-paginate-next-hook 0)
    ("paginate_prev" "" smarty-template-paginate-prev-hook 0)
    ("php" "" smarty-template-php-hook 0)
    ("popup" "" smarty-template-popup-hook 0)
    ("popup_init" "" smarty-template-popup-init-hook 0)
    ("rdelim" "" smarty-template-rdelim-hook 0)
    ("regex_replace" "" smarty-template-regex-replace-hook 0)
    ("repeat" "" smarty-template-repeat-hook 0)
    ("replace" "" smarty-template-replace-hook 0)
    ("section" "" smarty-template-section-hook 0)
    ("sectionelse" "" smarty-template-sectionelse-hook 0)
    ("spacify" "" smarty-template-spacify-hook 0)
    ("str_repeat" "" smarty-template-str-repeat-hook 0)
    ("string_format" "" smarty-template-string-format-hook 0)
    ("strip" "" smarty-template-vstrip-hook 0)
    ("strip_tags" "" smarty-template-strip-tags-hook 0)
    ("textformat" "" smarty-template-textformat-hook 0)
    ("truncate" "" smarty-template-truncate-hook 0)
    ("upper" "" smarty-template-upper-hook 0)
    ("validate" "" smarty-template-validate-hook 0)
    ("wordwrap" "" smarty-template-wordwrap-hook 0)
   ))

(define-abbrev-table 'smbclient-mode-abbrev-table '())

(define-abbrev-table 'smime-mode-abbrev-table '())

(define-abbrev-table 'snippet-mode-abbrev-table '())

(define-abbrev-table 'special-mode-abbrev-table '())

(define-abbrev-table 'speedbar-mode-abbrev-table '())

(define-abbrev-table 'sql-mode-abbrev-table
  '(
    ("aet0" "agency_event_template" nil 86)
    ("as0" "agency_student" nil 1)
    ("ast0" "agency_student" nil 74)
    ("av0" "assigned_value" nil 290)
    ("cc0" "certification_collections" nil 5)
    ("ck0" "config_key" nil 19)
    ("cmp0" "certification_manager_production" nil 1)
    ("cnt0" "COUNT(*)" nil 77)
    ("co0" "config_option" nil 59)
    ("con0" "config_option_name" nil 5)
    ("cv0" "config_value" nil 193)
    ("del0" "DELETE" nil 0)
    ("dis0" "DISTINCT" nil 1)
    ("dq0" "detail_question" nil 160)
    ("ep0" "education_program" nil 1)
    ("esc0" "event_schedule" nil 0)
    ("est0" "event_student" nil 14)
    ("et0" "event_template" nil 160)
    ("etdq0" "event_template_detail_question" nil 59)
    ("eti0" "event_template_id" nil 915)
    ("ets0" "event_template_student" nil 95)
    ("evst0" "event_student" nil 11)
    ("gb0" "GROUP BY" nil 36)
    ("idr0" "instructor_daily_report" nil 4)
    ("ii0" "inventory_item" nil 13)
    ("ij0" "INNER JOIN" nil 380)
    ("ins0" "INSERT" nil 1)
    ("is0" "information_schema" nil 0)
    ("iwl0" "invite_wait_list" nil 19)
    ("lj0" "LEFT JOIN" nil 7)
    ("ob0" "ORDER BY" nil 125)
    ("pd0" "proctor_detail" nil 123)
    ("ph0" "password_history" nil 9)
    ("qk0" "question_key" nil 7)
    ("rd0" "registration_detail" nil 346)
    ("reg0" "registration" nil 1266)
    ("rptd0" "report_detail" nil 17)
    ("rs0" "result_set" nil 106)
    ("rsn0" "result_set_name" nil 2)
    ("sel0" "SELECT" nil 1)
    ("wh0" "WHERE" nil 1)
    ("wl0" "wait_list" nil 15)
   ))

(define-abbrev-table 'svn-log-edit-mode-abbrev-table '())

(define-abbrev-table 'svn-log-view-mode-abbrev-table '())

(define-abbrev-table 'svn-status-diff-mode-abbrev-table '())

(define-abbrev-table 'tabulated-list-mode-abbrev-table '())

(define-abbrev-table 'tar-mode-abbrev-table '())

(define-abbrev-table 'tcl-mode-abbrev-table '())

(define-abbrev-table 'term-mode-abbrev-table '())

(define-abbrev-table 'tex-mode-abbrev-table '())

(define-abbrev-table 'tex-shell-abbrev-table '())

(define-abbrev-table 'texinfo-mode-abbrev-table '())

(define-abbrev-table 'text-mode-abbrev-table
  '(
    ("con" ".. contents::
..
   " nil 0)
   ))

(define-abbrev-table 'tree-mode-abbrev-table '())

(define-abbrev-table 'udev-control-mode-abbrev-table '())

(define-abbrev-table 'url-cookie-mode-abbrev-table '())

(define-abbrev-table 'vc-annotate-mode-abbrev-table '())

(define-abbrev-table 'vc-bzr-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-bzr-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-dir-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-git-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-hg-incoming-mode-abbrev-table '())

(define-abbrev-table 'vc-hg-log-edit-mode-abbrev-table '())

(define-abbrev-table 'vc-hg-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-hg-outgoing-mode-abbrev-table '())

(define-abbrev-table 'vc-mtn-log-view-mode-abbrev-table '())

(define-abbrev-table 'vc-svn-log-view-mode-abbrev-table '())

(define-abbrev-table 'wab-compilation-mode-abbrev-table '())

(define-abbrev-table 'wave-display-mode-abbrev-table '())

(define-abbrev-table 'web-mode-abbrev-table '())

(define-abbrev-table 'web-vcs-investigate-output-mode-abbrev-table '())

(define-abbrev-table 'yaml-mode-abbrev-table '())

(define-abbrev-table 'yas/rails-erb-mode-abbrev-table '())
