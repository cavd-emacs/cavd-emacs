(use-package amx
  :ensure t
  :commands (execute-extended-command counsel-M-x)
  :config
  (setq amx-backend 'ivy
        amx-history-length 111)
  (amx-mode 1))

;; Ivy/Swiper/Counsel
(use-package historian
  :ensure t
  :hook
  (kill-emacs . historian-save))

(use-package ivy-historian
  :ensure t
  :config
  (setq ivy-historian-freq-boost-factor most-positive-fixnum)
  (setq ivy-historian-recent-boost 1111))

(use-package ivy
  :ensure t
  :diminish
  :commands (ivy-avy ivy-resume)

  :bind
  (("C-c C-o" . ivy-occur)
   ("s-r" . ivy-resume)
   ("<s-up>" . ivy-push-view)
   ("<s-down>" . ivy-switch-view))
  :hook
  (ivy-occur-mode . hl-line-mode)
  (ivy-occur-grep-mode . toggle-truncate-lines)
  (eshell-mode . setup-eshell-ivy-completion)
  :init
  (ivy-mode +1)
  (historian-mode +1)

  :config
  (ivy-historian-mode +1)
  (setq projectile-completion-system 'ivy)
  (setq synosaurus-choose-method 'ivy)
  (defun setup-eshell-ivy-completion ()
    ;; only if you want to use the minibuffer for completions instead of the
    ;; in-buffer interface
    (setq-local ivy-display-functions-alist
                (remq (assoc
                       'ivy-completion-in-region
                       ivy-display-functions-alist)
                      ivy-display-functions-alist)))

  ;; From ivy manual
  (defun ivy-yank-action (x)
    (kill-new x))

  (defun ivy-copy-to-buffer-action (x)
    (with-ivy-window
      (insert x)))

  (setq ivy-use-virtual-buffers t)
  (setq ivy-wrap t)
  (setq ivy-use-selectable-prompt t)
  (ivy-set-occur 'swiper 'swiper-occur)
  (ivy-set-occur 'swiper-isearch 'swiper-occur)
  (ivy-set-occur 'counsel-fzf 'counsel-fzf-occur)
  (ivy-set-occur 'counsel-git-grep 'counsel-git-grep-occur)
  (ivy-set-occur 'counsel-ag 'counsel-ag-occur)
  (ivy-set-occur 'counsel-rg 'counsel-rg-occur)
  (ivy-set-occur 'counsel-grep 'counsel-grep-occur)
  (ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)
  ;; (setq ivy-re-builders-alist '((swiper . ivy--regex-plus)
  ;;                               (counsel-grep-or-swiper . ivy--regex-plus)
  ;;                               (t . orderless-ivy-re-builder)))
  ;; My way
;;   (setq ivy-re-builders-alist
;;         '((swiper . ivy--regex-plus)
;;           (counsel-ag . ivy--regex-plus)
;;           (counsel-grep-or-swiper . ivy--regex-plus)
;;           (counsel-M-x . orderless-ivy-re-builder)
;;           (counsel-fzf . ivy--regex)
;;           (t . ivy--regex-fuzzy)))
;;   (push '(counsel-describe-function .  "^") ivy-initial-inputs-alist)
;;   (push '(counsel-describe-variable .  "^") ivy-initial-inputs-alist)
;; ;  (push '(counsel-M-x .  "^") ivy-initial-inputs-alist)
;;   (push '(counsel-ag .  "--file-search-regex '' -- ") ivy-initial-inputs-alist)
;;   (push '(counsel-rg .  "--glob '**' -- ") ivy-initial-inputs-alist)
;;   (push '(ivy-switch-buffer . "^") ivy-initial-inputs-alist)
;;   (setq ivy-flx-limit 1000
;;         ivy-sort-matches-functions-alist
;;         '((ivy-switch-buffer . ivy-sort-function-buffer)
;;           (counsel-find-file . ivy-sort-function-buffer)
;;           (counsel-M-x . ivy-sort-function-buffer)
;;           (t . nil)))
   (setq ivy-re-builders-alist
         '((swiper . ivy--regex-plus)
           (counsel-ag . ivy--regex-plus)
           (counsel-grep-or-swiper . ivy--regex-plus)
           (counsel-fzf . ivy--regex-fuzzy)
           (counsel-M-x . ivy--regex-fuzzy)
           (describe-package . ivy--regex-plus)
           (t . ivy--regex-fuzzy)))
   (push '(counsel-ag .  "--file-search-regex '' -- ") ivy-initial-inputs-alist)
   (push '(counsel-rg .  "--glob '**' -- ") ivy-initial-inputs-alist)
   (push '(counsel-M-x . "^") ivy-initial-inputs-alist)
;  (push '(ivy-switch-buffer . "^") ivy-initial-inputs-alist)
   (setq ivy-flx-limit 1000)
   (setq ivy-sort-matches-functions-alist
         '((ivy-switch-buffer . ivy-sort-function-buffer)
           (counsel-M-x . ivy-sort-function-buffer)
           (t . nil)))

  (fset 'flyspell-emacs-popup 'flyspell-ivy-prompt)

   (setq ivy-views
         '(("cavd-init {}"
            (horz
             (file "cavd-local.el")
             (file "cavd-erc-funcs.el")))))
  (ivy-set-actions
   'ivy-switch-buffer
   '(("k"
      (lambda (x)
        (kill-buffer x)
        (ivy--reset-state ivy-last))
      "kill")
     ("j"
      ivy--switch-buffer-other-window-action
      "other")))
  (ivy-set-actions
   t
   '(("i" ivy-copy-to-buffer-action "insert")
     ("y" ivy-yank-action "yank"))))

;; (use-package ivy-fuz
;;   :ensure t
;;   :demand t
;;   :after ivy
;;   :config
;; ;  (setq ivy-sort-matches-functions-alist '((t . ivy-fuz-sort-fn)))
;;   (setq ivy-re-builders-alist '((t . ivy-fuz-regex-fuzzy)))
;;   (add-to-list
;;    'ivy-highlight-functions-alist
;;    '(ivy-fuz-regex-fuzzy . ivy-fuz-highlight-fn)))

(use-package swiper
  :ensure t
  :bind
  (("s-g" . swiper-thing-at-point)
   ("M-s s" . swiper-all)
   ("s-'" . swiper-avy)
   ("M-s m" . swiper-multi))
  :bind (:map swiper-map
              ("M-y" . yank)
              ("M-%" . swiper-query-replace)
              ("C-." . swiper-avy)
              ("M-c" . swiper-mc))
  :bind (:map isearch-mode-map
              ("C-o" . swiper-from-isearch))
  :init
  (setq swiper-action-recenter t)
  (setq swiper-include-line-number-in-search t)
  (setq swiper-goto-start-of-match t))

(use-package counsel
  :after ivy
  :ensure t
  :commands (counsel-fzf aw-window-list prot/counsel-fzf-rg-files)
  :custom
  (counsel-rg-base-command
   "rg -SHn --no-heading --color never --no-follow --hidden %s")
  (counsel-find-file-occur-cmd          ; TODO Simplify this
   "ls -a | grep -i -E '%s' | tr '\\n' '\\0' | xargs -0 ls -d --group-directories-first")

  :bind
  (("s-f" . counsel-grep-or-swiper)
   ("M-x" . counsel-M-x)
   ("C-c s-i" . counsel-imenu)
   ("s-o" . counsel-find-file)
   ("C-h f" . counsel-describe-function)
   ("C-h v" . counsel-describe-variable)
   ("C-h b" . counsel-descbinds)
   ("C-h i" . counsel-info-lookup-symbol)
   ("C-h y" . counsel-find-library)
   ("s-a" . counsel-ag)
   ("C-c !" . cavd-counsel-ag)
   ("M-s t" . counsel-git-grep)
   ("C-c t" . counsel-git)
   ("C-c j" . counsel-ag-projectile)
   ("C-c L" . counsel-git-log)
   ("M-s e" . counsel-rg)
   ("M-s z" . prot/counsel-fzf-rg-files)
   ("C-c A" . cavd-counsel-rg)
   ("C-c u" . counsel-unicode-char)
   ("s-z" . counsel-fzf)
   ("s-y" . counsel-yank-pop)
   ("C-x C-r" . counsel-recentf)
   ("C-x M-x" . counsel-command-history)
   ("C-x d" . counsel-dired)
   :map ivy-minibuffer-map
   ("C-r" . counsel-minibuffer-history)
   ("s-y" . ivy-next-line)        ; Avoid 2× `counsel-yank-pop'
   ("C-SPC" . ivy-restrict-to-matches))

  :config
  (setq counsel-describe-function-function #'helpful-callable
        counsel-describe-variable-function #'helpful-variable)

  (advice-add 'counsel-projectile-switch-project-action
              :override 'counsel-projectile-switch-project-action-find-file)
  (advice-add 'counsel-projectile-find-file :override '+projectile-find-file)

  (define-key read-expression-map (kbd "C-r")
    'counsel-expression-history)
  (define-key minibuffer-local-map
    (kbd "C-r") 'counsel-minibuffer-history)

  :init
  ;; From prot
  (defun prot/counsel-fzf-rg-files (&optional input dir)
    "Run `fzf' in tandem with `ripgrep' to find files in the
present directory.  If invoked from inside a version-controlled
repository, then the corresponding root is used instead."
    (interactive)
    (let* ((process-environment
            (cons (concat "FZF_DEFAULT_COMMAND=rg -Sn --color never --files --no-follow --hidden")
                  process-environment))
           (vc (vc-root-dir)))
      (if dir
          (counsel-fzf input dir)
        (if (eq vc nil)
            (counsel-fzf input default-directory)
          (counsel-fzf input vc)))))

  (defun prot/counsel-fzf-dir (arg)
    "Specify root directory for `counsel-fzf'."
    (prot/counsel-fzf-rg-files ivy-text
                               (read-directory-name
                                (concat (car (split-string counsel-fzf-cmd))
                                        " in directory: "))))

  (defun prot/counsel-rg-dir (arg)
    "Specify root directory for `counsel-rg'."
    (let ((current-prefix-arg '(4)))
      (counsel-rg ivy-text nil "")))

  ;; TODO generalise for all relevant file/buffer counsel-*?
  (defun prot/counsel-fzf-ace-window (arg)
    "Use `ace-window' on `prot/counsel-fzf-rg-files' candidate."
    (ace-window t)
    (let ((default-directory (if (eq (vc-root-dir) nil)
                                 counsel--fzf-dir
                               (vc-root-dir))))
      (if (> (length (aw-window-list)) 1)
          (progn
            (find-file arg))
        (find-file-other-window arg))
      (balance-windows)))

  ;; Pass functions as appropriate Ivy actions (accessed via M-o)
  (ivy-add-actions
   'counsel-fzf
   '(("r" prot/counsel-fzf-dir "change root directory")
     ("g" prot/counsel-rg-dir "use ripgrep in root directory")
     ("a" prot/counsel-fzf-ace-window "ace-window switch")))

  (ivy-add-actions
   'counsel-rg
   '(("r" prot/counsel-rg-dir "change root directory")
     ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))

  (ivy-add-actions
   'counsel-find-file
   '(("g" prot/counsel-rg-dir "use ripgrep in root directory")
     ("z" prot/counsel-fzf-dir "find file with fzf in root directory")))

  (defun counsel-ag-projectile ()
    "Run `counsel-ag' within the current project."
    (interactive)
    (counsel-ag nil (projectile-project-root)))

  (defun counsel-rg-occur ()
    "Generate a custom occur buffer for `counsel-rg'."
    (counsel-grep-like-occur
     counsel-rg-base-command))

  (defun cavd-counsel-ag ()
    "Call `counsel-ag' with `symbol-at-point'."
    (interactive)
    (counsel-ag (symbol-name (symbol-at-point))))

  (defun cavd-counsel-rg ()
    "Call `counsel-rg' with `symbol-at-point'."
    (interactive)
    (counsel-rg (symbol-name (symbol-at-point))))

  (setq counsel-grep-base-command
        "rg -i -M 120 --no-heading --line-number --color never '%s' %s")
  (setq counsel-find-file-at-point t
        counsel-yank-pop-preselect-last t)
  (setq counsel-ag-base-command
        (concat "ag "
                "--nocolor "
                "--line-number "
                "--column "
                "--nogroup "
                "--smart-case "
                "--follow "             ; follow symlinks
                "%s")))

(use-package counsel-projectile
  :ensure t
  :commands (counsel-projectile-switch-project counsel-projectile-find-dir)
  :config
  (add-to-list 'ivy-initial-inputs-alist '(counsel-projectile-switch-project . ""))
  (setq counsel-projectile-remove-current-buffer t)
  (setq counsel-projectile-sort-buffers nil)
  :hook
  (after-init . counsel-projectile-mode)
  ;; :bind-keymap ("M-s p" . projectile-command-map)
  :bind (("C-c s-p" . counsel-projectile-switch-project)
         ("C-c s-b" . counsel-projectile-switch-to-buffer)
         ("C-c s-d" . counsel-projectile-find-dir)))

(use-package counsel-tramp
  :ensure t
  :after (counsel vagrant-tramp)
  :bind
  ("s-v" . counsel-tramp)
  :hook
  (counsel-tramp-pre-command . (lambda ()
                                 (projectile-mode 0)))
  (counsel-tramp-quit . (lambda ()
                          (projectile-mode 1))))

(use-package ivy-rich
  :after ivy
  :ensure t
  :init
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (setq ivy-rich-display-transformers-list
        '(ivy-switch-buffer
          (:columns
           (
            (ivy-switch-buffer-transformer (:width 50))
            (ivy-rich-switch-buffer-size (:width 7 :align right))
            (ivy-rich-switch-buffer-indicators (:width 3 :face error :align right))
            (ivy-rich-switch-buffer-major-mode (:width 13 :face warning))
            (ivy-rich-switch-buffer-project (:width 15 :face success))
            (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
           :predicate
           (lambda (cand) (get-buffer cand)))
          counsel-M-x
          (:columns
           ((counsel-M-x-transformer (:width 40)) ; thr original transformer
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face)))) ; return the docstring of the command
          counsel-describe-function
          (:columns
           ((counsel-describe-function-transformer (:width 40)) ; the original transformer
            (ivy-rich-counsel-function-docstring (:face font-lock-doc-face)))) ; return the docstring of the function
          counsel-describe-variable
          (:columns
           ((counsel-describe-variable-transformer (:width 40)) ; the original transformer
            (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face)))) ; return the docstring of the variable
          counsel-recentf
          (:columns
           ((ivy-rich-candidate (:width 0.8 :face font-lock-string-face)) ; return the candidate itself
            (ivy-rich-file-last-modified-time (:face font-lock-string-face))))
          counsel-find-file
          (:columns
           ((ivy-read-file-transformer)
            (ivy-rich-counsel-find-file-truename
             (:face font-lock-doc-face))))))
  (setq ivy-virtual-abbreviate 'full
        ivy-rich-parse-remote-buffer t
        ivy-rich-path-style 'relative)
  ;; (ivy-mode +1)
  ;; (historian-mode +1)

  :hook
  (after-init . ivy-rich-mode))


(use-package flyspell-correct-ivy
  :init
  (setq flyspell-correct-interface #'flyspell-correct-ivy)
  :bind ("C-M-;" . flyspell-correct-wrapper))

;; if you are ivy user
(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)

(setq dumb-jump-selector 'ivy)

(provide 'cavd-ivy-counsel-swiper)
