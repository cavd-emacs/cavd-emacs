(use-package clojure-mode
  :ensure t
  :config
  (add-hook 'clojure-mode-hook 'paredit-mode)
  (add-hook 'clojure-mode-hook 'subword-mode))

(use-package cider
  :ensure t
  :config
  (setq nrepl-log-messages t)
;  (add-hook 'cider-mode-hook 'eldoc-mode)
;  (add-hook 'cider-repl-mode-hook 'eldoc-mode)
  (add-hook 'cider-repl-mode-hook 'paredit-mode))

(provide 'cavd-clojure-funcs)
