;; Jabber

(require 'jabber)
(load "jabber-autoloads")

(defvar tokyo-message-received-sound "/Applications/Adium.app/Contents/Resources/Sounds/TokyoTrainStation.AdiumSoundset/Message_Received.m4a")

(defvar tokyo-new-message-sound "/Applications/Adium.app/Contents/Resources/Sounds/TokyoTrainStation.AdiumSoundset/New_Message.m4a")

(defvar tokyo-message-sent-sound "/Applications/Adium.app/Contents/Resources/Sounds/TokyoTrainStation.AdiumSoundset/Message_Sent.m4a")

(defvar tokyo-message-contact-on "/Applications/Adium.app/Contents/Resources/Sounds/TokyoTrainStation.AdiumSoundset/Contact_On.m4a")

(defvar tokyo-message-contact-off "/Applications/Adium.app/Contents/Resources/Sounds/TokyoTrainStation.AdiumSoundset/Contact_Off.m4a")

(defvar *hc-rooms*
  '(("Certification Manager" "20457_certification_manager@conf.hipchat.com")
    ("Content and Publications" "20457_content_and_publications@conf.hipchat.com")
    ("Customer Service" "20457_customer_service@conf.hipchat.com")
    ("Data Transfer" "20457_data_transfer@conf.hipchat.com")
    ("Developers" "20457_developers@conf.hipchat.com")
    ("Event Manager" "20457_event_manager@conf.hipchat.com")
    ("Fishing 101" "20457_fishing_101@conf.hipchat.com")
    ("Git" "20457_git@conf.hipchat.com")
    ("Hubot Testing" "20457_hubot_testing@conf.hipchat.com")
    ("Hunting Lottery" "20457_hunting_lottery@conf.hipchat.com")
    ("Kalkomey Inc" "20457_kalkomey_inc@conf.hipchat.com")
    ("Managed Courses" "20457_managed_course_3.0@conf.hipchat.com")
    ("Marketing" "20457_marketing@conf.hipchat.com")
    ("MC Static Content" "20457_mc_static_content@conf.hipchat.com")
    ("Month-end" "20457_month-end@conf.hipchat.com")
    ("MySQL Troubleshooting" "20457_mysql_troubleshooting@conf.hipchat.com")
    ("New Course Creation" "20457_new_course_creation@conf.hipchat.com")
    ("Rails" "20457_rails@conf.hipchat.com")
    ("SEO" "20457_seo@conf.hipchat.com")
    ("Shopify" "20457_shopify@conf.hipchat.com")
    ("Support" "20457_support@conf.hipchat.com")
    ("Systems" "20457_systems@conf.hipchat.com")
    ("Technology" "20457_technology@conf.hipchat.com")
    ("EM Releases" "20457_testing_em_student@conf.hipchat.com")
    ("Testing Wait List" "20457_testing_wait_list@conf.hipchat.com")
    ("User Experience" "20457_user_experience@conf.hipchat.com")
    ("Wait List" "20457_wait_list@conf.hipchat.com")
    ("WrightIMC" "20457_wrightimc@conf.hipchat.com")
    ("Zendesk" "20457_zendesk@conf.hipchat.com")))

(defvar cavd-jabber-muc-autojoin
  '("20457_certification_manager@conf.hipchat.com"
    "20457_managed_course_3.0@conf.hipchat.com"
    "20457_systems@conf.hipchat.com"
    "20457_testing_em_student@conf.hipchat.com"
    "20457_testing_wait_list@conf.hipchat.com"
    "20457_customer_service@conf.hipchat.com"
    "20457_support@conf.hipchat.com"
    "20457_data_transfer@conf.hipchat.com"
    "20457_new_course_creation@conf.hipchat.com"
    "20457_event_manager@conf.hipchat.com")
  "List of rooms to join at start-up.")

;; Assign the nick to the autojoin rooms
(setq cavd-jabber-muc-default-nicknames
      (mapcar (lambda (room)
                (cons room erc-user-full-name))
              cavd-jabber-muc-autojoin))

;; Various settings
(setq jabber-use-global-history nil
      jabber-history-enabled t
      jabber-history-enable-rotation t
      jabber-history-muc-enabled t
      jabber-alert-presence-hooks nil
      jabber-autoaway-verbose t
      jabber-auto-reconnect t
      jabber-backlog-days 5.0
      jabber-backlog-number 71
      jabber-chat-buffer-show-avatar nil
      jabber-muc-colorize-foreign t
      jabber-roster-show-title nil
      jabber-show-resources nil
      jabber-vcard-avatars-retrieve nil)

(setq jabber-roster-line-format " %c %-25n %u %-8s  %S")
(setq my-chat-prompt "[%t] %u> ")
(setq jabber-chat-foreign-prompt-format my-chat-prompt
      jabber-chat-local-prompt-format my-chat-prompt
      jabber-groupchat-prompt-format my-chat-prompt
      jabber-muc-completion-delimiter " "
      jabber-muc-private-foreign-prompt-format "[%t] %g/%u> ")

(setq jabber-account-list
   '(("chris0123456789@gmail.com"
      (:network-server . "talk.google.com")
      (:connection-type . ssl))
     ("20457_94603@chat.hipchat.com"
      (:network-server . "conf.hipchat.com")
      (:connection-type . ssl))))

(defun cavd-message-received ()
  "Play a sound when a message is received."
  (cavd-play-sound-file tokyo-message-received-sound))

(defun cavd-new-message ()
  "Play a sund for new messages."
  (cavd-play-sound-file tokyo-new-message-sound))

(defun cavd-tokyo-message-sent ()
  "Play a sound when a message is sent"
  (cavd-play-sound-file tokyo-message-sent-sound))

(defun cavd-contact-on ()
  "Play a sound when a contact signs on."
  (cavd-play-sound-file tokyo-message-contact-on))

(defun cavd-contact-off ()
  "Play a sound when a contact signs off."
  (cavd-play-sound-file tokyo-message-contact-off))

(defun cavd-contact-alert (who old-stat new-stat status-text prop-alert)
  (cond ((and (string= old-stat "") (null new-stat))
         (cavd-contact-off))
        ((and (null old-stat) (string= "" new-stat))
         (cavd-contact-on))))

(defun cavd-jabber-connect ()
  "Connect to jabber and join some HC rooms."
  (interactive)
  (jabber-connect-all)
  (message "Connected")
  (setq hipchat-account (jabber-read-account))
  (cavd-jabber-muc-autojoin))

(defun cavd-jabber-disconnect ()
  "Disconnect from jabber, and kill the buffers
associated with it."
  (interactive)
;  (cavd-kill-jabber-buffers)
  (jabber-disconnect)
  (setq hipchat-account nil))

(defun cavd-jabber-muc-autojoin ()
  "Join rooms specified in account bookmarks and
global `cavd-jabber-muc-default-nicknames'."
  (interactive)
  (mapcar
   (lambda (r)
     (jabber-muc-join hipchat-account (car r) (cdr r) t))
          cavd-jabber-muc-default-nicknames))

(defun cavd-kill-jabber-buffers ()
  "Kill all active jabber buffers."
  (interactive)
  (cl-dolist (groupchat *jabber-active-groupchats*)
    (let ((buff (concat "*-jabber-groupchat-" (car groupchat) "-*")))
      (kill-buffer buff))))

(defun old-cavd-kill-jabber-buffers ()
  "Kill all buffers related to jabber."
  (interactive)
  (let ((buffers
         (list
          (intern "jabber-chat-mode")
          (intern "jabber-roster-mode"))))
    (mapcar (lambda (buf)
              (if (member (buffer-local-value 'major-mode buf) buffers)
                  (kill-buffer buf)))
            (buffer-list))))

;; Find the human-readable name of a room
;; Can be used for renaming jabber buffers
(defun cavd-find-name-of-room ()
  (cl-dolist (room cavd-jabber-muc-autojoin)
    (message
     "%S"
     (car
      (cl-rassoc-if (lambda (r)
                      (equal room (car r)))
                    *hc-rooms*)))))

(defun hipchat-join ()
  "Ask for a room, then join it."
  (interactive)
  (let ((room (hipchat-choose-room)))
    (if room
        (jabber-muc-join
         hipchat-account
         room
         erc-user-full-name
         t))))

(defun hipchat-choose-room ()
  (interactive)
  (let ((room
         (completing-read
          "Room: "
          (cl-mapcar (lambda (p)
                       (car p))
                     (cdr *hc-rooms*)))))
    (if room
        (car (cdr (assoc room *hc-rooms*))))))

;; Mention nicknames in a way that HipChat clients will pickup
(defun hipchat-mention (nickname)
  (interactive
   (list (jabber-muc-read-nickname jabber-group "Nickname: ")))
  (insert (concat "@" (replace-regexp-in-string " " "" nickname) " ")))

(define-key jabber-chat-mode-map (kbd "C-c C-n") 'jabber-muc-names)
(define-key jabber-chat-mode-map (kbd "C-c @") 'hipchat-mention)

;; Adjust hooks
(remove-hook 'jabber-alert-muc-hooks 'jabber-muc-echo)
(remove-hook 'jabber-alert-message-hooks 'jabber-message-echo)

(add-hook 'jabber-chat-mode-hook 'goto-address)

(add-hook 'jabber-chat-mode-hook
          (lambda()
            (flyspell-mode 1)))

(add-hook 'jabber-alert-presence-hooks 'cavd-contact-alert)

(provide 'cavd-jabber)
