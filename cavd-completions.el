;;; cavd-completions.el --- Completions

;;; Commentary:

;;; Code:
;(use-package flx :defer t)

;; (use-package fuz
;;   :load-path "~/elisp/fuz.el"
;;   :config
;;   (unless (require 'fuz-core nil t)
;;     (fuz-build-and-load-dymod)))

;; (setq completion-styles '(basic))

;; (setq completion-category-overrides
;;       '((file (styles . (basic)))
;;         (buffer (styles . (basic)))
;;         (info-menu (styles . (basic)))))

(setq pcomplete-ignore-case t)

(use-package hippie-exp
  :commands hippie-expand
  :bind
  ("C-M-/" . hippie-expand)
  ("s-/" . hippie-expand)
  :config
  (setq hippie-expand-try-functions-list
        '(try-expand-dabbrev
          try-expand-all-abbrevs
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill
          try-expand-list
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-line
          try-complete-lisp-symbol-partially
          try-complete-lisp-symbol)))

;;; dabbrev
(use-package dabbrev
  :commands dabbrev-expand
  :bind
  ("M-/" . dabbrev-expand)
  :config
  (setq dabbrev-abbrev-char-regexp "\\(\\sw\\|\\s_\\)"
        dabbrev-abbrev-skip-leading-regexp "\\$\\|\\*\\|/\\|="
        dabbrev-case-distinction nil
        dabbrev-case-replace nil
        dabbrev-upcase-means-case-search t))

;;
;; ido
;;

;; (require 'ido)
;; (require 'ido-completing-read+)
;; (require 'ido-vertical-mode)

;; (ido-mode t) ;; everything
;; (ido-everywhere 1)
;; (ido-ubiquitous-mode 1)
;; (ido-vertical-mode 1)
;; (setq ido-vertical-define-keys 'C-n-and-C-p-only)

;; (setq ido-confirm-unique-completion t  ; wait for RET (trying this out for a while)
;;       ido-enable-flex-matching t       ; be flexible
;;       ido-enable-last-directory-history t ; remember last used dirs
;;       ido-everywhere t                 ; use for many file dialogs
;;       ido-ignore-buffers ;; ignore these guys
;;       '("\\` " "^\*Back" "^\*Ido" "^.*php-mode" "^.*nxhtml-mode")
;;       ido-ignore-directories
;;       '("\\`CVS/" "\\`\\.\\./" "\\`\\./" "\\`\\.svn/")
;;       ido-ignore-directories-merge '("")
;;       ido-max-prospects 11             ; don't spam my minibuffer
;;       ido-max-work-directory-list 30   ; should be enough
;;       ido-max-work-file-list      50   ; remember many
;;       ido-save-directory-list-file (concat cavd-tmp-dir "/ido.last")
;;       ido-use-filename-at-point 'guess    ; don't use filename at point
;;       ido-use-url-at-point nil         ;  don't use url at point
;;       ido-use-virtual-buffers t        ; remember buffers
;; ;      magit-completing-read-function 'magit-ido-completing-read
;;       )
;; (setq ido-vertical-show-count t)

;; (flx-ido-mode 1)
;; Enabling, and default is t
;; (setq flx-ido-use-faces t)

;; (use-package ido
;;   :init
;;   ;; From Magnar Sveen
;;   (defun cavd-ido-hook ()
;;     "Define stuff for ido."
;;     ;; Drop into magit-status
;;     (define-key ido-completion-map
;;       (kbd "C-x g") 'ido-enter-magit-status)
;;     ;; Allow "/" to complete a match
;;     ;; (define-key ido-completion-map
;;     ;;     (kbd "/")
;;     ;;   (lambda ()
;;     ;;     (interactive)
;;     ;;     (if (looking-back "/" (- (point) 1))
;;     ;;         (call-interactively 'self-insert-command)
;;     ;;         (ido-exit-minibuffer))))
;;     ;; Go straight home.
;;     (define-key ido-file-completion-map
;;       (kbd "~")
;;       (lambda ()
;;         (interactive)
;;         (if (looking-back "/" 1)
;;             (insert "~/")
;;           (call-interactively 'self-insert-command)))))
;;   :hook
;;   (ido-setup . cavd-ido-hook))

;; Trying this out, as well
(setq confirm-nonexistent-file-or-buffer nil)

;; (use-package async-completing-read
;;   :load-path
;;   "~/elisp/async-completing-read"
;;   :config
;;   (setq completing-read-function #'async-completing-read))

;; company-mode
(use-package company
  :ensure t
  :diminish company-mode eldoc-mode
  :commands (company-complete company-mode)
  :init
  (setq company-minimum-prefix-length 2)
  (setq company-idle-delay 0.5)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  (setq company-dabbrev-ignore-case nil)
  (setq company-show-numbers t)
  (setq company-require-match 'never)
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-code-other-buffers 'all)
  (setq company-frontends '(company-pseudo-tooltip-frontend
                            company-echo-metadata-frontend))
  (setq company-transformers
        '(company-sort-by-backend-importance
          company-sort-prefer-same-case-prefix
          company-sort-by-occurrence))
  :bind
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous))
  :hook
  (after-init . global-company-mode))

(use-package company-flx
  :ensure t
  :hook
  (company-mode . company-flx-mode))

(use-package company-dict
  :after (company yasnippet)
  :commands company-dict-refresh
  :config
  (setq company-dict-dir
        (concat user-emacs-directory "company-dict/"))
  (setq company-dict-enable-yasnippet t)
  (add-to-list 'company-backends 'company-dict)
  :bind
  ;("s-m" . company-dict)
  )

; company-dict-minor-mode-list

;(require 'company-prescient)
;(company-prescient-mode 1)
;(prescient-persist-mode 1)
;;
;; yasnippet
;;
(use-package yasnippet
  :ensure t
  :diminish
  :commands yas-expand-from-trigger-key
  :bind
  ("<C-tab>" . tab-indent-or-complete)
  :config
  (defun check-expansion ()
    (save-excursion
      (if (looking-at "\\_>") t
        (backward-char 1)
        (if (looking-at "\\.") t
          (backward-char 1)
          (if (looking-at "->") t nil)))))

  (defun do-yas-expand ()
    (let ((yas-expand 'return-nil))
      (yas-expand)))

  (defun tab-indent-or-complete ()
    (interactive)
    (if (minibufferp)
        (minibuffer-complete)
      (if (or (not yas-minor-mode)
              (null (do-yas-expand)))
          (if (check-expansion)
              (company-complete-common)
            (indent-for-tab-command)))))

;  (diminish 'yas-minor-mode)
  (setq yas-wrap-around-region t)
  (setq yas-triggers-in-field t)
  (add-to-list 'auto-mode-alist '("yas/.*" . snippet-mode))
  (advice-add 'company-complete-common :before (lambda () (setq my-company-point (point))))
  (advice-add
   'company-complete-common
   :after (lambda () (when (equal my-company-point (point)) (yas-expand))))
  :hook
  (after-init . yas-global-mode))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

(use-package auto-yasnippet
  :ensure t
  :after yasnippet
  :commands (aya-create aya-expand))


;; Dumb & Smart Jump
(use-package dumb-jump
  :ensure t
  :commands (dumb-jump-go dumb-jump-back)
  :config
  (setq dumb-jump-grep-cmd 'rg
        dumb-jump-force-searcher 'rg))

(use-package smart-jump
  :ensure t
  :commands (smart-jump-go smart-jump-back)
  :bind
  ("C-c h j" . hydra-smart-dumb-jump/body)
  :config
  (smart-jump-setup-default-registers))

;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
(setq lsp-keymap-prefix "C-c s-l")

;; From https://ianyepan.github.io/posts/emacs-ide/
;; (use-package lsp-mode
;;   :hook ((php-mode
;;           js-mode         ; ts-ls (tsserver wrapper)
;;           js-jsx-mode     ; ts-ls (tsserver wrapper)
;;           typescript-mode ; ts-ls (tsserver wrapper)
;;           web-mode        ; ts-ls/HTML/CSS
;;           ) . lsp-deferred)
;;   :commands lsp
;;   :config
;;   (setq lsp-auto-guess-root t)
;;   (setq lsp-log-io t)
;;   (setq lsp-restart 'auto-restart)
;;   ;(setq lsp-enable-symbol-highlighting nil)
;;   ;(setq lsp-enable-on-type-formatting nil)
;;   ;(setq lsp-signature-auto-activate nil)
;;   ;(setq lsp-signature-render-documentation nil)
;;   ;(setq lsp-eldoc-hook nil)
;;   (setq lsp-modeline-code-actions-enable t)
;;   (setq lsp-modeline-diagnostics-enable t)
;;   (setq lsp-headerline-breadcrumb-enable t)
;;   (setq lsp-semantic-tokens-enable t)
;;   ;(setq lsp-enable-folding nil)
;;   ;(setq lsp-enable-imenu nil)
;;   ;(setq lsp-enable-snippet nil)
;;   (setq read-process-output-max (* 1024 1024)) ;; 1MB
;;   (setq lsp-idle-delay 0.5)
;;   ;; My configs
;;   (setq lsp-file-watch-threshold 1300)
;;   (setq lsp-auto-configure t)
;;   ;; don't scan 3rd party javascript libraries
;;   (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$"
;;         lsp-file-watch-ignored-directories) ; json

;;   ;; don't ping LSP lanaguage server too frequently
;;   (defvar lsp-on-touch-time 0)
;;   (defadvice lsp-on-change (around lsp-on-change-hack activate)
;;     ;; don't run `lsp-on-change' too frequently
;;     (when (> (- (float-time (current-time))
;;                 lsp-on-touch-time) 30) ;; 30 seconds
;;       (setq lsp-on-touch-time (float-time (current-time)))
;;       ad-do-it))
;;   (push 'company-capf company-backends)
;;   :hook
;;   (lsp-mode . lsp-enable-which-key-integration))

;; (use-package lsp-mode
;;   :ensure t
;;   :after company
;;   :hook (php-mode js-mode)
;;   :commands (lsp lsp-deferred)
;;   :init
;;   (require 'lsp)
;;   (require 'company)
;;   :config
;;   (lsp-modeline-diagnostics-mode -1)
;;   (setq lsp-file-watch-threshold 1100)
;;   (setq lsp-auto-guess-root t)
;;   (setq lsp-modeline-diagnostics-enable nil)
;;   (setq lsp-enable-folding nil)
;;   (setq lsp-enable-symbol-highlighting nil)
;;   (setq lsp-enable-links nil)
;;   (setq lsp-restart 'auto-restart)
;;   (setq lsp-auto-configure t)
;;   ;; don't scan 3rd party javascript libraries
;;   (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$"
;;         lsp-file-watch-ignored-directories) ; json

;;   ;; don't ping LSP lanaguage server too frequently
;;   (defvar lsp-on-touch-time 0)
;;   (defadvice lsp-on-change (around lsp-on-change-hack activate)
;;     ;; don't run `lsp-on-change' too frequently
;;     (when (> (- (float-time (current-time))
;;                 lsp-on-touch-time) 30) ;; 30 seconds
;;       (setq lsp-on-touch-time (float-time (current-time)))
;;       ad-do-it))
;;   (push 'company-capf company-backends)
;;   :hook
;;   (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;;                                         ;   (php-mode . lsp)
;;    ;; if you want which-key integration
;;    (lsp-mode . lsp-enable-which-key-integration)))

;; ;; optionally
;; (use-package lsp-ui
;;   :commands lsp-ui-mode
;;   ;; :config
;;   ;; (setq lsp-ui-doc-enable t)
;;   ;; (setq lsp-ui-doc-header t)
;;   ;; (setq lsp-ui-doc-include-signature t)
;;   ;; (setq lsp-ui-doc-border (face-foreground 'default))
;;   ;; (setq lsp-ui-sideline-show-code-actions t)
;;   ;; (setq lsp-ui-sideline-delay 0.05)
;;   ;; (lsp-ui-mode)
;;   )

;; The original
(use-package lsp-mode
  :ensure t
  :after (company-mode)
  :commands (lsp lsp-deferred)
  :config
  (lsp-modeline-diagnostics-mode -1)
  (setq lsp-auto-guess-root t)
  (setq lsp-file-watch-threshold 1300)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-enable-folding nil)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-enable-links nil)
  (setq lsp-restart 'auto-restart)
  (setq lsp-auto-configure nil)
  ;; don't scan 3rd party javascript libraries
  (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$"
        lsp-file-watch-ignored-directories) ; json

  ;; don't ping LSP lanaguage server too frequently
  (defvar lsp-on-touch-time 0)
  (defadvice lsp-on-change (around lsp-on-change-hack activate)
    ;; don't run `lsp-on-change' too frequently
    (when (> (- (float-time (current-time))
                lsp-on-touch-time) 30) ;; 30 seconds
      (setq lsp-on-touch-time (float-time (current-time)))
      ad-do-it))
  (push 'company-capf company-backends)
  :hook
  (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
   ;; if you want which-key integration
   (lsp-mode . lsp-enable-which-key-integration)))

;; optionally
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable t)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t))

;; optionally
;; (use-package lsp-ui
;;   :commands lsp-ui-mode
;;   :bind (:map lsp-ui-mode-map
;;   ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
;;   ([remap xref-find-references] . lsp-ui-peek-find-references)))

;; (use-package company-lsp
;;   :after (company-mode lsp)
;;   :commands company-lsp
;;   :config
;;   (setq company-lsp-cache-candidates 'auto)
;;   (push 'company-lsp company-backends))

;; optionally if you want to use debugger
;; (use-package dap-mode :defer t)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; (use-package orderless
;;   :ensure t
;;   :after ivy
;;   :init
;;     ;;; NOTE: Not sure why I had that value before.  Keeping it around
;;     ;;; it case the one below proves problematic in some edge case…
;;   ;; (setq orderless-component-separator "[/\s_-]+")
;;   (setq orderless-component-separator " +")
;;   (setq completion-category-overrides '((file (styles . (partial-completion)))))
;;   (setq orderless-matching-styles
;;         '(orderless-flex
;;           orderless-strict-leading-initialism
;;           orderless-regexp
;;           orderless-prefixes
;;           orderless-literal))

;;   (defun prot/orderless-literal-dispatcher (pattern _index _total)
;;     (when (string-suffix-p "=" pattern)
;;       `(orderless-literal . ,(substring pattern 0 -1))))

;;   (defun prot/orderless-initialism-dispatcher (pattern _index _total)
;;     (when (string-suffix-p "," pattern)
;;       `(orderless-strict-leading-initialism . ,(substring pattern 0 -1))))

;;   (setq orderless-style-dispatchers
;;         '(prot/orderless-literal-dispatcher
;;           prot/orderless-initialism-dispatcher))
;;   ;; (setq completion-styles
;;   ;;       '(orderless partial-completion))

;;   :bind (:map minibuffer-local-completion-map
;;               ("SPC" . nil)))
                                        ; Space should never complete: use
                                        ; it for `orderless' groups.

(use-package minibuffer
  :init
  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)

;;; General minibuffer functions
  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           (completions (get-buffer-window "*Completions*")))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

;;; Completions' buffer actions
  ;; NOTE In practice I only use those while inspecting a long list
  ;; produced by C-h {f,o,v}.  To pop the Completions buffer, use
  ;; `minibuffer-completion-help', by default bound to ? from inside the
  ;; minibuffer.

  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

  (defmacro prot/completions-buffer-act (name doc &rest body)
    `(defun ,name ()
       ,doc
       (interactive)
       (let ((completions-window (get-buffer-window "*Completions*"))
             (completions-buffer (get-buffer "*Completions*"))
             (symbol (thing-at-point 'symbol)))
         (if (window-live-p completions-window)
             (with-current-buffer completions-buffer
               ,@body)
           (user-error "No live window with Completions")))))

  (prot/completions-buffer-act
   prot/completions-kill-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to the kill ring."
   (kill-new `,symbol)
   (message "Copied %s to kill-ring"
            (propertize `,symbol 'face 'success)))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to active window."
   (let ((window (window-buffer (get-mru-window))))
     (with-current-buffer window
       (insert `,symbol)
       (message "Inserted %s"
                (propertize `,symbol 'face 'success)))))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point-exit
   "Like `prot/completions-insert-symbol-at-point' plus exit."
   (prot/completions-insert-symbol-at-point)
   (top-level))

;;; Miscellaneous functions and key bindings

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  (setq completion-category-defaults nil)
  (setq completion-cycle-threshold 5)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-pcm-word-delimiters "-_./:| ")
  (setq completion-show-help nil)
  (setq completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)
  (setq completions-format 'vertical)   ; *Completions* buffer
  (setq read-answer-short t)
  (setq resize-mini-windows t)

  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           ;; This could be hardened a bit, but I am okay with it.
           (completions (or (get-buffer-window "*Completions*")
                            (get-buffer-window "*Embark Live Occur*"))))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  ;; This will be deprecated in favour of the `embark' package
  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

;;;; DEPRECATED in favour of the `embark' package (see further below),
;;;; which implements the same functionality in a more efficient way.
;;  (defun prot/complete-kill-or-insert-candidate (&optional arg)
;;     "Place the matching candidate to the top of the `kill-ring'.
;; This will keep the minibuffer session active.
;;
;; With \\[universal-argument] insert the candidate in the most
;; recently used buffer, while keeping focus on the minibuffer.
;;
;; With \\[universal-argument] \\[universal-argument] insert the
;; candidate and immediately exit all recursive editing levels and
;; active minibuffers.
;;
;; Bind this function in `icomplete-minibuffer-map'."
;;     (interactive "*P")
;;     (let ((candidate (car completion-all-sorted-completions)))
;;       (when (and (minibufferp)
;;                  (or (bound-and-true-p icomplete-mode)
;;                      (bound-and-true-p live-completions-mode))) ; see next section
;;         (cond ((eq arg nil)
;;                (kill-new candidate))
;;               ((= (prefix-numeric-value arg) 4)
;;                (with-minibuffer-selected-window (insert candidate)))
;;               ((= (prefix-numeric-value arg) 16)
;;                (with-minibuffer-selected-window (insert candidate))
;;                (top-level))))))

  ;; Defines, among others, aliases for common actions to Super-KEY.
  ;; Normally these should go in individual package declarations, but
  ;; their grouping here makes things easier to understand.
  :bind (
         ("s-l" . prot/describe-symbol-at-point)
         ("s-L" . (lambda ()
                    (interactive)
                    (prot/describe-symbol-at-point '(4))))
         ("s-e" . prot/focus-minibuffer-or-completions)
         :map minibuffer-local-completion-map
         ("<return>" . minibuffer-force-complete-and-exit) ; exit with completion
         ("C-j" . exit-minibuffer)      ; force input unconditionally
         :map completion-list-mode-map
         ("h" . prot/describe-symbol-at-point)
         ("w" . prot/completions-kill-symbol-at-point)
         ("i" . prot/completions-insert-symbol-at-point)
         ("j" . prot/completions-insert-symbol-at-point-exit)
         ("n" . next-line)
         ("p" . previous-line)
         ("f" . next-completion)
         ("b" . previous-completion)
         ("M-v" . prot/focus-minibuffer)))

(provide 'cavd-completions)

;;; cavd-completions.el ends here.
