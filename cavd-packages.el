;;
;; Packages
;;
(use-package package
  :config
  (defun cavd-install-packages ()
    "Ensure the packages I use are installed. See `package-selected-packages'."
    (interactive)
    (let ((missing-packages (cl-remove-if #'package-installed-p package-selected-packages)))
      (when missing-packages
        (message "Installing %d missing package(s)" (length missing-packages))
        (package-refresh-contents)
        (mapc #'package-install missing-packages))))

  (add-to-list 'package-archives
               '("marmalade" . "http://marmalade-repo.org/packages/") t)
  (add-to-list 'package-archives
               '("melpa-stable" . "http://stable.melpa.org/packages/") t)
  (add-to-list 'package-archives
               '("melpa" . "http://melpa.org/packages/") t)
  (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))

  (setq package-archive-priorities
        '(
          ("melpa-stable" . 10)
          ("gnu" . 7)
          ("org" . 7)
          ("marmalade" . 5)
          ("melpa" . 1)))

  (when (not package-archive-contents)
    (package-refresh-contents))

  (dolist (p package-selected-packages)
    (when (not (package-installed-p p))
      ;; (unless
      ;;     (member
      ;;      p
      ;;      '(darkane-theme inf-php anything kaolin-theme constant-theme github-review nimbus-theme company-eshell-autosuggest) )
      ;;   (package-install p))
      ))
  )

;; Load packages/files from my elisp directory

;; Commenting this out as it's shadowing the files in /usr/local/share
;; (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
;;     (let* ((my-lisp-dir elisp-dir)
;;            (default-directory my-lisp-dir))
;;       (add-to-list 'load-path my-lisp-dir)
;;       (normal-top-level-add-subdirs-to-load-path)))

;; (setq package-load-list '((elnode) all))

(provide 'cavd-packages)
