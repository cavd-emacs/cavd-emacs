;; Copied from http://sprunge.us/NGJM?cl

 ;;; A decade ago, the TRAMP devs were uncommunicative and unhelpful.  I choose
 ;;; to assume they remain so, and don't bother discussing these fixes with them.
(eval-after-load (if (locate-library "tramp-sh") "tramp-sh" "tramp")
  '(progn
    ;; sudo -Hs is suboptimal since--, when sudo -i was added.
    ;; Actually replacing -Hs with -i was WAY too hard, so replace ALL args and
    ;; hope the others are the still the same.
    (setcdr (assoc 'tramp-login-args (cdr (assoc "sudo" tramp-methods)))
     '((("-ipPassword:")) (("-u%u"))))

    ;; Tramp allows MITM by default, by disabling SSH host key checking.
    ;; Probably this was probably done because prompting to add new host keys
    ;; is Too Hard, in which case BatchMode is the right answer.  This code
    ;; assumes that tramp-gw-args isn't used for anything else.
    (mapc (lambda (x)
            (let ((x (assoc 'tramp-gw-args x)))
              (if x (setcdr x '(("-oBatchMode=yes"))))))
     tramp-methods)

    ;; tramp allocates a tty; it shouldn't.
    ;;
    ;;
    ;; This means that in a default environment, things like M-! git log RET
    ;; will hang because they run an interactive pager.  When I asked the tramp
    ;; people about this, they claimed it was acceptable to patch EVERY SINGLE
    ;; APP on the app server to change "is a tty?" to "is a tty and not tramp?"
    ;; THEY ARE WRONG.
    ;;
    ;; FIXME: they set TERM=dumb now... is that sufficient?
    ;; ANSWER: no. --twb, May
    (setq tramp-remote-process-environment
     (nconc tramp-remote-process-environment
      '("GIT_PAGER=cat" "DARCS_PAGER=cat"
        ;; My own wrappers work by testing for a tty, too!
        "PAGER=cat" "EDITOR=ed" "VISUAL=ed" "BROWSER=wm -dump")))

    ;; If your ssh_config uses ControlMaster auto, the "scpc" method breaks it --
    ;; but only for the outermost hop (cf. ProxyCommand).  So use the "scp" method
    ;; instead, which is identical save the ControlMaster arguments.
    ;;(setq tramp-default-method "scp")
    ;; UPDATE: as at., there is instead an explicit option to say "tramp,
    ;; leave ControlMaster the fuck alone", so that the defaults in .ssh/config
    ;; actuall y work.
    (setq tramp-use-ssh-controlmaster-options nil)))

(provide 'tramp-updates)
