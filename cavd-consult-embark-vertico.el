(defun cavd-consult-line-thing-at-point ()
    (interactive)
    (let ((thing (thing-at-point 'symbol)))
      (when (use-region-p)
        (deactivate-mark))
      (consult-line (regexp-quote thing))))

(defun cavd-switch-sql-buffer ()
  "Switch to a sql buffer."
  (interactive)
  (let ((bufs
         (cl-remove-if-not
          (lambda (buf)
            (with-current-buffer buf
              (equal 'sql-interactive-mode major-mode)))
          (internal-complete-buffer "" nil t))))
    (cond ((eq (car bufs) 0)
           (message "No SQL buffers."))
          ((eq (length bufs) 1)
           (pop-to-buffer (car bufs)))
          ((> (length bufs) 1)
           (let ((buf (completing-read "Choose buffer: " bufs)))
             (pop-to-buffer buf))))))

(use-package prescient
  :ensure t
  :config
  (setq prescient-filter-method '(literal initialism prefix regexp)
        prescient-use-char-folding t
        prescient-use-case-folding 'smart
        prescient-sort-full-matches-first t ; Works well with `initialism'.
        prescient-sort-length-enable t)
  (setq prescient-history-length 100)
  (setq prescient-save-file "~/.emacs.d/var/prescient-save.el")
  (setq prescient-aggressive-file-save t)
  ;; (setq prescient-filter-method '(initialism literal fuzzy))
  ;; The following works well when using opening a file from the top of the project
  (setq prescient-filter-method '(initialism literal regexp fuzzy))
  (prescient-persist-mode 1)
  (prescient--load))


(use-package vertico
  :ensure t
  :demand
  :config
  (setq vertico-cycle t)
  ;; currently requires melpa version of vertico
  (setq vertico-preselect 'directory)
  :init
  (vertico-mode)
  (defun my/vertico-insert ()
    (interactive)
    (let* ((mb (minibuffer-contents-no-properties))
           (lc (if (string= mb "") mb (substring mb -1))))
      (cond ((string-match-p "^[/~:]" lc) (self-insert-command 1 ?/))
            ((file-directory-p (vertico--candidate)) (vertico-insert))
            (t (self-insert-command 1 ?/)))))
  :bind (:map vertico-map
              ("/" . #'my/vertico-insert)
              ("RET"   . vertico-directory-enter)
              ("DEL"   . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  :config
  (require 'vertico-directory)
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package vertico-prescient
  :init
  (setq vertico-prescient-override-sorting t)
  (setq vertico-prescient-enable-sorting t)
  (setq vertico-prescient-enable-filtering t)
  (vertico-prescient-mode))

;; (use-package orderless
;;   :init
;;   ;; Configure a custom style dispatcher (see the Consult wiki)
;;   ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
;;   ;;       orderless-component-separator #'orderless-escapable-split-on-space)
;;   (setq completion-category-defaults nil)
;;   :custom
;;   (completion-styles '(orderless basic))
;;   (completion-category-overrides '((file (styles basic partial-completion))))
;;   ;; (orderless-matching-styles '(orderless-initialism orderless-literal orderless-regexp))
;;   (orderless-matching-styles '(orderless-regexp orderless-initialism orderless-literal))
;;   )

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

;; Example configuration for Consult
(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c s-p" . consult-projectile-switch-project)
         ("s-o" . find-file)
         ("M-s ." . isearch-forward-thing-at-point)
         ("s-f" . consult-line)
         ("s-." . consult-imenu)
         ("s-g" . cavd-consult-line-thing-at-point)
         ("s-z" . consult-projectile-find-file)
         ("C-c H" . consult-history)
         ("C-c m" . consult-mode-command)
         ;; ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x C-r" . consult-recent-file)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config
  (setq consult-buffer-sources
        '(consult--source-recent-file
         consult--source-buffer
         consult--source-hidden-buffer
         consult--source-modified-buffer
         consult--source-file-register
         consult--source-bookmark
         consult--source-project-buffer-hidden
         consult--source-project-recent-file-hidden))

  (defun mag/consult-project-or-dired-ripgrep ()
    "If in dired, call `consult-ripgrep' for the currently selected
files.  Otherwise call `consult-ripgrep` on the current project."
    (interactive)
    (if (eq major-mode 'dired-mode)
        (let ((consult-project-function (lambda (x) nil)))
    	  (consult-ripgrep (mapconcat #'shell-quote-argument (dired-get-marked-files) " ")))
      (consult-ripgrep)
      ))

  (defun mag/consult-project-or-dired-find ()
    "If in dired, call `consult-find' for the currently selected
files.  Otherwise call `consult-find` on the current project."
    (interactive)
    (if (eq major-mode 'dired-mode)
        (let ((consult-project-function (lambda (x) nil)))
    	  (consult-find (mapconcat #'shell-quote-argument (dired-get-marked-files) " ")))
      (consult-find)
      ))

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key (kbd "M-.")
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;; There are multiple reasonable alternatives to chose from.
      ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
      ;;;; 2. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
      ;;;; 3. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
      ;;;; 4. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  )

;; (use-package consult-git-log-grep
;;   :custom
;;   (consult-git-log-grep-open-function #'magit-show-commit))
(use-package consult-git-log-grep)

;;   (use-package consult-eglot
;;    :straight (consult-eglot :type git :host nil :repo "https://github.com/mohkale/consult-eglot")
;;    :config
;;    )

(use-package consult-projectile
  :after consult
  :ensure t
  :bind
  (("C-c s-p" . counsel-projectile-switch-project)
   ("C-c s-b" . consult-projectile-switch-to-buffer)
   ("C-c s-d" . consult-projectile-find-dir)))

(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (setq embark-quit-after-action nil)
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package corfu
  ;; Optional customizations
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  ;; (corfu-auto t)                 ;; Enable auto completion
  (corfu-separator ?\s)          ;; Orderless field separator
  (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  (corfu-preselect 'prompt)      ;; Preselect the prompt
  (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  :init
  (setq corfu-prescient-override-sorting t)
  :hook
  (after-init . global-corfu-mode))

(use-package corfu-prescient
  :ensure t
  :hook (corfu-mode . corfu-prescient-mode))

;; company-mode
(use-package company
  :ensure t
  :diminish company-mode eldoc-mode
  :commands (company-complete company-mode)
  :init
  (setq company-minimum-prefix-length 2)
  (setq company-idle-delay 0.5)
  (setq company-selection-wrap-around t)
  (setq company-tooltip-align-annotations t)
  (setq company-dabbrev-ignore-case nil)
  (setq company-show-numbers t)
  (setq company-require-match 'never)
  (setq company-dabbrev-downcase nil)
  (setq company-dabbrev-code-other-buffers 'all)
  (setq company-frontends '(company-pseudo-tooltip-frontend
                            company-echo-metadata-frontend))
  (setq company-transformers
        '(company-sort-by-backend-importance
          company-sort-prefer-same-case-prefix
          company-sort-by-occurrence))
  :config
  (push 'company-robe company-backends)
  :bind
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous))
  :hook
  (after-init . global-company-mode))

(use-package company-flx
  :ensure t
  :hook
  (company-mode . company-flx-mode))

(use-package company-dict
  :after (company yasnippet)
  :commands company-dict-refresh
  :config
  (setq company-dict-dir
        (concat user-emacs-directory "company-dict/"))
  (setq company-dict-enable-yasnippet t)
  (add-to-list 'company-backends 'company-dict)
  :bind
                                        ;("s-m" . company-dict)
  )

(use-package yasnippet
  :ensure t
  :diminish
  :commands yas-expand-from-trigger-key
  :bind
  ("<C-tab>" . tab-indent-or-complete)
  :config
  (defun check-expansion ()
    (save-excursion
      (if (looking-at "\\_>") t
        (backward-char 1)
        (if (looking-at "\\.") t
          (backward-char 1)
          (if (looking-at "->") t nil)))))

  (defun do-yas-expand ()
    (let ((yas-expand 'return-nil))
      (yas-expand)))

  (defun tab-indent-or-complete ()
    (interactive)
    (if (minibufferp)
        (minibuffer-complete)
      (if (or (not yas-minor-mode)
              (null (do-yas-expand)))
          (if (check-expansion)
              (company-complete-common)
            (indent-for-tab-command)))))

                                        ;  (diminish 'yas-minor-mode)
  (setq yas-wrap-around-region t)
  (setq yas-triggers-in-field t)
  (add-to-list 'auto-mode-alist '("yas/.*" . snippet-mode))
  (advice-add 'company-complete-common :before (lambda () (setq my-company-point (point))))
  (advice-add
   'company-complete-common
   :after (lambda () (when (equal my-company-point (point)) (yas-expand))))
  :hook
  (after-init . yas-global-mode))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

(use-package auto-yasnippet
  :ensure t
  :after yasnippet
  :commands (aya-create aya-expand))

;; Dumb & Smart Jump
(use-package dumb-jump
  :ensure t
  :commands (dumb-jump-go dumb-jump-back)
  :config
  (setq dumb-jump-selector 'popup
        dumb-jump-grep-cmd 'rg
        dumb-jump-force-searcher 'rg))

(use-package smart-jump
  :ensure t
  :commands (smart-jump-go smart-jump-back)
  :bind
  ("C-c h j" . hydra-smart-dumb-jump/body)
  :config
  (smart-jump-setup-default-registers))

;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
(setq lsp-keymap-prefix "C-c s-l")

(use-package lsp-mode
  :ensure t
  :after (php-mode ruby-mode js-mode company-mode)
  :commands (lsp lsp-deferred)
  :config
  (lsp-modeline-diagnostics-mode -1)
  (setq lsp-auto-guess-root t)
  (setq lsp-file-watch-threshold 1300)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-enable-folding nil)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-enable-links nil)
  (setq lsp-restart 'auto-restart)
  (setq lsp-auto-configure nil)
  ;; don't scan 3rd party javascript libraries
  (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$"
        lsp-file-watch-ignored-directories) ; json

  ;; don't ping LSP lanaguage server too frequently
  (defvar lsp-on-touch-time 0)
  (defadvice lsp-on-change (around lsp-on-change-hack activate)
    ;; don't run `lsp-on-change' too frequently
    (when (> (- (float-time (current-time))
                lsp-on-touch-time) 30) ;; 30 seconds
      (setq lsp-on-touch-time (float-time (current-time)))
      ad-do-it))
  (push 'company-capf company-backends)
  :hook
  (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
   ;; if you want which-key integration
   (lsp-mode . lsp-enable-which-key-integration)))

;; optionally
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable t)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t))

(use-package minibuffer
  :init
  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)

;;; General minibuffer functions
  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           (completions (get-buffer-window "*Completions*")))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

;;; Completions' buffer actions
  ;; NOTE In practice I only use those while inspecting a long list
  ;; produced by C-h {f,o,v}.  To pop the Completions buffer, use
  ;; `minibuffer-completion-help', by default bound to ? from inside the
  ;; minibuffer.

  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

  (defmacro prot/completions-buffer-act (name doc &rest body)
    `(defun ,name ()
       ,doc
       (interactive)
       (let ((completions-window (get-buffer-window "*Completions*"))
             (completions-buffer (get-buffer "*Completions*"))
             (symbol (thing-at-point 'symbol)))
         (if (window-live-p completions-window)
             (with-current-buffer completions-buffer
               ,@body)
           (user-error "No live window with Completions")))))

  (prot/completions-buffer-act
   prot/completions-kill-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to the kill ring."
   (kill-new `,symbol)
   (message "Copied %s to kill-ring"
            (propertize `,symbol 'face 'success)))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point
   "Add \"Completions\" buffer symbol-at-point to active window."
   (let ((window (window-buffer (get-mru-window))))
     (with-current-buffer window
       (insert `,symbol)
       (message "Inserted %s"
                (propertize `,symbol 'face 'success)))))

  (prot/completions-buffer-act
   prot/completions-insert-symbol-at-point-exit
   "Like `prot/completions-insert-symbol-at-point' plus exit."
   (prot/completions-insert-symbol-at-point)
   (top-level))

;;; Miscellaneous functions and key bindings

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  (setq completion-category-defaults nil)
  (setq completion-cycle-threshold 5)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-pcm-word-delimiters "-_./:| ")
  (setq completion-show-help nil)
  (setq completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)
  (setq completions-format 'vertical)   ; *Completions* buffer
  (setq read-answer-short t)
  (setq resize-mini-windows t)

  (defun prot/focus-minibuffer ()
    "Focus the active minibuffer.

Bind this to `completion-list-mode-map' to M-v to easily jump
between the list of candidates present in the \\*Completions\\*
buffer and the minibuffer (because by default M-v switches to the
completions if invoked from inside the minibuffer."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (when mini
        (select-window mini))))

  (defun prot/focus-minibuffer-or-completions ()
    "Focus the active minibuffer or the \\*Completions\\*.

If both the minibuffer and the Completions are present, this
command will first move per invocation to the former, then the
latter, and then continue to switch between the two.

The continuous switch is essentially the same as running
`prot/focus-minibuffer' and `switch-to-completions' in
succession."
    (interactive)
    (let* ((mini (active-minibuffer-window))
           ;; This could be hardened a bit, but I am okay with it.
           (completions (or (get-buffer-window "*Completions*")
                            (get-buffer-window "*Embark Live Occur*"))))
      (cond ((and mini
                  (not (minibufferp)))
             (select-window mini nil))
            ((and completions
                  (not (eq (selected-window)
                           completions)))
             (select-window completions nil)))))

  ;; Technically, this is not specific to the minibuffer, but I define
  ;; it here so that you can see how it is also used from inside the
  ;; "Completions" buffer
  (defun prot/describe-symbol-at-point (&optional arg)
    "Get help (documentation) for the symbol at point.

With a prefix argument, switch to the *Help* window.  If that is
already focused, switch to the most recently used window
instead."
    (interactive "P")
    (let ((symbol (symbol-at-point)))
      (when symbol
        (describe-symbol symbol)))
    (when arg
      (let ((help (get-buffer-window "*Help*")))
        (when help
          (if (not (eq (selected-window) help))
              (select-window help)
            (select-window (get-mru-window)))))))

  ;; This will be deprecated in favour of the `embark' package
  (defun prot/completions-kill-save-symbol ()
    "Add symbol-at-point to the kill ring.

Intended for use in the \\*Completions\\* buffer.  Bind this to a
key in `completion-list-mode-map'."
    (interactive)
    (kill-new (thing-at-point 'symbol)))

;;;; DEPRECATED in favour of the `embark' package (see further below),
;;;; which implements the same functionality in a more efficient way.
  ;;  (defun prot/complete-kill-or-insert-candidate (&optional arg)
  ;;     "Place the matching candidate to the top of the `kill-ring'.
  ;; This will keep the minibuffer session active.
  ;;
  ;; With \\[universal-argument] insert the candidate in the most
  ;; recently used buffer, while keeping focus on the minibuffer.
  ;;
  ;; With \\[universal-argument] \\[universal-argument] insert the
  ;; candidate and immediately exit all recursive editing levels and
  ;; active minibuffers.
  ;;
  ;; Bind this function in `icomplete-minibuffer-map'."
  ;;     (interactive "*P")
  ;;     (let ((candidate (car completion-all-sorted-completions)))
  ;;       (when (and (minibufferp)
  ;;                  (or (bound-and-true-p icomplete-mode)
  ;;                      (bound-and-true-p live-completions-mode))) ; see next section
  ;;         (cond ((eq arg nil)
  ;;                (kill-new candidate))
  ;;               ((= (prefix-numeric-value arg) 4)
  ;;                (with-minibuffer-selected-window (insert candidate)))
  ;;               ((= (prefix-numeric-value arg) 16)
  ;;                (with-minibuffer-selected-window (insert candidate))
  ;;                (top-level))))))

  ;; Defines, among others, aliases for common actions to Super-KEY.
  ;; Normally these should go in individual package declarations, but
  ;; their grouping here makes things easier to understand.
  :bind (
         ("s-l" . prot/describe-symbol-at-point)
         ("s-L" . (lambda ()
                    (interactive)
                    (prot/describe-symbol-at-point '(4))))
         ("s-e" . prot/focus-minibuffer-or-completions)
         :map minibuffer-local-completion-map
         ("<return>" . minibuffer-force-complete-and-exit) ; exit with completion
         ("C-j" . exit-minibuffer)      ; force input unconditionally
         :map completion-list-mode-map
         ("h" . prot/describe-symbol-at-point)
         ("w" . prot/completions-kill-symbol-at-point)
         ("i" . prot/completions-insert-symbol-at-point)
         ("j" . prot/completions-insert-symbol-at-point-exit)
         ("n" . next-line)
         ("p" . previous-line)
         ("f" . next-completion)
         ("b" . previous-completion)
         ("M-v" . prot/focus-minibuffer)))

(provide 'cavd-consult-embark-vertico)
