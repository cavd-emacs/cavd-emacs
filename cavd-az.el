;;; 35619380045136915353
;;;

(defun get-and-parse-json (url)
 (interactive)
 (let* ((url-request-extra-headers *az-api-key*)
        (buffer (url-retrieve-synchronously url))) ; Uupdate this to be silent for >= 24.4
   (with-current-buffer buffer
     (goto-char (point-min))
     (re-search-forward "^$" nil 'move)
     (json-read))))

(defun get-my-projects ()
  (interactive)
  (let* ((projects (az-call-api "projects"))
         (total-items (cdr (assoc 'totalItems (cdr projects)))))
    (cl-dotimes (i total-items "Done")
      (princ (cdr (assoc 'name (elt (cdr (car projects)) i)))))))


;;;
;;; Begin
;;;
(require 'json)
(require 'url-http)

(defvar az-projects nil)

(defvar az-me nil)

(defvar az-colors '("grey" "blue" "red" "green" "orange" "yellow" "purple" "teal"))

(defvar az-fibonacci-sizes '("1" "2" "3" "5" "8" "13"))

(defun normalize-name (str)
  (replace-regexp-in-string
   "\-\-"
   "-"
   (downcase
    (replace-regexp-in-string
     "[^a-zA-Z0-9]"
     (lambda (x) (if (string= x " ") "-" ""))
     str))))

(defun az-format-date (date)
  "Format the date for internal date functions."
  (replace-regexp-in-string "T" " " date))

(defun az-long-day-date (date-string)
  "Format a date to Friday, January 4, 2013 at 19:34"
  (format-time-string "%A, %B%e, %Y at %k:%M"
                      (date-to-time (az-format-date date-string))))


(cl-defun az-call-api (endpoint &key content (method "GET"))
    (interactive)
    (let* ((url (concat "https://agilezen.com/api/v1/" endpoint))
           (url-request-method (if method method "GET"))
           (url-request-extra-headers *az-api-key*)
           (url-request-data content)
           (buffer (url-retrieve-synchronously url)))
      (with-current-buffer buffer
        (let ((response (url-http-parse-response)))
          (when (and (>= response 200) (<= response 300))
            (progn (goto-char (point-min))
                   (re-search-forward "^$" nil 'move)
                   (json-read)))))))


(defun az-load-projects ()
  "Load projects and their phases into a data structure."
  (interactive)
  (let* ((projects (get-my-projects))
         (project-count (get-total-items projects)))
    (cl-dotimes (i project-count "Done")
      (let* ((name (cdr (assoc 'name (elt (cdr (car projects)) i))))
             (id (cdr (assoc 'id (elt (cdr (car projects)) i))))
             (project-phases (get-phases id))
             (phase-count (if project-phases
                              (get-total-items project-phases)
                              0))
             (project nil))
        (push (cons "id" id) project)
        (let ((phases nil))
          (cl-dotimes (p phase-count nil)
            (add-to-list 'phases
                         (cons
                          (cdr (assoc 'name (elt (cdr (car project-phases)) p)))
                          (cdr (assoc 'id (elt (cdr (car project-phases)) p))))
                  ))
          (push (cons "phases" phases) project))
        (add-to-list 'az-projects `(,name . ,project))))))

(defun get-me ()
  (interactive)
  (setq az-me (az-call-api "me"))
  (setq az-my-id (cdr (assoc 'id az-me))))

(defun az-choose-project ()
  "Prompt the user for a project."
  (interactive)
  (let ((project
         (completing-read
          "Project: "
          (cl-mapcar (lambda (p) (car p))
                     (cdr az-projects)))))
    (if project
        (assoc project az-projects)
        nil)))

(defun az-choose-phase (project)
  "Given a project, prompt the user for a phase."
  (interactive)
  (let* ((phases (cdr (assoc "phases" project)))
        (phase
         (completing-read
          "Phase: "
          (cl-mapcar (lambda (p) (car p))
                     phases))))
    (if phase
        (assoc phase phases)
        nil)))

(defun az-choose-story ()
  (interactive)
  (let ((story-id (read-from-minibuffer "Story Number: ")))
    (if (string= story-id "")
        nil
        story-id)))

(defun az-choose-color ()
  "Prompt the user for a color."
  (interactive)
  (completing-read
   "Color: "
   (cl-mapcar (lambda (p) p)
              az-colors)))

(defun get-total-items (thing)
  (or (cdr (assoc 'totalItems (cdr thing)))
      (length thing)))

(defun get-project-id (project)
  "Pick out the project id from the project 'object'."
  (cdr (assoc "id" project)))

(defun get-project-name (project)
  "Pick out the project name from the project 'object'."
  (car project))

(defun get-phase-id (phase)
  "Pick out the phase id from the phase 'object'."
  (cdr phase))

(defun get-project-complete-id (project)
  "Pick out the id for the 'Complete' phase from a project.
Unfortunately, assumes 'Complete' as the name of a phase."
  (get-phase-id (assoc "Complete" (cdr (assoc "phases" project)))))

(defun get-project-working-id (project)
  "Pick out the id for the 'Working' phase from a project.
Unfortunately, assumes 'Working' as the name of a phase."
  (get-phase-id (assoc "Working" (cdr (assoc "phases" project)))))

(defun get-phase-name (phase)
  "Pick out the phase name from the phase 'object'."
  (car phase))

(defun get-story-owner (story)
  "Pick out the name of the story's creator."
  (cdr (assoc 'name (assoc 'owner story))))

(defun get-story-creator (story)
  "Pick out the name of the story's creator."
  (cdr (assoc 'name (assoc 'creator story))))

(defun get-story-id (story)
  "Retrieve the id of a story from the story object."
  (cdr (assoc 'id (cdr story))))

(defun get-story-text (story)
  "Retrieve the text of a story object."
  (let* ((text0 (cdr (assoc 'text (cdr story))))
            (text1 (replace-regexp-in-string "[â¢]" "" text0))
            (text2 (replace-regexp-in-string "" "'" text1))
            (text3 (replace-regexp-in-string "[]" "\"" text2)))
    text3))

(defun get-story-size (story)
  "Retrieve the size from a story."
  (cdr (assoc 'size story)))

(defun get-story-details (story)
  "Retrieve the details from a story."
  (cdr (assoc 'details story)))

(defun get-stories-in-phase (project-id phase-id)
  "For a project, get the stories in a phase."
  (az-call-api (concat
                "projects/"
                (int-to-string project-id)
                "/phases/"
                (int-to-string phase-id)
                "/stories")))

(defun show-stories-in-phase ()
  "Show the stories in a phase, prompting for both."
  (interactive)
  (let* ((project (az-choose-project))
         (phase (az-choose-phase project))
         (stories (get-stories-in-phase (get-project-id project)
                                        (get-phase-id phase)))
         (buf (get-buffer-create
               (concat "*" (get-project-name project) " - " (get-phase-name phase) "*"))))
    (with-current-buffer buf
      (erase-buffer)
      (cl-dotimes (i (get-total-items stories))
        (let ((story-id (get-story-id (elt (cdr (car stories)) i)))
              (story-text (get-story-text (elt (cdr (car stories)) i))))
          (insert (int-to-string story-id))
          (insert (concat " (owned by "
                          (or
                           (get-story-owner (elt (cdr (car stories)) i))
                           "No One")
                          ") "))
          (insert ":  ")
          (insert story-text)
          (insert "\n********************************************************************************
\n")))
      (goto-char (point-min))
      (setq fill-column 40)
      (auto-fill-mode 1))
    (switch-to-buffer buf)))

(defun get-story (project-id story-id)
  "Retrieve a story and its comments for a project."
  (let ((story-id (if (integerp story-id) (int-to-string story-id) story-id)))
    (az-call-api (concat "projects/"
                         (int-to-string project-id)
                         "/stories/"
                         story-id
                         "?with=comments,details"))))

(defun show-story ()
  "Show a story, along with comments and details."
  (interactive)
  (let ((project (az-choose-project))
        (story-id (az-choose-story)))
    (if story-id
        (let* ((story (get-story (get-project-id project) story-id))
               (comments (cdr (assoc 'comments story)))
               (comments-length (length comments))
               (buf
                (get-buffer-create (concat "* AZ " story-id " *"))))
          (with-current-buffer buf
            (erase-buffer)
            (insert (cdr (assoc 'text story)))
            (insert "\n*** Details ***\n")
            (insert (cdr (assoc 'details story)))
            (if (> comments-length 0)
                (progn
                  (insert "\n\n*** Comments ***\n")
                  (cl-dotimes (i comments-length "Done")
                    (insert (concat
                             "\nOn "
                             (az-long-day-date (cdr (assoc 'createTime (elt comments i))))
                             " "
                             (cdr (assoc 'name (cdr (assoc 'author (elt comments i)))))
                             " said:\n"))
                    (insert (cdr (assoc 'text (elt comments i))))
                    (insert "\n\n"))))
            (goto-char (point-max)))
          (switch-to-buffer buf)))))

(defun complete-story ()
  "Mark a story as being complete."
  (interactive)
  (let* ((project (az-choose-project))
         (story-id (az-choose-story))
         (complete-id (get-project-complete-id project))
         (properties
          (json-encode `(,(cons "phase" complete-id)))))
    (if (az-call-api (concat "projects/"
                             (int-to-string (get-project-id project))
                             "/stories/"
                             story-id)
                     :content properties
                     :method "PUT")
        (message (concat "Story " story-id " is complete."))
        (message "Failed to complete story."))))

(defun work-on-story ()
  "Start working on a story."
  (interactive)
  (let* ((project (az-choose-project))
         (story-id (az-choose-story))
         (working-id (get-project-working-id project))
         (properties
          (json-encode `(,(cons "phase" working-id)))))
    (if (az-call-api (concat "projects/"
                             (int-to-string (get-project-id project))
                             "/stories/"
                             story-id)
                     :content properties
                     :method "PUT")
        (message (concat "Now working on story " story-id))
        (message "Failed to work on story."))))

(defun own-story ()
  "Take ownership of a story."
  (interactive)
  (let* ((project (az-choose-project))
         (story-id (az-choose-story))
         (properties (json-encode `(,(cons "owner" az-my-id)))))
    (if (az-call-api (concat "projects/"
                             (int-to-string (get-project-id project))
                             "/stories/"
                             story-id)
                     :content properties
                     :method "PUT")
        (message (concat "You now own story " story-id))
        (message "Failed to take ownership of " story-id))))

(defun update-story ()
  "Update the text, details, size, color, or phase of a story."
  (interactive)
  (let* ((project (az-choose-project))
         (story-id (az-choose-story))
         (story (get-story (get-project-id project) story-id))
         (text (edit-story-property "Text" (get-story-text story)))
         (details (edit-story-property "Detail" (get-story-details story)))
         (size (edit-story-property "Size" (get-story-size story)))
         (color (az-choose-color))
         (phase (get-phase-id (az-choose-phase project)))
         (properties (json-encode
                      `(,(cons "text" text)
                         ,(cons "details" details)
                         ,(cons "size" size)
                         ,(cons "color" color)
                         ,(cons "phase" phase)))))
    (if (az-call-api (concat "projects/"
                             (int-to-string (get-project-id project))
                             "/stories/"
                             story-id)
                     :content properties
                     :method "PUT")
        (message (concat "Story " story-id " updated."))
        (message "Failed to update story."))))

(defun edit-story-property (prompt prop)
  "Edit the prop of a story."
  (read-from-minibuffer (concat prompt ": ") prop))

(defun get-my-projects ()
  (az-call-api "projects"))

(defun az-show-my-projects ()
  "Retrieve the projects for the authenticated user."
  (interactive)
  (let* ((all-projects (get-my-projects))
         (total-items (get-total-items all-projects))
         (projects nil))
    (cl-dotimes (i total-items "Done")
      (let ((name (cdr (assoc 'name (elt (cdr (car all-projects)) i))))
            (id (cdr (assoc 'id (elt (cdr (car all-projects)) i)))))
        (push (cons name id) projects)
        (add-to-list 'az-projects `(,(normalize-name name) . ,id))))
    (let ((buf (get-buffer-create "*Agile Zen Projects*")))
      (with-current-buffer buf
        (erase-buffer)
        (dolist (elt projects)
          (let ((beg (point)))
            (insert (car elt))
            (insert " - ")
            (insert (int-to-string (cdr elt)))
            (insert ?\n)))
        (goto-char (point-max)))
      (switch-to-buffer buf))))

(defun get-phases (project-id)
  "Retrieve the phases for a project."
  (az-call-api
   (concat "projects/"
           (int-to-string project-id)
           "/phases")))
