(require 'url-http)

(defconst tp-divider
  "--------------------------------------------------------------------------------"
  "Divider between ticket parts.")

(defvar *tp-credentials* nil)

(defvar *tp-header* nil)

(defvar *tp-me* nil)

(defvar *tp-bug-states* nil
  "The states that a bug can be in.")

(defvar *tp-userstory-states* nil
  "The states that a user story can be in.")

(defvar tp-base-url "https://kalkomey.tpondemand.com/api/v1/"
  "The main URL for Target Process.")

(defun tp-format-date-xml (date)
  "Format the date for internal date functions."
  (replace-regexp-in-string "T" " " date))

(defun tp-long-day-date-xml (date-string)
  "Format a date to Friday, January 4, 2013 at 19:34"
  (format-time-string
   "%A, %B %e, %Y at %k:%M"
   (date-to-time (tp-format-date date-string))))

(defun tp-create-credentials ()
  (interactive)
  (cl-letf ((username (read-from-minibuffer "Username: "))
            (password (read-passwd "Password: ")))
    (setq *tp-header*
          `(("Content-Type" . "application/json")
            ("Authorization"
             .
             ,(concat
               "Basic "
               (base64-encode-string
                (concat username ":" password)))))))
  (message "Done"))
;; https://kalkomey.tpondemand.com/api/views/v1/view/5268788756811697339
;; https://kalkomey.tpondemand.com/api/v2/Context/4BF6F07A5FA46A64A182F099D70760AF
;; https://kalkomey.tpondemand.com/api/views/v1/view/5268788756811697339


(cl-defun tp-call-api-view ()
  (let* ((url "https://kalkomey.tpondemand.com/slice/v1/TreeView/treeView")
         (url-request-method "POST")
         (url-request-extra-headers *tp-header*)
         (url-request-data tp-backlog-ranked-body)
         (url-mime-accept-string "application/json")
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (if (and (>= response 200) (<= response 300))
          (progn (goto-char (point-min))
                   (re-search-forward "^$" nil 'move)
                   (json-read))
          (switch-to-buffer buffer))))))

;; https://kalkomey.tpondemand.com/api/v2/testPlan?select={testHierarchyCount}&where=(id==9326)

(cl-defun tp-call-api-image (endpoint &key content (method "GET"))
  (let* ((url endpoint)
         (url-request-method (if method method "GET"))
         (url-request-extra-headers *tp-header*)
         (url-request-data content)
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (when (and (>= response 200) (<= response 300))
          (progn (goto-char (point-min))
                   (re-search-forward "^$" nil 'move)
                   (create-image (string-as-unibyte
                                  (buffer-substring (point) (point-max)))
                                 'png
                                 t)))))))

(defun tp-show-image (endpoint)
  (interactive)
  (let* ((image (tp-call-api-image endpoint))
         (image-buffer (get-buffer-create (concat "*image buffer*"))))
    (with-current-buffer image-buffer
      (erase-buffer)
      (insert-image image)
      (switch-to-buffer image-buffer))))

(cl-defun tp-call-api-json (endpoint &key content (method "GET"))
  (let* ((url (concat tp-base-url endpoint))
         (url-request-method (if method method "GET"))
         (url-request-extra-headers *tp-header*)
         (url-request-data content)
         (url-mime-accept-string "application/json")
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (when (and (>= response 200) (<= response 300))
          (progn (goto-char (point-min))
                 (re-search-forward "^$" nil 'move)
                 (json-read)))))))


;; (tp-call-api "TestCases/16818/TestSteps?resultFormat=json" :content (json-encode '(("Description" . "Step") ("Result" . "Expectation"))) :method "POST")

(cl-defun tp-call-api (endpoint &key content (method "GET"))
  (let* ((url (concat tp-base-url endpoint))
         (url-request-method (if method method "GET"))
         (tp-header *tp-header*)
         (url-request-extra-headers tp-header)
         (url-request-data content)
         (url-mime-accept-string "application/json")
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (message "%S" response)
        (when (and (>= response 200) (<= response 300))
          (progn (goto-char (point-min))
                 (re-search-forward "^$" nil 'move)
                 (json-read)))))))

(cl-defun tp-test-call-api (endpoint &key content (method "GET"))
  (let* ((url endpoint)
         (url-request-method (if method method "GET"))
         (url-request-extra-headers *tp-header*)
         (url-request-data content)
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (if (and (>= response 200) (<= response 300))
          (switch-to-buffer buffer))))))

(defun tp-get-context ()
  (tp-call-api "Context/"))

(defun tp-show-context ()
  (interactive)
  (let ((buf (tp-get-context)))
    (if buf
        (with-current-buffer buf))))


;; ?Location is not 'Done' and Location is not 'Inbox' and Location is not 'Fridge' and Location is not 'Freezer' and EntityState is not 'Resolved' and EntityState is not 'Closed' and Assignments.Count == 0

;; (defun tp-backlog-ranked-filter-json (exp)
;;   "Filter tickets that are assigned or are not in the Back Log."
;;   (remove-if-not
;;    (lambda (el)
;;      (and
;;       (equal 0 (length (cdr (assoc 'Items (assoc 'Assignments el)))))
;;       (cl-member
;;        (cdr (assoc 'Value (aref (cdr (assoc 'CustomFields el)) 2)))
;;        '("Back Log") :test 'equal)))
;;    (cdr (assoc 'Items exp))))

;; original filter
;; (remove-if-not
;;    (lambda (el)
;;      (and
;;       ;; (equal 0 (length (cdr (assoc 'Items (assoc 'Assignments el)))))
;;       ;; (cl-member
;;       ;;  (cdr (assoc 'Value (aref (cdr (assoc 'CustomFields el)) 2)))
;;       ;;  '("Back Log") :test 'equal)
;;       ))
;;    (xml-get-children (first tickets) 'Assignable))



;; filter
;; (remove-if (lambda (el) (and (equal 0 (length (cdr (assoc 'Items (assoc 'Assignments el))))) (cl-member (cdr (assoc 'Value (aref (cdr (assoc 'CustomFields el)) 2))) '("Freezer" "Inbox" "Fridge" "Done") :test 'equal))) (cdr (assoc 'Items tp-assignables-resp)))

(defun tp-decode-string (s)
  "Decode the provided string using utf-8."
  (decode-coding-string s 'utf-8))

;; /api/v1/UserStories?include=[Name]&append=[Tasks-Count]&format=json&skip=25&where=EntityState.Name%20eq%20'Done'

(defun tp-get-em-states ()
  "Get the states for Bugs and UserStories."
  (tp-call-api "EntityStates?where=(Process.name eq 'EM-RE') and (EntityType.name in ('UserStory', 'Bug'))&take=200"))

(defun tp-load-em-states ()
  (let ((em-states (tp-get-attribute (tp-get-em-states) 'Items)))
    (mapcar
     (lambda (el)
       (concat
        (tp-get-attribute (tp-get-attribute el 'EntityType) 'Name)
        " - "
        (tp-get-attribute el 'Name)))
     em-states)))

(defun tp-get-projects ()
  "Get all TP projects."
  (tp-call-api "Projects"))

(defun tp-get-me ()
  (interactive)
  (let ((me (tp-call-api "Users?where=(Email eq 'cvandusen@kalkomey.com')")))
    (setq *tp-me* (aref (cdr (assoc 'Items me)) 0))))

(defun tp-get-assigned-user (id)
  (let* ((id (int-to-string id))
         (endpoint (concat "UserStories/" id "?include=[AssignedUser]"))
         (user (tp-call-api endpoint)))
    ))

;; Time
(defun tp-create-time (id assigned project-id desc spent json-date)
  (let* ((payload (json-encode
                   `(("Assignable" ("Id" . ,id))
                     ("Project" ("Id" . ,project-id))
                     ("User" ("Id" . ,assigned))
                     ("Description" . ,desc)
                     ("Spent" . ,spent)
                     ("Date" . ,json-date))))
        (endpoint "Times?resultFormat=json"))
    (message "%S" payload)
    (tp-call-api endpoint :content payload :method "POST")))

(defun tp-add-time (id)
  (interactive "nTicket Number: ")
  (let* ((ticket (tp-get-story (int-to-string id)))
         (assigned (tp-get-attribute *tp-me* 'Id))
         (project-id (tp-get-attribute (tp-get-attribute ticket 'Project) 'Id))
         (desc (read-from-minibuffer "Description: "))
         (spent (read-from-minibuffer "How many hours: "))
         (date (tp-make-json-date
                (read-from-minibuffer "Date (YYYY-MM-DD) "))))
    (tp-create-time id assigned project-id desc spent date ))
  (message "Added"))

(defun tp-make-json-date (date)
  "Turn a date in format YYYY-MM-DD HH:MM:SS
into a json date."
  (let ((date (round
               (* 1000
                  (time-to-seconds
                   (date-to-time
                    (concat date " 00:00:00")))))))
    (concat "/Date(" (int-to-string date) "-0600)/")))

;; Comment
(defun tp-create-comment (id desc)
  (let* ((payload (json-encode
                   `(("General" ("Id" . ,id))
                     ("Description" . ,desc))))
        (endpoint "Comments?resultFormat=json"))
    (tp-call-api endpoint :content payload :method "POST")))

(defun tp-add-comment (id)
  (interactive "nTicket Number: ")
  (let* ((ticket (tp-get-story (int-to-string id)))
         (desc (read-from-minibuffer "Description: ")))
    (tp-create-comment id desc )))

;;  Test Steps
(defun tp-clone-teststeps (from-id to-id)
  (let ((steps (tp-get-teststeps
                (int-to-string from-id))))
    (mapcar
     (lambda (step)
       (tp-create-teststep
        to-id
        (tp-get-attribute step 'Description)
        (tp-get-attribute step 'Result)
        (tp-get-attribute step 'RunOrder)))
     steps)
    (message "Done")))

(defun tp-add-teststep ()
  (interactive)
  (let* ((id (read-from-minibuffer "Test Case ID: "))
         (step (read-from-minibuffer "Step: "))
         (expected (read-from-minibuffer "Expected Result: "))
         (run-order (read-from-minibuffer "Run Order: ")))
    (if (tp-create-teststep id step expected run-order)
        (message "Created")
        (message "Step not created"))))

(defun tp-create-teststep (id step expected run-order)
  (let ((payload (json-encode
                  `(("TestCase" ("Id" . ,id))
                    ("Description" . ,step)
                    ("Result" . ,expected)
                    ("RunOrder" . ,run-order))))
        (endpoint "TestSteps?resultFormat=json"))
    (tp-call-api endpoint :content payload :method "POST")))

(defun tp-get-teststeps (id)
  "Retrieve the Test steps for a test case."
  (let* ((tp-include "include=[Description, Result, RunOrder]")
         (qs (concat "TestCases/" id "/TestSteps/?take=100&" tp-include))
         (tp-steps (tp-call-api qs)))
    (if tp-steps
        (cdr (assoc 'Items tp-steps)))))

(defun tp-display-teststeps (test-id)
  "Display the steps for a test case."
  (interactive "nTest Case Number: ")
  (let* ((id (int-to-string test-id))
         (steps (tp-get-teststeps id))
         (tp-buffer
          (get-buffer-create
           (concat "* Test Casse for " id " *"))))
    (if steps
        (with-current-buffer tp-buffer
          (erase-buffer)
          (mapcar
           (lambda (tp-step)
             (insert (tp-build-step tp-step)))
           steps)
          (shr-render-region (point-min) (point-max))
          (goto-char (point-max))
          (switch-to-buffer tp-buffer)))))

(defun tp-build-step (tp-step)
  (concat
   (cdr (assoc 'Description tp-step))
   " "
   (cdr (assoc 'Result tp-step))
   "<br>"))

;; Test Case
;; TODO handle multiple pages of steps e.g.
;; https://kalkomey.tpondemand.com/api/v1/TestCases/16818/TestSteps/?skip=25&take=25&include=[Id,Description,Result]
(defun tp-get-testcase (id)
  "Retrieve a Test Case and all associated resources."
  (let* ((tp-include "include=[Comments, TestSteps[Description, Result], UserStories]")
         (qs (concat "TestCases/" id "?take=1000&" tp-include)))
    (tp-call-api qs)))

(defun tp-display-testcase (test-id)
  "Display the steps for a test case."
  (interactive "nTest Case Number: ")
  (let* ((id (int-to-string test-id))
         (test-case (tp-get-testcase id))
         (steps (tp-get-steps test-case))
         (tp-buffer
          (get-buffer-create
           (concat "* Test Casse for " id " *"))))
    (if steps
        (with-current-buffer tp-buffer
          (erase-buffer)
          (mapcar
           (lambda (tp-step)
             (insert (tp-build-step tp-step)))
           steps)
          (shr-render-region (point-min) (point-max))
          (goto-char (point-max))
          (switch-to-buffer tp-buffer)))))

(defun tp-get-steps (test-case)
  (cdr
   (assoc
    'Items
    (cdr
     (assoc 'TestSteps test-case)))))

;; Test Plan
(defun tp-get-testplan (id)
  "Get a TP test plan by ID."
  (tp-call-api (concat "TestPlans/" id)))

(defun tp-display-ticket (ticket-id)
  "Display all aspects of a story."
  (interactive "nTP Number: ")
  (let* ((id (int-to-string ticket-id))
         (story (tp-get-story id))
         (comments (tp-get-comments story))
         (tp-buffer
          (get-buffer-create
           (concat "* TP " id " *"))))
    (if story
        (with-current-buffer tp-buffer
          (erase-buffer)
          (insert (tp-build-ticket-header story))
          (insert ?\n)
          (insert (tp-get-description story))
          (insert "<br><br>")
          (mapcar
           (lambda (el)
             (insert (tp-build-comment el)))
           comments)
          (shr-render-region (point-min) (point-max))
          (goto-char (point-max))
          (switch-to-buffer tp-buffer)))))

(defun tp-get-story-full (id)
  "Get a TP story by ID."
  (or (tp-call-api
       (concat "Bugs/" id "?include=[CustomFields, EntityState, Priority, Owner, EntityType, LinkedTestPlan]"))
      (tp-call-api
       (concat "UserStories/" id "?include=[CustomFields, EntityState, Priority, Owner, EntityType, UserStoryTestCases, UserStoryTestPlans]"))))

(defun tp-get-story (id)
  "Get a TP story by ID."
  (or (tp-call-api
       (concat "Bugs/" id))
      (tp-call-api
       (concat "UserStories/" id))))

(defun tp-get-comments (ticket)
  "Get comments for a ticket."
  (let* ((id (int-to-string (tp-get-attribute ticket 'Id)))
         (endpoint (concat (tp-get-ticket-type ticket) "/" id "/Comments"))
         (comments (assoc 'Items (tp-call-api endpoint)))
         (rev-comments))
    (if comments
        (progn
          (mapcar (lambda (el)
                    (cl-pushnew el rev-comments))
                  (cdr comments))
          (cdr rev-comments)))))

(defun tp-get-ticket-type (ticket)
  (if (equal "Bug" (tp-get-attribute ticket 'ResourceType))
      "Bugs"
      "UserStories"))

(defun tp-get-description (ticket)
  (tp-decode-string
   (tp-get-attribute ticket 'Description)))

(defun tp-build-comment (comment)
  (let* ((desc (tp-decode-string
                (cdr (assoc 'Description comment))))
         (created-at (tp-long-day-date
                      (tp-get-json-date
                       (tp-get-attribute comment 'CreateDate))))
         (owner (tp-get-owner comment)))
    (concat tp-divider "<br>" owner " (" created-at ") - " desc "<br>")))

(defun tp-build-ticket-header (ticket)
  "Put the pieces together that represent a TP ticket."
  (let* ((tp-name (tp-get-attribute ticket 'Name))
         (tp-created-at
          (tp-long-day-date (tp-get-json-date
                             (tp-get-attribute ticket 'CreateDate))))
         (owner (tp-get-owner ticket)))
    (concat " (Created by " owner " on: " tp-created-at ")")))

(defun tp-get-attribute (node attr)
  (cdr (assoc attr node)))

(defun tp-long-day-date (date)
  "Format a date to DOW, Month day, YEAR at HH:MM"
  (format-time-string "%A, %B %e, %Y at %k:%M" date))

(defun tp-get-json-date (date)
  (let* ((match (string-match "\\([0-9]+\\)" date))
         (time (match-string 0 date)))
    (if time
        (seconds-to-time (/ (string-to-int time) 1000))
        "")))

(defun tp-get-owner (ticket)
  "Get the node for the owner."
  (let* ((owner (tp-get-attribute ticket 'Owner))
        (last-name (tp-get-attribute owner 'LastName))
         (first-name (tp-get-attribute owner 'FirstName)))
    (concat first-name " " last-name)))


;; Begin Backlog Ranked code
(defun tp-display-backlog-ranked ()
  "Display the Backlog Ranked sorted by date
created descending."
  (interactive)
  (let* ((backlog (tp-get-backlog-ranked))
         (buffer (get-buffer-create "* Backlog: Ranked*")))
    (with-current-buffer buffer
      (toggle-truncate-lines t)
      (erase-buffer)
      (mapcar
        (lambda (ticket)
          (insert (tp-build-view-row ticket))
          (insert ?\n))
        backlog)
      (goto-char (point-min))
      (switch-to-buffer buffer))))

(defun tp-get-backlog-ranked ()
  "Retrieve tickets for EM-RE that are not currently being worked on."
  (let* ((tp-where "&where=(EntityType.name in ('UserStory', 'Bug')) and (Project.name eq 'EM-RE') and (EntityState.name ne 'Closed') and (EntityState.name ne 'Resolved')")
         (tp-include "&include=[name, createdate, CustomFields, EntityType, Assignments, Priority]")
         (tp-order "&orderByDesc=CreateDate")
         (qs (concat "Assignables?take=91" tp-where tp-include tp-order)))
    (tp-backlog-ranked-filter
     (tp-call-api qs))))

(defun tp-backlog-ranked-filter (all-tickets)
  "Filter tickets that are assigned or are not in the Back Log."
  (let ((tickets (cdr (assoc 'Items all-tickets))))
    (remove-if-not
     (lambda (ticket)
       (and
        (tp-is-in-backlog ticket)
        ;(null (tp-assignments ticket))
        ))
     tickets)))

(defun tp-is-in-backlog (ticket)
  "Return t if the Value of the Location
CustomField is equal to 'Back Log'."
  (string=
   "Back Log"
   (tp-get-location-field ticket)))

(defun tp-get-location-field (ticket)
  "Finds the 'Location' node in the CustomFields
for a ticket."
  (let* ((custom-fields (cdr (assoc 'CustomFields ticket)))
         (location (remove-if-not
                    (lambda (fields)
                      (rassoc "Location" fields))
                    custom-fields)))
    (cdr (assoc 'Value (aref location 0)))))

(defun tp-build-view-row (tp-ticket)
  (let* ((id (int-to-string (tp-get-attribute tp-ticket 'Id)))
         (ticket (tp-get-story id))
         (name (tp-get-attribute ticket 'Name))
         (severity (tp-get-severity ticket))
         (ticket-type
          (tp-get-entity-type ticket)))
    (concat id " (" ticket-type  ") - " name severity)))

(defun tp-get-entity-type (ticket)
  (cdr
   (assoc
    'Name
    (tp-get-attribute ticket 'EntityType))))

(defun tp-get-severity (ticket)
  "Extract the severity name for a ticket."
  (let* ((severity
          (tp-get-attribute ticket 'Severity)))
    (if severity
      (concat " : " (cdr (assoc 'Name severity)))
      "")))

(defun tp-assignments (node)
  "Retrieve the Assignment nodes within the Assignments node."
  (xml-get-children
   (first (xml-get-children node 'Assignments))
   'Assignment))


;; Get the Description nodes from the Comments node
;; (dolist (el (xml-get-children (car resp-comments-9532) 'Comment)) (message "%s" (xml-get-children el 'Description)))


;; (setq resp (tp-call-api "UserStories/7555"))


;; (let ((resp (tp-call-api "UserStories/7555")))
;;   resp)

;; (defun tp-old-get-story-attribute (attribute story)
;;   (cdr
;;    (assoc
;;     attribute
;;     (second
;;      (assoc 'UserStory story)))))

;; Can call-process return the stdout of the process in a string, as
;; shell-command-to-string does?

;; Yep (like Eli latter suggests):

;; (with-temp-buffer
;;  (call-process "ls" nil t)
;;  (buffer-string))
