(require 'mu4e)
(require 'mu4e-contrib)
(use-package mu4e-maildirs-extension :after mu4e)

;; https://www.djcbsoftware.nl/code/mu/mu4e/Refiling-messages.html
(defvar cavd-mu4e-subject-alist
  '(("Managed_Course" . "/DT_MC"))
  "List of subjects and their respective refile folders.")

(defun cavd-mu4e-refile-folder-function (msg)
  "Set the refile folder for MSG."
  (let ((subject (mu4e-message-field msg :subject))
        (folder (or (cdar (member*
                           subject my-mu4e-subject-alist
                           :test #'(lambda (x y)
                                     (string-match (car y) x))))
                    "/INBOX")))
    folder))

(setq mu4e-refile-folder 'cavd-mu4e-refile-folder-function)

;; default
(setq mu4e-maildir (expand-file-name "~/Maildir"))

(setq mu4e-drafts-folder "/Drafts"
      mu4e-sent-folder   "/Sent"
      mu4e-trash-folder  "/Trash"
      mu4e-refile-folder "/Archive")

;; don't save message to Sent Messages, GMail/IMAP will take care of this
(setq mu4e-sent-messages-behavior 'delete)
(setq message-kill-buffer-on-exit t)

;; Attempt to show images when viewing messages
(setq mu4e-view-show-images t
      mu4e-show-images t
      mu4e-view-image-max-width 800)

;; setup some handy shortcuts
(setq mu4e-maildir-shortcuts
      '(("/DT" . ?d)
        ("/DT_MC" . ?m)
        ("/INBOX" . ?i)
        ("/Sent" . ?s)
        ("/Trash" . ?t)))

;(mu4e-maildirs-extension)
(setq mu4e-headers-auto-update t)
(add-hook 'mu4e-index-updated-hook 'mu4e~headers-maybe-auto-update)

;; allow for updating mail using 'U' in the main view:
(setq mu4e-get-mail-command "offlineimap; exit 0"
      mu4e-update-interval 101)

;; (setq mu4e-html2text-command "html2text -utf8 -width 72") ;; nil "Shel command that converts HTML
;; ref: http://emacs.stackexchange.com/questions/3051/how-can-i-use-eww-as-a-renderer-for-mu4e
(defun my-render-html-message ()
  (let ((dom (libxml-parse-html-region (point-min) (point-max))))
    (erase-buffer)
    (shr-insert-document dom)
    (goto-char (point-min))))
;(setq mu4e-html2text-command 'my-render-html-message)

; An alternative
;; Call EWW to display HTML messages
(defun cavd-view-in-eww (msg)
  (eww-browse-url (concat "file://" (mu4e~write-body-to-html msg))))

;; yt
(setq mu4e-view-prefer-html t) ;; try to render
; Original value of mu4e-view-actions
;; (("capture message" . mu4e-action-capture-message)
;;  ("view as pdf" . mu4e-action-view-as-pdf)
;;  ("show this thread" . mu4e-action-show-thread))

;; Arrange to view messages in either the default browser or EWW
(add-to-list 'mu4e-view-actions '("ViewInBrowser" . mu4e-action-view-in-browser) t)
(add-to-list 'mu4e-view-actions '("Eww view" . cavd-view-in-eww) t)
;; mu4e as default email agent in emacs
(setq mail-user-agent 'mu4e-user-agent)
(require 'org-mu4e)

;; From Ben Maughan: Get some Org functionality in compose buffer
(add-hook 'message-mode-hook 'turn-on-orgtbl)
(add-hook 'message-mode-hook 'turn-on-orgstruct++)

(setq org-mu4e-convert-to-html t)
(setq mu4e-headers-date-format "%Y-%m-%d %H:%M")
(setq mu4e-headers-fields
      '((:human-date . 19)
        (:flags . 6)
        (:mailing-list . 10)
        (:from . 22)
        (:subject)))
(setq mu4e-attachment-dir  "~/Downloads")

;; something about ourselves
(setq message-signature
  (concat
   "--\n"
    "Chris Van Dusen.\n"
    "http://www.kalkomey.com\n"))


;; (add-hook
;;  'mu4e-main-mode-hook
;;  'mu4e-alert-enable-mode-line-display)

;; (use-package mu4e-alert
;;   :ensure t
;;   :after mu4e
;;   :init
;;   (setq mu4e-alert-interesting-mail-query
;;         "flag:unread maildir:/Exchange/INBOX ")
;;   (mu4e-alert-enable-mode-line-display)
;;   (mu4e-alert-set-default-style 'osx-notifier)
;;   (add-hook 'after-init-hook #'mu4e-alert-enable-notifications)
;;   (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)
;;   (defun cavd-refresh-mu4e-alert-mode-line ()
;;     (interactive)
;;     (mu4e~proc-kill)
;;     (mu4e-alert-enable-mode-line-display))
;;   (run-with-timer 0 60 'cavd-refresh-mu4e-alert-mode-line))


;; Now I set a list of
(defvar my-mu4e-account-alist
  '(("KE"
     (smtpmail-smtp-user "cvandusen@kalkomey.com")
     (smtpmail-local-domain "office365.com")
     (smtpmail-default-smtp-server "smtp.office365.com")
     (smtpmail-smtp-server "smtp.office365.com")
     (smtpmail-smtp-service 587))))

(defun my-mu4e-set-account ()
  "Set the account for composing a message.
   This function is taken from:
     https://www.djcbsoftware.nl/code/mu/mu4e/Multiple-accounts.html"
  (let* ((account
          (if mu4e-compose-parent-message
              (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
                (string-match "/\\(.*?\\)/" maildir)
                (match-string 1 maildir))
            (completing-read (format "Compose with account: (%s) "
                                     (mapconcat #'(lambda (var) (car var))
                                                my-mu4e-account-alist "/"))
                             (mapcar #'(lambda (var) (car var)) my-mu4e-account-alist)
                             nil t nil nil (caar my-mu4e-account-alist))))
         (account-vars (cdr (assoc account my-mu4e-account-alist))))
    (if account-vars
        (mapc #'(lambda (var)
                  (set (car var) (cadr var)))
              account-vars)
      (error "No email account found"))))

(add-hook 'mu4e-compose-pre-hook 'my-mu4e-set-account)


;; sending mail -- replace USERNAME with your gmail username
;; also, make sure the gnutls command line utils are installed
;; package 'gnutls-bin' in Debian/Ubuntu, 'gnutls' in Archlinux.

;; I have my "default" parameters from Gmail
;; (setq smtpmail-default-smtp-server "smtp.gmail.com"
;;       smtpmail-smtp-server "smtp.gmail.com"
;;       smtpmail-smtp-service 587)

(setq send-mail-function  'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it
      smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg")
      smtpmail-smtp-server  "smtp.office365.com"
      smtpmail-stream-type  'starttls
      smtpmail-smtp-service 587
      starttls-use-gnutls t)

(setq
      ;; smtpmail-starttls-credentials
      ;; '(("smtp.gmail.com" 587 nil nil))
      ;; smtpmail-auth-credentials (expand-file-name "~/.authinfo")
      smtpmail-smtp-service 1025
      smtpmail-debug-info t)


(setq gnus-select-method
      '(nnmaildir "ke"
        (get-new-mail nil)
        (target-prefix "")
        (directory "~/Maildir/")))


(require 'smtpmail)

(provide 'cavd-mu4e)
