;;;  cavd-hydra-dired.el --- Define hydra for Dired

;;; Commentary:

;;; Code:

(defhydra hydra-dired (:hint nil :color green)
"
_C-o_ Display         _SPC_ Next              _!_ Shell cmd          _#_ Auto-Save         _$_ Hide Subdir          _%_      Do Regexp
_(_   Hide            _*_   Mark              _+_ Create Dir         _-_ Negative Arg      _._ Clean                _:_      EPA
_<_ Prev              _=_   Diff              _>_ Next Dir           _?_ Summary           _I_ Info                 _N_      Man
_U_ Unmark All        _W_   Browse Url        _z_ Up-Dir             _a_ Find Alt          _d_ Flag Delete          _g_      Revert
_h_ Describe          _i_   Insert Subdir     _j_ Goto File          _m_ Mark              _n_ Next                 _o_      Other-Window
_p_ Previous          _q_   Quit Window       _r_ Wdired             _s_ Sort              _t_ Toggle Marks         _u_      Unmark
_v_ View              _w_   Copy Filename     _y_ Show Type          _~_ Flag-Backup       _DEL_ Unmark Back        _S-SPC_  Prev
_C-c o_ Open          _C-c v_ Doc View        _C-c w_ W3M            _T_ Image             _C-M-d_ Tree Down        _C-n_    Next Subdir
_C-p_ Prev            _C-u_ Tree Up           _M-!_ Smart Shell      _C-$_ Hide All        _M-(_ Mark Sexp          _M-G_    Goto Subdir
_M-{_ Prev Marked     _M-}_ Next Marked       _M-DEL_ Unmark All     _b_ Search            _f_ Filename Search      _c_      Cancel
"
  ("C-o" dired-display-file)
  ("SPC" dired-next-line)
  ("!" dired-do-shell-command)
  ("#" dired-flag-auto-save-files)
  ("$" dired-hide-subdir)
  ("%" (progn
         (hydra-dired-do-regexp/body)
         (hydra-push '(hydra-dired/body)))
        :exit t)
  ("(" dired-hide-details-mode)
  ("*" hydra-dired-mark/body :exit t)
  ("+" dired-create-directory)
  ("-" negative-argument)
  ("." dired-clean-directory)
  (":" hydra-dired-epa/body :exit t)
  ("<" dired-prev-dirline)
  ("=" dired-diff)
  (">" dired-next-dirline)
  ("?" dired-summary)
  ("I" dired-info)
  ("N" dired-man)
  ("c" nil)
  ("T" hydra-dired-image/body :exit t)
  ("U" dired-unmark-all-marks)
  ("W" browse-url-of-dired-file)
  ("z" dired-up-directory)
  ("a" dired-find-alternate-file)
  ("d" dired-flag-file-deletion)
  ("g" revert-buffer)
  ("h" describe-mode)
  ("i" dired-maybe-insert-subdir)
  ("j" dired-goto-file)
  ("m" dired-mark)
  ("n" dired-next-line)
  ("o" dired-find-file-other-window)
  ("p" dired-previous-line)
  ("q" quit-window :exit t)
  ("r" wdired-change-to-wdired-mode)
  ("s" dired-sort-toggle-or-edit)
  ("t" dired-toggle-marks)
  ("u" dired-unmark)
  ("v" dired-view-file)
  ("w" dired-copy-filename-as-kill)
  ("y" dired-show-file-type)
  ("~" dired-flag-backup-files)
  ("DEL" dired-unmark-backward)
  ("S-SPC" dired-previous-line)
  ("C-c o" cavd-dired-mac-open-file)
  ("C-c v" cavd-dired-doc-view-file)
  ("C-c w" cavd-dired-w3m-find-file)
  ("C-M-d" dired-tree-down)
  ("C-n" dired-next-subdir)
  ("C-p" dired-prev-subdir)
  ("C-u" dired-tree-up)
  ("M-!" dired-smart-shell-command)
  ("C-$" dired-hide-all)
  ("M-(" dired-mark-sexp)
  ("M-G" dired-goto-subdir)
  ("M-{" dired-prev-marked-file)
  ("M-}" dired-next-marked-file)
  ("M-DEL" dired-unmark-all-files)
  ("b" hydra-dired-search/body :exit t)
  ("f" hydra-dired-file-search/body :exit t))

(defhydra hydra-dired-search (:color blue)
  "
_s_ Search        _r_ Regexp Search        _q_ Quit
"
  ("s" dired-do-isearch)
  ("r" dired-do-isearch-regexp)
  ("q" nil))

(defhydra hydra-dired-file-search (:color blue)
  "
_s_ Search        _r_ Regexp Search         _q_ Quit
"
  ("s" dired-isearch-filenames)
  ("r" dired-isearch-filenames-regexp)
  ("q" nil))

(defhydra hydra-dired-do-regexp (:color blue)
  "
_&_ Flag Garbage        _C_ Copy Regexp        _H_ Hardlink Regexp        _R_ Rename Regexp        _S_ Symlink Regexp
_Y_ Relsymlink Regexp   _d_ Flag Regexp        _g_ Mark Files Regexp      _l_ Downcase             _m_ Mark Regexp
_r_ Rename Regexp       _u_ Upcase             _q_ Quit
"
  ("&" dired-flag-garbage-files)
  ("C" dired-do-copy-regexp)
  ("H" dired-do-hardlink-regexp)
  ("R" dired-do-rename-regexp)
  ("S" dired-do-symlink-regexp)
  ("Y" dired-do-relsymlink-regexp)
  ("d" dired-flag-files-regexp)
  ("g" dired-mark-files-containing-regexp)
  ("l" dired-downcase)
  ("m" dired-mark-files-regexp)
  ("q" hydra-pop)
  ("r" dired-do-rename-regexp)
  ("u" dired-upcase))

(defhydra hydra-dired-mark (:color blue)
  "
_n_ Next                  _p_ Previous         _!_ Unmark All              _%_ Mark Regexp          _(_ Mark Sexp          _*_ Mark Executables
_._ Mark Extension        _/_ Mark Dirs        _?_ Unmark All Files        _@_ Mark Symlinks        _O_ Mark Omitted       _c_ Change Marks
_m_ Mark                  _s_ Mark Subdirs     _t_ Toggle Marks            _u_ Unmark               _D_ Unmark Backward    _q_ Quit
"
  ("n" dired-next-marked-file)
  ("p" dired-prev-marked-file)
  ("!" dired-unmark-all-marks)
  ("%" dired-mark-files-regexp)
  ("(" dired-mark-sexp)
  ("*" dired-mark-executables)
  ("." dired-mark-extension)
  ("/" dired-mark-directories)
  ("?" dired-unmark-all-files)
  ("@" dired-mark-symlinks)
  ("O" dired-mark-omitted)
  ("c" dired-change-marks)
  ("m" dired-mark)
  ("s" dired-mark-subdir-files)
  ("t" dired-toggle-marks)
  ("u" dired-unmark)
  ("D" dired-unmark-backward)
  ("q" nil))

(defhydra hydra-dired-epa (:color blue)
  "
_d_ Decrypt        _e_ Encrypt        _s_ Sign        _v_ Verify
_q_ Quit
"
  ("d" epa-dired-do-decrypt)
  ("e" epa-dired-do-encrypt)
  ("q" nil)
  ("s" epa-dired-do-sign)
  ("v" epa-dired-do-verify))

(defhydra hydra-dired-do (:color blue)
  "
_&_ Async Shell          _A_ Find Regexp         _B_ Byte Comp         _C_ Copy          _D_ Delete            _F_ Find Marked
_G_ Chgrp                _H_ Hardlink            _L_ Load              _M_ Chmod         _O_ Chown             _P_ Print
_Q_ Find/Replace         _R_ Rename              _S_ Symlink           _T_ Touch         _V_ Mail              _X_ Shell Cmds
_Y_ Relsymlink           _Z_ Compress            _c_ Compress          _k_ Kill          _l_ Redisplay         _x_ Delete Flagged
_q_ Quit
"
  ("&" dired-do-async-shell-command)
  ("A" dired-do-find-regexp)
  ("B" dired-do-byte-compile)
  ("C" dired-do-copy)
  ("D" dired-do-delete)
  ("F" dired-do-find-marked-files)
  ("G" dired-do-chgrp)
  ("H" dired-do-hardlink)
  ("L" dired-do-load)
  ("M" dired-do-chmod)
  ("O" dired-do-chown)
  ("P" dired-do-print)
  ("Q" dired-do-find-regexp-and-replace)
  ("R" dired-do-rename)
  ("S" dired-do-symlink)
  ("T" dired-do-touch)
  ("V" dired-do-run-mail)
  ("X" dired-do-shell-command)
  ("Y" dired-do-relsymlink)
  ("Z" dired-do-compress)
  ("c" dired-do-compress-to)
  ("k" dired-do-kill-lines)
  ("l" dired-do-redisplay)
  ("x" dired-do-flagged-delete)
  ("q" nil))

(defhydra hydra-dired-image (:color blue)
  "
_g_ Toggle Marked Thumbs        _._ Display Thumb        _a_ Display Thumb Append        _c_ Comment Files
_d_ Display Thumbs              _e_ Edit Comment/Tags    _f_ Mark Tagged Files           _i_ Display Image
_j_ Jump Thumb Buffer           _r_ Delete Tag           _t_ Tag Files                   _x_ Display External
_q_ Quit
"
  ("g" image-dired-dired-toggle-marked-thumbs)
  ("." image-dired-display-thumb)
  ("a" image-dired-display-thumbs-append)
  ("c" image-dired-dired-comment-files)
  ("d" image-dired-display-thumbs)
  ("e" image-dired-dired-edit-comment-and-tags)
  ("f" image-dired-mark-tagged-files)
  ("i" image-dired-dired-display-image)
  ("j" image-dired-jump-thumbnail-buffer)
  ("r" image-dired-delete-tag)
  ("t" image-dired-tag-files)
  ("x" image-dired-dired-display-external)
  ("q" nil))

(provide 'cavd-hydra-dired)
;;; cavd-hydra-dired.el ends here.
