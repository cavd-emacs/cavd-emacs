;;;  cavd-ui.el --- Define UI settings

;;; Commentary:

;;; Code:

(use-package doom-modeline
  :ensure t
  :commands doom-modeline-mode
  :init
  (setq doom-modeline-vcs-max-length 17)
  :config
  (setq doom-modeline-height 25)
;  (set-face-attribute 'mode-line nil :family "Iosevka Comfy" :height 150)
;  (set-face-attribute 'mode-line-inactive nil :family "Hack" :height 150)
  ;; Don’t compact font caches during GC.
  (setq inhibit-compacting-font-caches t)
  (setq doom-modeline-buffer-file-name-style 'truncate-except-project)
  (setq doom-modeline-icon t)
  (setq doom-modeline-irc t)
  (setq doom-modeline-major-mode-icon nil)
  (setq doom-modeline-unicode-fallback t)
  (setq doom-modeline-lsp nil)
  (setq doom-modeline-window-width-limit fill-column)
  :hook
  (after-init . doom-modeline-mode))

(use-package all-the-icons
  :ensure t
  :commands all-the-icons-insert
  :config (setq all-the-icons-scale-factor 1.0))

;; (use-package ivy-posframe
;;   :ensure t
;;   :custom
;;   (ivy-posframe-parameters
;;    '((left-fringe . 2)
;;      (right-fringe . 2)
;;      (internal-border-width . 2)
;;      ))
;;   (ivy-posframe-height-alist
;;    '((swiper . 17)
;;      (swiper-isearch . 17)
;;      (t . 10)))
;;   (ivy-posframe-display-functions-alist
;;    '((complete-symbol . ivy-posframe-display-at-point)
;;      (swiper . nil)
;;      (swiper-isearch . nil)
;;      (counsel-describe-function . ivy-posframe-display-at-frame-center)
;;      (counsel-describe-variable . ivy-posframe-display-at-frame-center)
;;      (counsel-rg . nil)
;;      (counsel-switch-buffer . nil)
;;      (complete-symbol . ivy-posframe-display-at-point)
;;      (counsel-M-x . ivy-posframe-display-at-window-bottom-left)
;;      (t . ivy-posframe-display-at-frame-center)))
;;   :config
;;   (ivy-posframe-mode 1))

(use-package company-posframe
  :ensure t
  :diminish
  :hook
  (after-init . (lambda ()
                  (company-posframe-mode 1)))
  ;; :config
  ;; (company-posframe-mode 1)
  )

;; (use-package hydra-posframe
;;   :after hydra
;;   :load-path "~/elisp/hydra-posframe"
;;   :hook (after-init . hydra-posframe-mode))

;; (use-package which-key-posframe
;;   :ensure t
;;   :config
;;   (which-key-posframe-mode))

;; Different command can use different display function.

(defadvice load-theme (before theme-dont-propagate activate)
  (mapc #'disable-theme custom-enabled-themes))

;(modus-vivendi-theme-load)
    ;; modus-themes-bold-constructs                (boolean)
    ;; modus-themes-fringes                        (choice)
    ;; modus-themes-headings                       (alist)
    ;; modus-themes-links                          (choice)
    ;; modus-themes-no-mixed-fonts                 (boolean)
    ;; modus-themes-org-blocks                     (choice)
    ;; modus-themes-paren-match                    (choice)
    ;; modus-themes-region                         (choice)
    ;; modus-themes-slanted-constructs             (boolean)
    ;; modus-themes-variable-pitch-headings        (boolean)
    ;; modus-themes-scale-1
    ;; modus-themes-scale-2
    ;; modus-themes-scale-3
    ;; modus-themes-scale-4
    ;; modus-themes-scale-5

(add-to-list 'load-path (concat elisp-dir "/modus-themes"))
(add-to-list
 'custom-theme-load-path
 (concat elisp-dir "/modus-themes"))
(add-to-list 'Info-default-directory-list "~/elisp/modus-themes/doc")
(add-to-list 'Info-directory-list "~/elisp/modus-themes/doc")

(require 'modus-themes)

;; Obsolete variables
;; (setq modus-themes-region '(bg-only))
;; (setq modus-themes-scale-1 1.05)
;; (setq modus-themes-scale-2 1.1)
;; (setq modus-themes-scale-3 1.15)
;; (setq modus-themes-scale-4 1.2)
;; (setq modus-themes-scale-headings t)
;; (setq modus-themes-syntax '(faint))
;; (setq modus-themes-tabs-accented t)
;; (setq modus-themes-diffs nil)
;; (setq modus-themes-fringes nil)
;; (setq modus-themes-hl-line '(accented intense))
;; (setq modus-themes-links nil)
;; (setq modus-themes-markup '(background intense bold))
;; (setq modus-themes-mode-line nil)
;; (setq modus-themes-paren-match '(intense))

;(modus-themes-load-themes)
;(modus-themes-load-vivendi)
(setq modus-themes-completions
      '((matches . (extrabold))
        (selection . (semibold text-also))))
(setq modus-themes-prompts '(light))
(setq modus-themes-bold-constructs t)

(setq modus-themes-common-palette-overrides
      modus-themes-preset-overrides-faint)
(add-to-list 'modus-themes-common-palette-overrides
             '(border-mode-line-active unspecified))
(add-to-list 'modus-themes-common-palette-overrides
             '(border-mode-line-inactive unspecified))
(add-to-list 'modus-themes-common-palette-overrides
             '(bg-paren-match bg-magenta-intense))
(add-to-list 'modus-themes-common-palette-overrides
             '(fringe unspecified))

;; (setq modus-themes-common-palette-overrides
;;       '((border-mode-line-active unspecified)
;;         (border-mode-line-inactive unspecified)
;;         (bg-paren-match bg-magenta-intense)
;;         (fringe unspecified)))

(load-theme 'modus-vivendi :no-confirm)

(use-package pulse
  :commands (prot/pulse-line)
  :config
  (defface prot/pulse-line-modus-theme
    '((t :inherit modus-theme-subtle-green :extend t))
    "Ad-hoc face for `prot/pulse-line'.
This is done because it is not possible to highlight empty lines
without the `:extend' property.")

  (defun prot/pulse-line (&optional face)
    "Temporarily highlight the current line."
    (interactive)
    (let ((start (if (eobp)
                     (line-beginning-position 0)
                   (line-beginning-position)))
          (end (line-beginning-position 2))
          (pulse-delay .04)
          (face (or face 'prot/pulse-line-modus-theme)))
      (pulse-momentary-highlight-region start end face)))
  :bind ("M-`" . prot/pulse-line))

;; https://github.com/xenodium/ready-player
;; brew install mpv ffmpeg ffmpegthumbnailer
(use-package ready-player
  :init
  (unless (package-installed-p (intern "ready-player"))
    (package-vc-install "https://github.com/xenodium/ready-player"))
  :config
  (ready-player-add-to-auto-mode-alist))

(use-package tab-bar
  :commands (
             tab-bar-mode
             tab-bar-history-mode
             tab-bar-new-tab
             tab-bar-switch-to-next-tab
             tab-bar-switch-to-prev-tab
             prot-tab-select-tab-dwim
             prot-tab-tab-bar-toggle
             )
  :init
  (setq tab-bar-close-button-show nil)
  (setq tab-bar-close-last-tab-choice 'tab-bar-mode-disable)
  (setq tab-bar-close-tab-select 'recent)
  (setq tab-bar-new-tab-choice "*scratch*")
  (setq tab-bar-new-tab-to 'right)
  (setq tab-bar-position nil)
  (setq tab-bar-show nil)
  (setq tab-bar-tab-hints t)
  (setq tab-bar-tab-name-function 'tab-bar-tab-name-all)
  (setq tab-bar-history-limit 100)

  (defun prot-tab--tab-bar-tabs ()
    "Return a list of `tab-bar' tabs, minus the current one."
    (mapcar (lambda (tab)
              (alist-get 'name tab))
            (tab-bar--tabs-recent)))

  (defun prot-tab-select-tab-dwim ()
    "Do-What-I-Mean function for getting to a `tab-bar' tab.
If no other tab exists, create one and switch to it.  If there is
one other tab (so two in total) switch to it without further
questions.  Else use completion to select the tab to switch to."
    (interactive)
    (let ((tabs (prot-tab--tab-bar-tabs)))
      (cond ((eq tabs nil)
             (tab-new))
            ((eq (length tabs) 1)
             (tab-next))
            (t
             (tab-bar-switch-to-tab
              (completing-read "Select tab: " tabs nil t))))))

  (defun prot-tab-tab-bar-toggle ()
    "Toggle `tab-bar' presentation."
    (interactive)
    (if (bound-and-true-p tab-bar-mode)
        (progn
          (setq tab-bar-show nil)
          (tab-bar-mode -1))
      (setq tab-bar-show t)
      (tab-bar-mode 1)))

  :bind (("C-c s-t" . prot-tab-tab-bar-toggle)
         ("C-c <s-left>" . tab-bar-history-back)
         ("C-c <s-right>" . tab-bar-history-forward)
         ("<s-right>" . tab-bar-switch-to-next-tab)
         ("<s-left>" . tab-bar-switch-to-prev-tab)
         ("s-+" . tab-bar-new-tab)
         ("s-t" . prot-tab-select-tab-dwim)))

(use-package emacs
  :config
  (setq window-divider-default-right-width 1)
  (setq window-divider-default-bottom-width 1)
  (setq window-divider-default-places 'right-only)
  :hook
  (after-init . window-divider-mode))

;; ;; For consideration
;; (defvar-local my-minibuffer-font-remap-cookie nil
;;   "The current face remap of `my-minibuffer-set-font'.")

;; (defface my-minibuffer-default
;;   '((t :height 1.1))
;;   "Face for the minibuffer and the Completions.")

;; (defun my-minibuffer-set-font ()
;;   (setq-local my-minibuffer-font-remap-cookie
;;               (face-remap-add-relative 'default 'my-minibuffer-default)))

;; (add-hook 'minibuffer-mode-hook #'my-minibuffer-set-font)

(provide 'cavd-ui)
