(require 'url-http)

(defconst redmine-divider
  "--------------------------------------------------------------------------------"
  "Divider between ticket parts.")

(defvar *redmine-credentials* nil)

(defvar *redmine-header* nil)

(defvar *redmine-me* nil)

(defvar *redmine-bug-states* nil
  "The states that a bug can be in.")

(defvar *redmine-userstory-states* nil
  "The states that a user story can be in.")

(defvar redmine-base-url "https://tickets.kalkomey.com/"
  "The main URL for Redmine.")


(defun redmine-create-credentials ()
  (interactive)
  (cl-letf ((api-key (read-passwd "API Key: ")))
    (setq *redmine-header*
          `(("Content-Type" . "application/json")
            ("X-Redmine-API-Key"
             .
             ,(concat api-key)))))
  (message "Done"))

(cl-defun redmine-call-api (endpoint &key content (method "GET"))
  (let* ((url (concat redmine-base-url endpoint ".json"))
         (url-request-method (if method method "GET"))
         (redmine-header *redmine-header*)
         (url-request-extra-headers redmine-header)
         (url-request-data content)
         (url-mime-accept-string "application/json")
         (buffer (url-retrieve-synchronously url)))
    (with-current-buffer buffer
      (let ((response (url-http-parse-response)))
        (message "%S" response)
        (when (and (>= response 200) (<= response 300))
          (progn (goto-char (point-min))
                 (re-search-forward "^$" nil 'move)
                 (json-read)))))))

(defun redmine-display-ticket (ticket-id)
  "Display all aspects of a ticket."
  (interactive "nTicket Number: ")
  (let* ((id (int-to-string ticket-id))
         (story (redmine-get-issue id))
         ;; (comments (redmine-get-comments story))
         (redmine-buffer
          (get-buffer-create
           (concat "* TP " id " *"))))
    (if story
        (with-current-buffer redmine-buffer
          (erase-buffer)
          (insert (redmine-get-issue-subject story))
          (insert "<br><br>")
          (insert (redmine-get-issue-description story))
          (insert "<br><br>")
          ;; (mapcar
          ;;  (lambda (el)
          ;;    (insert (redmine-build-comment el)))
          ;;  comments)
          (shr-render-region (point-min) (point-max))
          (goto-char (point-max))
          (switch-to-buffer redmine-buffer)))))

(defun redmine-get-issue (id)
  "Get a ticket by ID."
  (redmine-call-api (concat "issues/" id)))

(defun redmine-get-issue-attribute (issue attr)
  "Retrieve the description for an issue."
  (cl-rest (assoc attr (cl-rest (assoc 'issue issue)))))

(defun redmine-get-id (attr)
  "Retrieve the id for an attribute."
  (cl-rest (assoc 'id attr)))

(defun redmine-get-name (attr)
  "Retrieve the id for an attribute."
  (cl-rest (assoc 'name attr)))

(defun redmine-get-issue-subject (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'subject))

(defun redmine-get-issue-description (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'description))

(defun redmine-get-issue-status (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'status))

(defun redmine-get-issue-priority (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'priority))

(defun redmine-get-issue-owner (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'author))

(defun redmine-get-issue-assignee (issue)
  "Retrieve the description for an issue."
  (redmine-get-issue-attribute issue 'assigned_to))

(defun redmine-get-issue-statuses ()
  "Get all issue statuses."
  (redmine-call-api (concat "issue_statuses")))

(provide 'redmine)
