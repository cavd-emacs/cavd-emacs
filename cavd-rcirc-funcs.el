;; Configs for rcirc

(use-package rcirc
  :config
  (setq rcirc-nick "cvandusen")
  (setq rcirc-default-nick "cvandusen")
  (setq rcirc-default-user-name "cvandusen")
  (setq rcirc-default-full-name "Chris Van Dusen")
  (setq rcirc-default-port 6697)
  (setq rcirc-server-alist
        '(("irc.libera.chat" :channels ("#emacs"))))
  :hook
  (rcirc-mode . flyspell-mode)
  (rcirc-mode . rcirc-omit-mode)
  (rcirc-mode . rcirc-track-minor-mode)
)

(require 'rcirc)
(setq rcirc-server-alist
      '(("irc.libera.chat" :channels ("#emacs"))))
(setq rcirc-authinfo
   (quote
    (("irc.libera.chat" nickserv "USERNAME" "PASSWORD"))))
(setq rcirc-default-nick "DEFAULTNICK")
(setq rcirc-default-user-name "DEFAULTUSERNAME")
(setq rcirc-log-flag t)

(provide 'cavd-rcirc-funcs)
