(use-package docker
  :defer t
  :ensure t
  :bind ("C-c d" . docker)
  :config
  (setf docker-command "~/.rd/bin/docker"
        docker-compose-command "~/.rd/bin/docker-compose"))

(use-package dockerfile-mode
  :defer t
  :ensure t
  :init
  (defun cavd-dockerfile-hook ()
    (interactive)
    (lsp))
  :config
  (setq dockerfile-mode-command "~/.rd/bin/docker")
  :hook
  (dockerfile-mode . cavd-dockerfile-hook))

(use-package docker-compose-mode
  :ensure t
  :config
  (defun cavd-docker-compose-hook ()
    (interactive)
    (lsp))
  :hook
  (dockerfile-mode . cavd-docker-compose-hook))

(provide 'cavd-docker)
